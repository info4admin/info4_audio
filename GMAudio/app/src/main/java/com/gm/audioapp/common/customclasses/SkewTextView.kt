package com.gm.audioapp.common.customclasses

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Canvas
import android.support.v7.widget.AppCompatTextView
import android.util.AttributeSet
import android.view.View

import com.gm.audioapp.R

import java.lang.Math.PI

class SkewTextView : AppCompatTextView {

    var degree: Float = 0.toFloat()
    var tan: Float = 0.toFloat()
    private var angle: Float = 0.toFloat()
    internal var pie = PI
    private var mContext: Context? = null

    constructor(context: Context) : super(context) {
        mContext = context
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        mContext = context

        val a = context.theme.obtainStyledAttributes(attrs, R.styleable.SkewRelativeLayout, 0, 0)

        try {
            // Getting attributes.


            degree = a.getFloat(R.styleable.SkewRelativeLayout_slopedAngle, 0f)
            //if(degree==0) degree=15;

            setAngle(degree)
        } finally {
            a.recycle()
        }
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        mContext = context
    }

    override fun onDraw(canvas: Canvas) {
        if (mContext!!.resources.configuration.layoutDirection == View.LAYOUT_DIRECTION_RTL) {
            //in Right To Left layout
            degree = -tan
            canvas.skew(-tan, 0f)
        } else {
            degree = tan
            canvas.skew(tan, 0f)
        }

        super.onDraw(canvas)
    }

    fun setAngle(degree: Float) {
        tan = Math.tan(degree * pie / 180).toFloat()
        android.util.Log.v("SkewTextView", "tan::" + tan + "degree" + degree)
        angle = degree
        invalidate()
    }

    /**
     * Need to invalidate the proper area of parent for skewed bounds
     */
    /*private void invalidateSkewedBound() {
        if (mSkewX != 0) {
            Matrix matrix = new Matrix();
            matrix.setSkew(-mSkewX, 0);
            mTempRect.set(0, 0, getRight(), getBottom());
            matrix.mapRect(mTempRect);
            mTempRect.offset(getLeft() + getTranslationX(), getTop() + getTranslationY());
            ((View) getParent()).invalidate((int) mTempRect.left, (int) mTempRect.top,
                    (int) (mTempRect.right + .5f), (int) (mTempRect.bottom + .5f));
        }
    }*/
}
