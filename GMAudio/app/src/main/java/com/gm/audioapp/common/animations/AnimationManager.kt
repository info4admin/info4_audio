package com.gm.audioapp.common.animations

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Point
import android.support.v4.view.animation.FastOutSlowInInterpolator
import android.support.v7.widget.CardView
import android.util.DisplayMetrics
import android.util.Log
import android.view.Display
import android.view.View
import android.view.ViewTreeObserver
import android.view.WindowManager
import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.R
import com.gm.media.models.DataPoolDataHandler

/**
 * It initialises child views for the given container
 */
class AnimationManager(mContext: Context, private val mContainer: View, private val mIsAnimation: Boolean) {
    private val mScreenWidth: Int
    private val display: Display
    private var mAppScreenHeight = 0

    private val mCornerRadius: Int
    private val mActionPanelHeight: Int
    private val mPresetFinalHeight: Int

    private val mFabSize: Int
    private val mPresetFabSize: Int
    private val mRadioCard: CardView

    private val mRadioControls: View
    private val mRadioCardControls: View
    private val mPresetsList: View


    /**
     * Interface to be implemented to get call back when animation exit finishes
     */
    interface OnExitCompleteListener {
        /**
         * this method is called by 'RadioAnimationManager' when it finishes exit animation
         */
        fun onExitAnimationComplete()
    }


    init {
        if (!mIsAnimation) clearAnimationDuration() else addAnimationDuration()

        val windowManager = mContext.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        display = windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)

        mScreenWidth = if (DataPoolDataHandler.isInfoCurvedView.get()!!) size.x / 3 else size.x

        val res = mContext.resources
        val resAudio = GMAudioApp.appContext.resources
        mCornerRadius = res.getDimensionPixelSize(R.dimen.car_preset_item_radius)
        mActionPanelHeight = resAudio.getDimensionPixelSize(R.dimen.ics_audio_am_station_list_CardView12_height)
        mPresetFinalHeight = if (DataPoolDataHandler.isInfoCurvedView.get()!!) 150 else res.getDimensionPixelSize(R.dimen.car_preset_item_height)
        mFabSize = res.getDimensionPixelSize(R.dimen.stream_fab_size)
        mPresetFabSize = res.getDimensionPixelSize(R.dimen.car_presets_play_button_size)

        mRadioCard = mContainer.findViewById(R.id.current_radio_station_card)
        mRadioControls = mContainer.findViewById(R.id.controlButtonsLayout)
        //        mRadioControls = container.findViewById(R.id.radio_buttons_container);
        mRadioCardControls = mContainer.findViewById(R.id.current_radio_station_card)
        mPresetsList = mContainer.findViewById(R.id.stations_list)

    }

    /**
     * Start the exit animation for the preset activity. This animation will move the radio controls
     * down to where it would be in the [AnimationManager]. Upon completion of the
     * animation, the given [OnExitCompleteListener] will be notified.
     */
    fun playExitAnimation(listener: OnExitCompleteListener) {
        Log.v("playExitAnimation", "yes")

        addAnimationDuration()

        // Animator that will animate the radius of mRadioCard from rounded to non-rounded.
        val cornerRadiusAnimator = ValueAnimator.ofInt(mCornerRadius, 0)
        cornerRadiusAnimator.startDelay = STOP_ANIM_DELAY_MS.toLong()
        cornerRadiusAnimator.duration = STOP_ANIM_DURATION_MS.toLong()
        cornerRadiusAnimator.addUpdateListener { animator -> mRadioCard.radius = (animator.animatedValue as Int).toFloat() }
        cornerRadiusAnimator.interpolator = sInterpolator

        // Animator that will animate the radius of mRadioCard from its current width to the width
        // of the screen.
        val widthAnimator = ValueAnimator.ofInt(mRadioCard.width, mScreenWidth)
        widthAnimator.interpolator = sInterpolator
        widthAnimator.startDelay = STOP_ANIM_DELAY_MS.toLong()
        widthAnimator.duration = STOP_ANIM_DURATION_MS.toLong()
        widthAnimator.addUpdateListener { valueAnimator ->
            val width = valueAnimator.animatedValue as Int
            mRadioCard.layoutParams.width = width
            mRadioCard.requestLayout()
        }

        // Animate the height of the radio controls from its current height to the full height of
        // the action panel.
        val heightAnimator = ValueAnimator.ofInt(mRadioCard.height,
                mActionPanelHeight)
        heightAnimator.interpolator = sInterpolator
        heightAnimator.startDelay = STOP_ANIM_DELAY_MS.toLong()
        heightAnimator.duration = STOP_ANIM_DURATION_MS.toLong()
        heightAnimator.addUpdateListener { valueAnimator ->
            val height = valueAnimator.animatedValue as Int
            mRadioCard.layoutParams.height = height
            mRadioCard.requestLayout()
        }

        // Animate the fab back to the size it will be in the main radio display.
        val fabAnimator = ValueAnimator.ofInt(mPresetFabSize, mFabSize)
        fabAnimator.interpolator = sInterpolator
        fabAnimator.startDelay = STOP_ANIM_DELAY_MS.toLong()
        fabAnimator.duration = STOP_ANIM_DURATION_MS.toLong()

        // The animator for the move downwards of the radio card.
        val translationYAnimator = ObjectAnimator.ofFloat<View>(mRadioCard,
                View.TRANSLATION_Y, mRadioCard.translationY, 0f)
        translationYAnimator.duration = STOP_TRANSLATE_ANIM_DURATION_MS.toLong()

        // The animator for the move downwards of the preset list.
        val presetAnimator = ObjectAnimator.ofFloat<View>(mPresetsList,
                View.TRANSLATION_Y, 0f, mAppScreenHeight.toFloat())
        presetAnimator.duration = STOP_TRANSLATE_ANIM_DURATION_MS.toLong()
        presetAnimator.start()

        // The animator for will fade in the radio controls.
        val radioControlsAlphaAnimator = ValueAnimator.ofFloat(0f, 1f)
        radioControlsAlphaAnimator.interpolator = sInterpolator
        radioControlsAlphaAnimator.startDelay = STOP_FADE_ANIM_DELAY_MS.toLong()
        radioControlsAlphaAnimator.duration = STOP_FADE_ANIM_DURATION_MS.toLong()
        radioControlsAlphaAnimator.addUpdateListener { valueAnimator -> mRadioControls.alpha = valueAnimator.animatedValue as Float }
        radioControlsAlphaAnimator.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animator: Animator) {
                mRadioControls.visibility = View.VISIBLE
            }

            override fun onAnimationEnd(animator: Animator) {}

            override fun onAnimationCancel(animator: Animator) {}

            override fun onAnimationRepeat(animator: Animator) {}
        })

        // The animator for will fade out of the preset radio controls.
        val radioCardControlsAlphaAnimator = ObjectAnimator.ofFloat(mRadioCardControls,
                View.ALPHA, 1f, 0f)
        radioCardControlsAlphaAnimator.interpolator = sInterpolator
        radioCardControlsAlphaAnimator.startDelay = STOP_FADE_ANIM_DELAY_MS.toLong()
        radioCardControlsAlphaAnimator.duration = STOP_FADE_ANIM_DURATION_MS.toLong()
        radioCardControlsAlphaAnimator.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animator: Animator) {}

            override fun onAnimationEnd(animator: Animator) {
                mRadioCardControls.visibility = View.GONE
                mRadioCard.visibility = View.GONE
            }

            override fun onAnimationCancel(animator: Animator) {}

            override fun onAnimationRepeat(animator: Animator) {}
        })

        val animatorSet = AnimatorSet()
        animatorSet.playTogether(cornerRadiusAnimator, heightAnimator, widthAnimator, fabAnimator,
                translationYAnimator, radioControlsAlphaAnimator, radioCardControlsAlphaAnimator,
                presetAnimator)
        animatorSet.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animator: Animator) {
                // Remove any elevation from the radio container since the radio card will move
                // out from the container.
                //                mRadioCardContainer.setElevation(0);
            }

            override fun onAnimationEnd(animator: Animator) {
                listener.onExitAnimationComplete()
            }

            override fun onAnimationCancel(animator: Animator) {}

            override fun onAnimationRepeat(animator: Animator) {}
        })
        animatorSet.start()
    }

    /**
     * Start the enter animation for the preset fragment. This animation will move the radio
     * controls up from where they are in the [AnimationManager] to its final position.
     */
    fun playEnterAnimation() {
        // The animation requires that we know the size of the activity window. This value is
        // different from the size of the screen, which we could obtain using DisplayMetrics. As a
        // result, need to use a ViewTreeObserver to get the size of the containing view.
        mContainer.viewTreeObserver.addOnGlobalLayoutListener(
                object : ViewTreeObserver.OnGlobalLayoutListener {
                    override fun onGlobalLayout() {
                        mAppScreenHeight = mContainer.height
                        startEnterAnimation()
                        mContainer.viewTreeObserver.removeOnGlobalLayoutListener(this)
                    }
                })
    }

    /**
     * Actually starts the animations for the enter of the preset fragment.
     */
    private fun startEnterAnimation() {
        val sInterpolator = FastOutSlowInInterpolator()

        // Animator that will animate the radius of mRadioCard to its final rounded state.
        val cornerRadiusAnimator = ValueAnimator.ofInt(0, mCornerRadius)
        cornerRadiusAnimator.duration = START_ANIM_DURATION_MS.toLong()
        cornerRadiusAnimator.addUpdateListener { animator -> mRadioCard.radius = (animator.animatedValue as Int).toFloat() }
        cornerRadiusAnimator.interpolator = sInterpolator

        // Animate the radio card from the size of the screen to its final size in the preset
        // list.

        val widthAnimator = ValueAnimator.ofInt(mScreenWidth, mScreenWidth / 2)
        widthAnimator.interpolator = sInterpolator
        widthAnimator.duration = START_ANIM_DURATION_MS.toLong()
        if (!mIsAnimation)
            widthAnimator.startDelay = 0
        else
            widthAnimator.startDelay = 40
        widthAnimator.addUpdateListener { valueAnimator ->
            if (!mIsAnimation)
                mRadioCard.visibility = View.INVISIBLE
            val width = valueAnimator.animatedValue as Int
            mRadioCard.layoutParams.width = width
            mRadioCard.requestLayout()
        }

        // Shrink the radio card down to its final height.
        val heightAnimator = ValueAnimator.ofInt(mActionPanelHeight, mPresetFinalHeight)
        heightAnimator.interpolator = sInterpolator
        heightAnimator.duration = START_ANIM_DURATION_MS.toLong()
        heightAnimator.addUpdateListener { valueAnimator ->
            val height = valueAnimator.animatedValue as Int
            mRadioCard.layoutParams.height = height
            mRadioCard.requestLayout()
        }

        // Animate the fab from its large size in the radio controls to the smaller size in the
        // preset list.
        val fabAnimator = ValueAnimator.ofInt(mFabSize, mPresetFabSize)
        fabAnimator.interpolator = sInterpolator
        fabAnimator.duration = START_ANIM_DURATION_MS.toLong()

        // The top of the screen relative to where mRadioCard is positioned.
        var topOfScreen = mAppScreenHeight - mActionPanelHeight

        // Because the height of the radio controls changes, we need to add the difference in height
        // to the final translation.
        topOfScreen += (mActionPanelHeight - mPresetFinalHeight)


        // The radio card will need to be centered within the area given by mPresetContainerHeight.
        // This finalTranslation value is negative so that mRadioCard moves upwards.
        //        int finalTranslation = -(topOfScreen - ((mPresetContainerHeight - mPresetFinalHeight) / 2));
        //        int finalTranslation = -(topOfScreen - mPresetCardMarginTop);

        val dis = DisplayMetrics()
        display.getMetrics(dis)
        val marginTop = dis.densityDpi / 2
        val finalTranslation = -(topOfScreen - marginTop)

        // Animator to move the radio card from the bottom of the screen to its final y value.
        val translationYAnimator = ObjectAnimator.ofFloat<View>(mRadioCard,
                View.TRANSLATION_Y, 0f, finalTranslation.toFloat())
        translationYAnimator.startDelay = START_TRANSLATE_ANIM_DELAY_MS.toLong()
        translationYAnimator.duration = START_TRANSLATE_ANIM_DURATION_MS.toLong()

        // Animator to slide the preset list from the bottom of the screen to just below the radio
        // card.
        val presetAnimator = ObjectAnimator.ofFloat<View>(mPresetsList,
                View.TRANSLATION_Y, mAppScreenHeight.toFloat(), 0f)
        presetAnimator.startDelay = START_TRANSLATE_ANIM_DELAY_MS.toLong()
        presetAnimator.duration = START_TRANSLATE_ANIM_DURATION_MS.toLong()
        presetAnimator.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animator: Animator) {
                mPresetsList.visibility = View.VISIBLE
            }

            override fun onAnimationEnd(animator: Animator) {}

            override fun onAnimationCancel(animator: Animator) {}

            override fun onAnimationRepeat(animator: Animator) {}
        })

        // Animator to fade out the radio controls.
        val radioControlsAlphaAnimator = ValueAnimator.ofFloat(1f, 0f)
        radioControlsAlphaAnimator.interpolator = sInterpolator
        radioControlsAlphaAnimator.startDelay = START_FADE_ANIM_DELAY_MS.toLong()
        radioControlsAlphaAnimator.duration = START_FADE_ANIM_DURATION_MS.toLong()
        radioControlsAlphaAnimator.addUpdateListener { valueAnimator -> mRadioControls.alpha = valueAnimator.animatedValue as Float }
        radioControlsAlphaAnimator.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animator: Animator) {}

            override fun onAnimationEnd(animator: Animator) {
                mRadioControls.visibility = View.GONE
                mRadioCard.visibility = View.VISIBLE
            }

            override fun onAnimationCancel(animator: Animator) {}

            override fun onAnimationRepeat(animator: Animator) {}
        })

        // Animator to fade in the radio controls for the preset card.
        val radioCardControlsAlphaAnimator = ObjectAnimator.ofFloat(mRadioCardControls,
                View.ALPHA, 0f, 1f)
        radioCardControlsAlphaAnimator.interpolator = sInterpolator
        radioCardControlsAlphaAnimator.startDelay = START_FADE_ANIM_DELAY_MS.toLong()
        radioCardControlsAlphaAnimator.duration = START_FADE_ANIM_DURATION_MS.toLong()
        radioCardControlsAlphaAnimator.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animator: Animator) {
                mRadioCardControls.visibility = View.VISIBLE
            }

            override fun onAnimationEnd(animator: Animator) {}

            override fun onAnimationCancel(animator: Animator) {}

            override fun onAnimationRepeat(animator: Animator) {}
        })

        val animatorSet = AnimatorSet()
        animatorSet.playTogether(cornerRadiusAnimator, heightAnimator, widthAnimator, fabAnimator,
                translationYAnimator, radioControlsAlphaAnimator, radioCardControlsAlphaAnimator,
                presetAnimator)
        animatorSet.start()
    }

    //set all animation duration to "0"
    private fun clearAnimationDuration() {
        START_ANIM_DURATION_MS = 0
        START_TRANSLATE_ANIM_DELAY_MS = 0
        START_TRANSLATE_ANIM_DURATION_MS = 0
        START_FADE_ANIM_DELAY_MS = 0
        START_FADE_ANIM_DURATION_MS = 0

        STOP_ANIM_DELAY_MS = 0
        STOP_ANIM_DURATION_MS = 0
        STOP_TRANSLATE_ANIM_DURATION_MS = 0
        STOP_FADE_ANIM_DELAY_MS = 0
        STOP_FADE_ANIM_DURATION_MS = 0
    }

    //set all animation duration to appropriate duration
    private fun addAnimationDuration() {
        START_ANIM_DURATION_MS = 500
        START_TRANSLATE_ANIM_DELAY_MS = 0
        START_TRANSLATE_ANIM_DURATION_MS = 383
        START_FADE_ANIM_DELAY_MS = 0
        START_FADE_ANIM_DURATION_MS = 100

        STOP_ANIM_DELAY_MS = 215
        STOP_ANIM_DURATION_MS = 333
        STOP_TRANSLATE_ANIM_DURATION_MS = 417
        STOP_FADE_ANIM_DELAY_MS = 150
        STOP_FADE_ANIM_DURATION_MS = 100
    }

    companion object {
        private var START_ANIM_DURATION_MS = 500
        private var START_TRANSLATE_ANIM_DELAY_MS = 117
        private var START_TRANSLATE_ANIM_DURATION_MS = 383
        private var START_FADE_ANIM_DELAY_MS = 150
        private var START_FADE_ANIM_DURATION_MS = 100

        private var STOP_ANIM_DELAY_MS = 215
        private var STOP_ANIM_DURATION_MS = 333
        private var STOP_TRANSLATE_ANIM_DURATION_MS = 417
        private var STOP_FADE_ANIM_DELAY_MS = 150
        private var STOP_FADE_ANIM_DURATION_MS = 100

        private val sInterpolator = FastOutSlowInInterpolator()
    }

}