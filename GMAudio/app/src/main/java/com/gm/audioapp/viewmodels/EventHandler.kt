package com.gm.audioapp.viewmodels

import android.view.KeyEvent
import android.view.View
import android.widget.RadioGroup
import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.R
import com.gm.audioapp.R.id.*
import com.gm.audioapp.viewmodels.EventProcessor.eventTable
import com.gm.audioapp.viewmodels.EventProcessor.keyboardCommands
import com.gm.audioapp.viewmodels.EventProcessor.remoteCommands
import com.gm.audioapp.viewmodels.EventProcessor.triggerEvent
import com.gm.audioapp.viewmodels.EventProcessor.voiceCommands
import com.gm.audioapp.viewmodels.Utility.setNewLocale
import com.gm.media.models.eLHD_RHD_RTL
import com.gm.media.models.DataPoolDataHandler
import org.json.JSONObject


/**
 * This class is used to handle the event generated when user interact with the screen
 * @author by GM on 3/7/2018.
 */

object EventHandler : IEventHandler {

    /**
     * Added for eVS test app to trigger event sent through adb.
     */
    fun processEventByTag(tag: String) {
        val eventObj = eventTable[tag] as JSONObject?
        processEventHandler(null, eventObj, null)
    }

    /**
     * It is used to process the events.

     * @param eventName passing event name.
     */

    fun initEvent(eventName: String) {
        println("AMFM_REQ_INIT:: $eventName")
        EventProcessor.processEvent(eventName, null)
    }

    /**
     * It is used to process the events.

     * @param eventName passing event name.
     * @param any passing  additional information.

     */

    fun initEvent(eventName: String, any: Any) {
        println("init:: $eventName")
        EventProcessor.processEvent(eventName, any)
    }

    override fun onClickHandler(view: View) {
        println("onClickHandler::$view ")
        //val eventObj = eventTable[touchViewCommands[getResourceName(view.id)]] as JSONObject?
        val eventObj = eventTable[view.tag] as JSONObject?
        processEventHandler(view, eventObj, null)
    }

    override fun onClickHandler(view: View, any: Any) {
        println("onClickHandler::$view $any")
        //val eventObj = eventTable[touchViewCommands[getResourceName(view.id)]] as JSONObject?
        val eventObj = eventTable[view.tag] as JSONObject?
        processEventHandler(view, eventObj, any)
    }

    fun onDrawerClick(tag: String?) {
        println("onDrawerClick::$tag")
        val eventObj = eventTable[tag] as JSONObject?
        println("onDrawerClick eventObj ::$eventObj")
        processEventHandler(null, eventObj, null)
    }

    fun onDrawerClickItem(tag: String?) {
        println("onDrawerClickItem::$tag")
        val navigationItems = GMAudioApp.appContext.activityContext.navigationDrawerItemsWithKeyMap()
        onDrawerClick(navigationItems[tag])
    }

    override fun onKeyHandler(keyCode: Int, event: KeyEvent?) {
        val eventObj = eventTable[keyboardCommands[event?.displayLabel.toString()]] as JSONObject?
        processEventHandler(null, eventObj, null)
    }

    override fun onVoiceHandler(vararg voiceControlCommands: String) {
        for (command in voiceControlCommands) {
            val eventObj = eventTable[voiceCommands[command.toLowerCase()]] as JSONObject?
            if (eventObj != null) {
                processEventHandler(null, eventObj, null)
                break
            }
        }
    }

    override fun onRemoteHandler(remoteControlCommands: String) {
        val eventObj = eventTable[remoteCommands[remoteControlCommands]] as JSONObject?
        processEventHandler(null, eventObj, null)
    }


    /**
     * It is used to process the events.

     * @param eventObj passing json data.
     * @param data passing any additional information.

     */
    private fun processEventHandler(view: View?, eventObj: JSONObject?, data: Any?) {
        if (eventObj != null) {
            val eventType = eventObj.getString("eventType")
            val eventName = eventObj.getString("eventName")
            val activityName = eventObj.getString("activityName")
            val isProcess = eventObj.getBoolean("isProcess")
            println("processEventHandler::$eventType $eventName $activityName $isProcess")
            triggerEvent(view, eventType, eventName, data, activityName, isProcess)
        }
    }

    /**
     * change the radio button corresponding to given id.

     * @param id used to change radio group.

     */
    fun onRadioChanged(radioGroup: RadioGroup, id: Int) {
        // This will get the radiobutton that has changed in its check state

        when (id) {
            radioButton1 ->
                DataPoolDataHandler.themeType.set(true)
            radioButton2 ->
                DataPoolDataHandler.themeType.set(false)
            lhdRadiButton -> {
                DataPoolDataHandler.LHD_RHD_RTL.set(eLHD_RHD_RTL.LHD)
                setNewLocale(GMAudioApp.appContext, "en")
            }
            rhdRadioButton -> {
                DataPoolDataHandler.LHD_RHD_RTL.set(eLHD_RHD_RTL.RHD)
                setNewLocale(GMAudioApp.appContext, "uk")
            }
            rtlRadioButton -> {
                DataPoolDataHandler.LHD_RHD_RTL.set(eLHD_RHD_RTL.RTL)
                setNewLocale(GMAudioApp.appContext, "ar")
            }
            SideBarEnable ->
                DataPoolDataHandler.isLargeScreenLayout.set(true)
            SideBarDisable -> {
                DataPoolDataHandler.isLargeScreenLayout.set(false)
                DataPoolDataHandler.isInfoCurvedView.set(false)
            }
            curveEnable ->
                DataPoolDataHandler.isInfoCurvedView.set(true)
            curveDisable ->
                DataPoolDataHandler.isInfoCurvedView.set(false)
            SkewEnable -> {
                DataPoolDataHandler.isSkewEnabled.set(true)
                DataPoolDataHandler.SKEW_ANGLE_8.set(GMAudioApp.appContext.resources.getInteger(R.integer.sloped_angle_skew_8).toFloat())
                DataPoolDataHandler.SKEW_ANGLE_9.set(GMAudioApp.appContext.resources.getInteger(R.integer.sloped_angle_skew_9).toFloat())
                DataPoolDataHandler.SKEW_ANGLE_15.set(GMAudioApp.appContext.resources.getInteger(R.integer.sloped_angle_skew_15).toFloat())
            }
            SkewDisable -> {
                DataPoolDataHandler.isSkewEnabled.set(false)
                DataPoolDataHandler.SKEW_ANGLE_8.set(GMAudioApp.appContext.resources.getInteger(R.integer.sloped_angle_skew_0).toFloat())
                DataPoolDataHandler.SKEW_ANGLE_9.set(GMAudioApp.appContext.resources.getInteger(R.integer.sloped_angle_skew_0).toFloat())
                DataPoolDataHandler.SKEW_ANGLE_15.set(GMAudioApp.appContext.resources.getInteger(R.integer.sloped_angle_skew_0).toFloat())
            }
        }
    }

}