package com.gm.audioapp.viewmodels

import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.view.View
import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.GMAudioApp.Companion.appContext
import com.gm.audioapp.GMAudioApp.Companion.navigator
import com.gm.audioapp.R
import com.gm.media.apiintegration.AudioService
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader


/**
 * This class process the event generated when user interacts  with screens
 */
object EventProcessor {

    var eventTable = HashMap<String, Any>()
    //var touchViewCommands = HashMap<String, Any>()
    var voiceCommands = HashMap<String, Any>()
    var keyboardCommands = HashMap<String, Any>()
    var remoteCommands = HashMap<String, Any>()
    private const val resFileCommands: Int = R.raw.commands
    private const val resFileEventMap: Int = R.raw.eventmap

    init {
        //touchViewCommands = getCommands(resFileCommands,"touch")
        voiceCommands = getCommands(resFileCommands, "voice")
        keyboardCommands = getCommands(resFileCommands, "keyboard")
        remoteCommands = getCommands(resFileCommands, "remote")
        eventTable = getEventMapTable(resFileEventMap)
    }

    /**
     * To get commands
     * @param resFileRead , used to get commands.
     * @param commandType , used to get commandtype.
     * @return hashMap.
     */

    private fun getCommands(resFileRead: Int, commandType: String): HashMap<String, Any> {
        val jsonObj = JSONObject(getJsonString(appContext.resources.openRawResource(resFileRead))).getJSONObject(commandType)
        return hashMap(jsonObj)
    }

    /**
     * To get event map table
     * @param resFileRead , used to get table.
     * @return hashMap.
     */

    private fun getEventMapTable(resFileRead: Int): HashMap<String, Any> {
        val jsonObj = JSONObject(getJsonString(appContext.resources.openRawResource(resFileRead)))
        return hashMap(jsonObj)
    }

    /**

     * @param jsonObj passing json data.
     * @return map.
     */
    private fun hashMap(jsonObj: JSONObject): HashMap<String, Any> {
        val map = HashMap<String, Any>()
        val iterator = jsonObj.keys()
        while (iterator.hasNext()) {
            val key = iterator.next()
            try {
                val command = jsonObj.get(key)
                map[key] = command
            } catch (e: JSONException) {
            }

        }
        return map
    }

    /**
     * To get json string
     * @param stream is InputStream, used to get json string.
     * @return String.
     */
    fun getJsonString(stream: InputStream): String {

        val strBuilder = StringBuilder()
        var jsonString: String? = null
        val bfReader = BufferedReader(InputStreamReader(stream, "UTF-8"))
        while ({ jsonString = bfReader.readLine(); jsonString }() != null) {
            strBuilder.append(jsonString)
        }
        return strBuilder.toString()
    }

    /**
     * trigger the event corresponding to given eventType,eventName and activityName.
     * @param eventType passing which type of event has to perform.
     * @param eventName passing which name of event has to perform.
     * @param activityName passing which name of activity/fragment has to perform.
     * @param isProcess passing boolean status.
     * @param any  additional data that is passed.
     */
    fun triggerEvent(view: View?, eventType: String, eventName: String, any: Any?, activityName: String, isProcess: Boolean) {
        when (eventType) {
            "navigate" -> {
                println("eventProcessor::navigate $eventName")

                if (isProcess) {
                    processEvent(eventName, any)
                }

                if (eventName == "evG_Back") {
                    println("eventProcessor::finish $eventName")
                    navigator.finishActivity()
                } else if (eventName == "evG_Close") {
                    println("eventProcessor::finish $eventName")
                    navigator.finishActivity()
                } else if (eventName == "evG_Recreate") {
                    println("eventProcessor::finish $eventName")
                    navigator.recreateActivity()
                } else {
                    if (GMAudioApp.appContext.activityContext.javaClass.simpleName == activityName) {
                        replaceScreen(view, eventName, any)
                    } else {
                        navigateScreen(eventName, any)
                    }
                }
            }
            "process" -> {
                println("eventProcessor::process $eventName")
                processEvent(eventName, any)
            }

        }
    }

    /**
     * process the event corresponding to given eventName
     * @param eventName passing which name of event has to perform.
     * @param any  additional data that is passed.
     */
    fun processEvent(eventName: String, any: Any?) {
        println("processEvent::process $eventName")
        AudioService.any = any
        val serviceIntent = Intent()
        serviceIntent.action = "com.gm.media.apiintegration.AudioService"
        serviceIntent.`package` = GMAudioApp.appContext.packageName
        serviceIntent.putExtra("eventName", eventName)
        GMAudioApp.appContext.startService(serviceIntent)

    }

    /**
     * navigate the fragment corresponding to given eventName
     * @param eventName passing which name of event has to perform.
     * @param any  additional data that is passed.
     */
    private fun navigateScreen(eventName: String, any: Any?) {
        println("navigateScreen::navigate $eventName")
        navigator.triggerActivity(eventName, any)
    }

    /**
     * add/replaces fragment corresponding to given eventName
     * @param eventName passing which name of event has to perform.
     * @param any  additional data that is passed.
     */

    private fun replaceScreen(view: View?, eventName: String, any: Any?) {
        println("navigateScreen::navigate $eventName")
        val currentFragment = GMAudioApp.appContext.activityContext.supportFragmentManager.findFragmentById(GMAudioApp.appContext.activityContext.getContainerId())
        println("navigateScreen::currentFragment $currentFragment")
        if (view != null && currentFragment is RegisterListener) {
            currentFragment.onRegisterListener(view, eventName, any)
        } else
            navigator.triggerFragment(eventName, any)

    }

}