package com.gm.audioapp.ui.activities

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.DrawerLayout
import android.view.KeyEvent
import android.view.View
import android.view.ViewGroup
import com.android.car.app.CarDrawerAdapter
import com.android.car.app.DrawerItemViewHolder
import com.android.car.gm.ui.GMCarDrawerAdapter
import com.android.car.gm.ui.GMDrawerActivity
import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.R
import com.gm.media.models.DataPoolDataHandler
import com.gm.audioapp.viewmodels.EventHandler
import com.gm.audioapp.viewmodels.Utility.setLocale
import com.gm.audioapp.viewmodels.Utility.setNewLocale
import com.gm.media.models.*
import java.util.*


/**
 * The base class for Audio Module. Override abstract methods to setup navigation drawer. You can also change custom themes dynamically.
 *
 * @author Aswin
 */
abstract class BaseActivity : GMDrawerActivity() {

    /** To enabled or disable [mDrawerLayout] inflated in super class */
    protected var isNavigationDrawerEnabled: Boolean = false
    /** Map of drawer items and respective events */
    protected lateinit var navigationItems: HashMap<String, String>
    /** To save the event sent through adb command for testing*/
    private lateinit var adbIntentEvent: String

    private fun setUpNavigationDrawer() {
        isNavigationDrawerEnabled = enableNavigationDrawer()
        if (isNavigationDrawerEnabled) {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
            navigationItems = navigationDrawerItemsWithKeyMap()
        } else {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
            mToolbar.navigationIcon = null
        }
    }

    override fun onResume() {
        super.onResume()
        setUpNavigationDrawer()
        if(null!=mToolbar)
            mToolbar.title = getString(R.string.ui_audio_small)
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        EventHandler.onKeyHandler(keyCode, event)
        return super.onKeyUp(keyCode, event)
    }

    override fun getTheme(): Resources.Theme {
        val theme = super.getTheme()
        useCadillacTheme = DataPoolDataHandler.themeType.get()!!
        if (useCadillacTheme) {
            theme.applyStyle(R.style.CadillacTheme, true)
        } else {
            theme.applyStyle(R.style.NonCadillacTheme, true)
        }
        // you could also use a switch if you have many themes that could apply
        return theme
    }

    fun setBackgroundColor() {
        val parent = this.findViewById<View>(this.contentContainerId) as ViewGroup
        com.gm.audioapp.ui.adapters.setBackgroundColor(parent, resources.getColor(R.color.bg_color,
                super.getTheme()))
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        if (intent?.hasExtra("action")!!) { // Handle intents from eVS test app
            adbIntentEvent = intent.getStringExtra("action")!!
            EventHandler.processEventByTag(adbIntentEvent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        if (intent.hasExtra("action")) { // Handle intents from eVS test app
            adbIntentEvent = intent.getStringExtra("action")
        }

        // To Info3.5c mode enable and disable
        isLargeScreen()
        super.onCreate(savedInstanceState)


        // Changing layout direction to LTR and RTL
        when (DataPoolDataHandler.LHD_RHD_RTL.get()!!) {
            eLHD_RHD_RTL.RHD -> {
                setNewLocale(GMAudioApp.appContext, "uk")
                this.mDrawerLayout.layoutDirection = View.LAYOUT_DIRECTION_RTL
                this.mToolbar.layoutDirection = View.LAYOUT_DIRECTION_RTL
            }
            eLHD_RHD_RTL.LHD -> {
                setNewLocale(GMAudioApp.appContext, "en")
                window.decorView.layoutDirection = View.LAYOUT_DIRECTION_LTR
            }
            eLHD_RHD_RTL.RTL ->{
                setNewLocale(GMAudioApp.appContext, "ar")
                window.decorView.layoutDirection = View.LAYOUT_DIRECTION_RTL
            }
        }

        supportFragmentManager.addOnBackStackChangedListener {
            currentFragment = supportFragmentManager.findFragmentById(getContainerId())
        }

    }

    companion object {
        var currentFragment: Fragment?=null
    }

    /**
     * An adapter that is responsible for populating the Radio drawer with the available bands to
     * select, as well as the option for opening the manual tuner.
     */
    private inner class RadioDrawerAdapter internal constructor() : GMCarDrawerAdapter(this@BaseActivity,
            false) {
        private val mDrawerOptions = ArrayList<String>()
        override fun getActualItemCount(): Int {
            return mDrawerOptions.size
        }

        init {
            setTitle(navigationDrawerTitle())
            navigationItems = navigationDrawerItemsWithKeyMap()
            isNavigationDrawerEnabled = enableNavigationDrawer()
            setUpNavigationDrawer()
            navigationItems.forEach {
                mDrawerOptions.add(it.key) //Get items for navigation drawer
            }
        }

        override fun populateViewHolder(holder: DrawerItemViewHolder, position: Int) {
            holder.title.text = mDrawerOptions[position]
        }

        override fun onItemClick(position: Int) {
            closeDrawer()
            val screenKey = navigationItems[mDrawerOptions[position]]
            onNowPlayingDrawerChange(screenKey)
        }

    }

    fun getContainerId(): Int {
        return if (DataPoolDataHandler.isLargeScreenLayout.get()!!) {
            R.id.content_frame_layout
        } else {
            super.getContentContainerId()
        }
    }

    override fun onNavDrawerClosed() {
        //Not handling this event
    }

    /**
     * Use this method to get empty string. Specifically defined for method [navigationDrawerTitle]
     */
    protected fun emptyString() = String()

    /**
     * Use this method to get empty string. Specifically defined for method [navigationDrawerItemsWithKeyMap]
     */
    protected fun emptyHashMap(): HashMap<String, String> = HashMap()

    /**
     * Handle drawer item selection event
     * @param screenKey Event name of the selected item
     */
    private fun onNowPlayingDrawerChange(screenKey: String?) {
        EventHandler.onDrawerClick(screenKey)
    }

    override fun getRootAdapter(): CarDrawerAdapter = RadioDrawerAdapter()

    /**
     * Override to enable/disable Navigation Drawer
     *
     * @return true will enable navigation drawer; false will remove it completely (including navigation icon)
     */
    abstract fun enableNavigationDrawer(): Boolean

    /**
     * Override to return options for Navigation Drawer and respective events
     *
     * @return Map of navigation drawer items and events
     */
    abstract fun navigationDrawerItemsWithKeyMap(): HashMap<String, String>

    /**
     * Set the navigation drawer title
     * @return Title as string. If your [enableNavigationDrawer] returns false, then return [emptyString]
     */
    abstract fun navigationDrawerTitle(): String


    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(setLocale(base))
    }

    // Check if the screen is large or not
    protected fun isLargeScreen() {
        val orientation = resources.configuration.orientation
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            DataPoolDataHandler.isLargeScreenLayout.set(false)
            DataPoolDataHandler.isLargeScreenLayoutEnabled.set(false)
            return
        }
        val screenSize = resources.configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK
        when (screenSize) {
            Configuration.SCREENLAYOUT_SIZE_NORMAL,Configuration.SCREENLAYOUT_SIZE_LARGE,Configuration.SCREENLAYOUT_SIZE_XLARGE  -> {
                if (DataPoolDataHandler.isLargeScreenLayout.get()!!) DataPoolDataHandler.isLargeScreenLayout.set(true)
                DataPoolDataHandler.isLargeScreenLayoutEnabled.set(true)
            }
            else -> {
                DataPoolDataHandler.isLargeScreenLayout.set(false)
                DataPoolDataHandler.isLargeScreenLayoutEnabled.set(false)
            }
        }
    }




}