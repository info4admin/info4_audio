package com.gm.audioapp.common.customclasses

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Canvas
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.LinearLayout
import android.widget.RelativeLayout

import com.gm.audioapp.R

import java.lang.Math.PI

/**
 * @author Pavan Kandi : 8th march,2017 : used to skew complete relative layout
 * @modified Bhargava Mandapati : 14th march,2017 : worked on comments & naming conventions
 */
class SkewLinearLayout : LinearLayout {

    var degree: Float = 0.toFloat()
    var tan: Float = 0.toFloat()
    private var mContext: Context? = null

    private var angle: Float = 0.toFloat()
    internal var pie = PI

    internal var x1: Float = 0.toFloat()
    internal var w: Float = 0.toFloat()
    internal var y1: Float = 0.toFloat()
    internal var h: Float = 0.toFloat()

    internal var touchedX: Float = 0.toFloat()
    internal var touchedY: Float = 0.toFloat()
    //private PaintFlagsDrawFilter mFilter;

    constructor(context: Context) : super(context) {
        mContext = context
        //mFilter = new PaintFlagsDrawFilter(Paint.ANTI_ALIAS_FLAG, Paint.DITHER_FLAG);
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        mContext = context
        //mFilter = new PaintFlagsDrawFilter(Paint.ANTI_ALIAS_FLAG, Paint.DITHER_FLAG);

        val a = context.theme.obtainStyledAttributes(attrs, R.styleable.SkewRelativeLayout, 0, 0)

        try {
            // Getting attributes.


            degree = a.getFloat(R.styleable.SkewRelativeLayout_slopedAngle, 0f)
            //if(degree==0) degree=15;

            setAngle(degree)
        } finally {
            a.recycle()
        }
    }

    fun setAngle(degree: Float) {
        tan = Math.tan(degree * pie / 180).toFloat()
        Log.v("SkewLinearLayout", "tan::" + tan + "degree" + degree)
        angle = degree
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        mContext = context
        //mFilter = new PaintFlagsDrawFilter(Paint.ANTI_ALIAS_FLAG, Paint.DITHER_FLAG);
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        // skew the canvas over X
        //canvas.skew(0.27f, 0f);

        //canvas.skew(tan, 0f);

        if (mContext!!.resources.configuration.layoutDirection == View.LAYOUT_DIRECTION_RTL) {
            //in Right To Left layout
            degree = -tan
            canvas.skew(-tan, 0f)
        } else {
            degree = tan
            canvas.skew(tan, 0f)
        }
        initVar()
    }


    fun initVar() {
        x1 = this.x
        y1 = this.y
        w = this.width.toFloat()
        h = this.height.toFloat()
    }

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {

        touchedX = ev.x
        touchedY = ev.y
        val finalX = touchedX - touchedY * degree
        if (finalX > 0 && finalX < w) {
            ev.setLocation(finalX, touchedY)
            return super.dispatchTouchEvent(ev)
        }
        return false
    }

    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
        Log.e(TAG, "-------------onInterceptTouchEvent-----------")
        var needToStop = false
        val touchedX = ev.x
        val touchedY = ev.rawY
        val skewHeight = (touchedY / 8).toInt()
        val v = getChildAt(0).parent as View

        val skewtextleft = getChildAt(0).left + skewHeight
        val skewtextright = getChildAt(0).right + skewHeight

        if (touchedX > skewtextleft + 30) {
            Log.e("$TAG::", "in If")
            //this.onTouchEvent(ev);
            //v.setBackgroundColor(Color.BLUE);
            //return true;
            needToStop = true
        } else {
            Log.e("$TAG::", "in else")
            needToStop = false
        }
        return if (needToStop) {
            false
        } else super.onInterceptTouchEvent(ev)
    }



    companion object {
        private val TAG = "SkewLinearLayout"
    }


}
