package com.gm.audioapp.ui.activities.fragments

import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v7.widget.RecyclerView
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.FrameLayout
import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.R
import com.gm.audioapp.databinding.RadioPresetsListBinding
import com.gm.audioapp.ui.activities.NowPlayingActivity
import com.gm.media.models.DataPoolDataHandler
import com.gm.audioapp.viewmodels.EventHandler
import com.gm.audioapp.viewmodels.RegisterListener

/**
 * Displays channel specific to category
 * A fragment that functions as the display of the information relating to the particular catagorie information of current radio station.

 */
open class AMFMCategoriesInfoFragment : BaseFragment(), com.gm.audioapp.common.animations.AnimationManager.OnExitCompleteListener, RegisterListener {

    private var mRootView: View? = null
    private var mCurrentRadioCard: View? = null

    private var mPresetsList: RecyclerView? = null
    private var mAnimManager: com.gm.audioapp.common.animations.AnimationManager? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val binding = DataBindingUtil.inflate<RadioPresetsListBinding>(inflater, R.layout.ics_audio_am_station_list, container, false)

        mRootView = binding.root
        binding.let {

            it?.clickHandler = EventHandler
            it?.dataPoolHandler = DataPoolDataHandler
        }
        DataPoolDataHandler.BROWSE_SOURCE_TYPE.set(GMAudioApp.appContext.getString(R.string.categoryinfo))
        GMAudioApp.appContext.recyclerViewAdapter = null
        DataPoolDataHandler.nowPlayingBrowseListPosition.set(-1)
        mCurrentRadioCard = mRootView?.findViewById(R.id.current_radio_station_card)
        mPresetsList = mRootView?.findViewById(R.id.stations_list)
        setPresetsListMarginTop(mPresetsList)
        mPresetsList!!.visibility = View.VISIBLE
        mAnimManager = com.gm.audioapp.common.animations.AnimationManager(context!!, mRootView!!, false)
        mAnimManager?.playEnterAnimation()

        return mRootView
    }

    /**
     * Adds a [mPresetsList] to this AMFMCategoriesInfoFragment.
     * @return the new size of the mPresetsList.
     */
    private fun setPresetsListMarginTop(mPresetsList: RecyclerView?) {

        val windowManager = GMAudioApp.appContext.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = windowManager.defaultDisplay
        val dis = DisplayMetrics()
        display.getMetrics(dis)
        val marginTop = dis.densityDpi/2
        val params = FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT
        )
        val mPresetFinalHeight = GMAudioApp.appContext.resources.getDimensionPixelSize(R.dimen.car_preset_item_height)
        params.setMargins(0, marginTop + mPresetFinalHeight + 20, 0, 0)
        mPresetsList?.layoutParams = params
    }

    override fun onExitAnimationComplete() {
    }

    //close the  fragment
    private fun closeFragment(eventName: String, any: Any?) {
        DataPoolDataHandler.AMFMTUNER_CATEGORYSTATIONLIST_OBJECTLIST.clear()
        DataPoolDataHandler.amfmCategoryList.clear()
        //clear the history
        GMAudioApp.navigator.clearBackStack()
        GMAudioApp.navigator.triggerFragment(eventName, any)
    }

    //previous fragment
    private fun previousFragment() {
        val supportFragmentManager = (GMAudioApp.appContext.activityContext as NowPlayingActivity).supportFragmentManager
        supportFragmentManager.popBackStack()
    }

    override fun onRegisterListener(view: View, eventName: String, any: Any?) {
        when (view.tag.toString()) {
            "eCategoryCloseAM", "eCategoryCloseFM", "eAMFMTuneAM", "eAMFMTuneFM" -> {
                closeFragment(eventName, any)
            }
            "eCategoryInfoBackAM", "eCategoryInfoBackFM" -> {
                previousFragment()
            }
        }

    }


    override fun getFragmentTransaction(containerId: Int, fragment: Fragment): FragmentTransaction? {
        val fragmentTransaction = GMAudioApp.appContext.activityContext.supportFragmentManager.beginTransaction()
                ?.add(containerId, fragment, null)
                ?.addToBackStack(null)
        fragmentTransaction?.commit()
        return fragmentTransaction
    }

    override fun entryAnimation() {
    }

    override fun exitAnimation() {
    }

    override fun onDestroyView() {
        super.onDestroyView()
        GMAudioApp.appContext.recyclerViewAdapter=null
        DataPoolDataHandler.BROWSE_SOURCE_TYPE.set(GMAudioApp.appContext.getString(R.string.category))
    }

}
