package com.gm.audioapp.common.customclasses

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.annotation.ColorInt
import android.support.v7.widget.AppCompatButton
import android.util.AttributeSet
import com.gm.audioapp.R

/**
 * A button that represents a band the user can select in manual tuning. When this button is
 * selected, then it draws a rounded pill background around itself.
 */
class RadioBandButton : AppCompatButton {
    private var mSelectedBackground: Drawable? = null
    private var mNormalBackground: Drawable? = null

    @ColorInt
    private var mSelectedColor: Int = 0
    @ColorInt
    private var mNormalColor: Int = 0

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttrs: Int) : super(context, attrs, defStyleAttrs) {
        init(context, attrs)
    }

    /*public RadioBandButton(Context context, AttributeSet attrs, int defStyleAttrs,
            int defStyleRes) {
        super(context, attrs, defStyleAttrs, defStyleRes);
        init(context, attrs);
    }*/ // Manish Commented and nothing added for this

    /**
     * Initializes whether or not this button is initially selected.
     * @param context context of activity
     * @param attrs AttributeSet
     */
    private fun init(context: Context, attrs: AttributeSet?) {
        mSelectedBackground = context.getDrawable(R.drawable.manual_tuner_band_bg)
        mNormalBackground = context.getDrawable(R.drawable.radio_control_background)

        mSelectedColor = context.getColor(R.color.car_grey_50)
        mNormalColor = context.getColor(R.color.manual_tuner_channel_text)

        val ta = context.obtainStyledAttributes(attrs, R.styleable.RadioBandButton)

        try {
            setIsBandSelected(ta.getBoolean(R.styleable.RadioBandButton_isBandSelected, false))
        } finally {
            ta.recycle()
        }
    }

    /**
     * Sets whether or not this button has been selected. This is different from
     * [.setSelected].
     * @param selected defines button selected or not
     */
    fun setIsBandSelected(selected: Boolean) {
        if (selected) {
            setTextColor(mSelectedColor)
            background = mSelectedBackground
        } else {
            setTextColor(mNormalColor)
            background = mNormalBackground
        }
    }
}
