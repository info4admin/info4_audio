package com.gm.audioapp.ui.adapters

import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Configuration
import android.databinding.BindingAdapter
import android.databinding.ObservableArrayList
import android.graphics.Rect
import android.os.CountDownTimer
import android.support.constraint.ConstraintLayout
import android.support.constraint.Guideline
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.DisplayMetrics
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.view.WindowManager
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.R
import com.gm.media.utils.Log
import com.gm.audioapp.common.customclasses.StartSnapHelper
import com.gm.media.models.*
import com.gm.media.models.DataPoolDataHandler
import com.gm.audioapp.viewmodels.EventProcessor
import com.gm.audioapp.viewmodels.UIUpdateListener
import java.text.DecimalFormat

/**
 *
 * Created by Aswin on 3/16/2018.
 */

@BindingAdapter("bind:items", "bind:childLayout")
        /**
         * It display a list of radio station.It is used to binding the list to the corresponding adapter.
         * @param recyclerView to display the views of radio station list.
         * @param items the List of  this class.
         * @param childlayout the childLayout of this class.
         * @param progress is not used
         */
fun bindList(view: RecyclerView, items: ObservableArrayList<*>, childLayout: Int) {
    val layoutManager = LinearLayoutManager(view.context)
    view.layoutManager = layoutManager

    when (childLayout) {
        R.layout.uil_audio_row_list_am_carousel_list_view -> {
            isPaddingAlreadySet = false

            GMAudioApp.appContext.carouselRecyclerViewAdapter = RecyclerViewAdapter(view, items, childLayout)
            view.adapter = GMAudioApp.appContext.carouselRecyclerViewAdapter

            val startSnapHelper = StartSnapHelper()
            view.onFlingListener = null
            startSnapHelper.attachToRecyclerView(view)

            var frequency: String? = ""
            when (DataPoolDataHandler.AUDIOMANAGER_CHANGE_AUDIOSOURCE.get()!!) {
                NOWPLAYING_SOURCE_TYPE.AM ->
                    if (DataPoolDataHandler.AMTUNER_CURRENTSTATIONINFO_FREQUENCY.get() != null)
                        frequency = DataPoolDataHandler.AMTUNER_CURRENTSTATIONINFO_FREQUENCY.get()!!.toFloat().toString()
                else ->
                    if (DataPoolDataHandler.FMTUNER_CURRENTSTATIONINFO_FREQUENCY.get() != null)
                        frequency = DataPoolDataHandler.FMTUNER_CURRENTSTATIONINFO_FREQUENCY.get().toString()
            }

            val filterItem = (items as ArrayList<AMFMStationInfo_t>).filter { it -> it.frequency.toString() == frequency }
            if (filterItem.isNotEmpty()) {
                val index = (items as ArrayList<AMFMStationInfo_t>).indexOf(filterItem[0])
                layoutManager.scrollToPositionWithOffset(index, 0)
                GMAudioApp.appContext.carouselRecyclerViewAdapter?.updateVisibleItem(index, true)
            }


            view.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                var isScrollStareDragging = false
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    if (!isPaddingAlreadySet)
                        setRecyclerViewPadding(layoutManager, recyclerView)
                    /* if (isScrollStareDragging) {
                         recyclerView.post(Runnable { updateCarouselView(layoutManager) })
                     }*/
                    val firstVisiblePosition = layoutManager.findFirstCompletelyVisibleItemPosition()
                    if (firstVisiblePosition == 0) {
                        isScrollStareDragging = false
                        GMAudioApp.appContext.carouselRecyclerViewAdapter?.updateIsScrolling(isScrollStareDragging)
                    }
                }

                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)

                    if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                        isScrollStareDragging = false
                        recyclerView.post(Runnable { updateCarouselView(layoutManager) })
                        GMAudioApp.appContext.carouselRecyclerViewAdapter?.updateIsScrolling(isScrollStareDragging)
                    }

                    if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                        isScrollStareDragging = true
                        GMAudioApp.appContext.carouselRecyclerViewAdapter?.updateIsScrolling(isScrollStareDragging)
                    }
                }
            })
        }
        else -> {
            val adapter = RecyclerViewAdapter(view, items, childLayout)
            view.adapter = adapter
        }
    }
}

@BindingAdapter("bind:items", "bind:childLayout", "bind:progress")
fun bindList(view: RecyclerView, items: ObservableArrayList<*>, childLayout: Int, progress: Int) {

    val layoutManager = LinearLayoutManager(view.context)
    view.layoutManager = layoutManager

    if (GMAudioApp.appContext.recyclerViewAdapter != null && GMAudioApp.appContext.recyclerViewAdapter!!.itemCount > 0) {
        GMAudioApp.appContext.recyclerViewAdapter!!.updateData(items)

    } else {
        GMAudioApp.appContext.recyclerViewAdapter = RecyclerViewAdapter(view, items, childLayout)
        GMAudioApp.appContext.recyclerViewAdapter?.setPresetListView(true)
    }


    if (view.adapter == null || view.adapter!!.itemCount != GMAudioApp.appContext.recyclerViewAdapter!!.itemCount)
        view.adapter = GMAudioApp.appContext.recyclerViewAdapter!!


    /*try {
        if (GMAudioApp.appContext.visibleItemPosition <= 0) {
            val height: Int = GMAudioApp.appContext.resources.getDimensionPixelSize(R.dimen.uil_audio_row_list_am_station_list_view_LinearLayout8_height)
            GMAudioApp.appContext.visibleItemPosition = GMAudioApp.appContext.recyclerViewAdapter!!.recyclerView.height / height
        }

        val filterItem = DataPoolDataHandler.browseList.filter { it -> it.frequency.toString() == aMFMStationInfo_t.get()!!.frequency.toString() }
        if (filterItem.isNotEmpty()) {
            val index = DataPoolDataHandler.browseList.indexOf(filterItem[0])
            if (GMAudioApp.appContext.visibleItemPosition in 2..(index - 1))
                layoutManager.scrollToPosition(index)
        }

    } catch (exception: Exception) {
        exception.printStackTrace()
    }*/
}

/**
 * It update a list of radio station.
 * @param layoutManager passing layoutmanager.

 */
fun updateCarouselView(layoutManager: LinearLayoutManager) {
    val position = layoutManager.findFirstCompletelyVisibleItemPosition()
    if (position != -1) {
        GMAudioApp.appContext.carouselRecyclerViewAdapter?.updateVisibleItem(position, false)
    }
}

var isPaddingAlreadySet = false

/**
 * To set padding to the recyclerview
 * @param layoutManager passing layoutmanager.
 * @param recyclerView set the list and display the list
 */
fun setRecyclerViewPadding(layoutManager: LinearLayoutManager, recyclerView: RecyclerView) {
    val firstVisiblePosition = layoutManager.findFirstCompletelyVisibleItemPosition()
    if (firstVisiblePosition != -1) {
        val rvRect = Rect()
        recyclerView.getGlobalVisibleRect(rvRect)
        val view = layoutManager.findViewByPosition(firstVisiblePosition)
        if (view != null) {
            val height = view.measuredHeight
            val totalHeight: Int
            isPaddingAlreadySet = true
            val dis = DisplayMetrics()
            totalHeight = when {
                dis.heightPixels > 1000 -> rvRect.bottom - GMAudioApp.appContext.resources.getDimensionPixelSize(R.dimen.ics_audio_am_station_list_CardView12_height) - height
                else -> rvRect.bottom - GMAudioApp.appContext.resources.getDimensionPixelSize(R.dimen.ics_audio_am_station_list_CardView12_height1) - height / 2
            }
            recyclerView.setPadding(0, 0, 0, totalHeight)
        }
    }
}

@BindingAdapter("bind:setTPStatusSrc")
fun setTPStatusSrc(imageButton: ImageButton, tpsStatus: Int) {
    if (DataPoolDataHandler.aMFMStationInfo_t.get()?.tpStationStatus != null) {
        if (DataPoolDataHandler.aMFMStationInfo_t.get()?.tpStationStatus == eTPStationStatus.AMFM_STATION_SUPPORTS_TP && tpsStatus == 1) {
            imageButton.setImageResource(R.drawable.ic_audio_tp_on)
        } else if (DataPoolDataHandler.aMFMStationInfo_t.get()?.tpStationStatus == eTPStationStatus.AMFM_STATION_SUPPORTS_TP && tpsStatus == 0) {
            imageButton.setImageResource(R.drawable.ic_audio_tp_off)
        } else if (DataPoolDataHandler.aMFMStationInfo_t.get()?.tpStationStatus == eTPStationStatus.AMFM_STATION_DOES_NOT_SUPPORT_TP && tpsStatus == 1) {
            imageButton.setImageResource(R.drawable.ic_audio_non_tp_on)
        } else if (DataPoolDataHandler.aMFMStationInfo_t.get()?.tpStationStatus == eTPStationStatus.AMFM_STATION_DOES_NOT_SUPPORT_TP && tpsStatus == 0) {
            imageButton.setImageResource(R.drawable.ic_audio_non_tp_off)
        }
    }
}

@BindingAdapter("bind:Visibility")
fun setVisibility(view: View, boolean: Boolean) {
    when (boolean) {
        true -> view.visibility = View.VISIBLE
        else -> view.visibility = View.INVISIBLE
    }
}

@BindingAdapter("bind:showUpdateProgress")
fun setVisibilityPosition(view: View, boolean: Boolean) {
    if (boolean)
        view.visibility = View.VISIBLE
    else
        view.visibility = View.GONE
}

@BindingAdapter("bind:color")
fun textColor(textView: TextView, color: Int) {
    textView.setTextColor(color)
}

/**
 * To set the text color
 * @param textView the TextView.
 * @param color
 */
var previousBackgroundColor = 0

@BindingAdapter("bind:color")
fun setBackgroundColor(view: View, color: Int) {

    val mBackgroundColorUpdater = { animator: ValueAnimator ->
        val backgroundColor = animator.animatedValue as Int
        view.setBackgroundColor(backgroundColor)
    }
    val background_change_anim_time_ms = 450
    val colorAnimation = ValueAnimator.ofObject(ArgbEvaluator(),
            previousBackgroundColor, color)
    colorAnimation.duration = background_change_anim_time_ms.toLong()
    colorAnimation.addUpdateListener(mBackgroundColorUpdater)
    colorAnimation.start()
    previousBackgroundColor = color
}

/**
 * To set the text.
 * @param textView the TextView.
 * @param aMFMStationInfo_t the AMFMStationInfo_t.
 */
@BindingAdapter("bind:text")
fun setText(textView: TextView, aMFMStationInfo_t: AMFMStationInfo_t) {
    if (DataPoolDataHandler.AUDIOMANAGER_CHANGE_AUDIOSOURCE.get()?.name.equals(NOWPLAYING_SOURCE_TYPE.AM.name))
        textView.text = aMFMStationInfo_t.frequency.toString().split(".")[0]
    else
        textView.text = aMFMStationInfo_t.frequency.toString()
}

var previousFrequency = "0.0f"
/**
 * To set the text.
 * @param textView the TextView.
 * @param frequency the String.
 */
@BindingAdapter("bind:text")
fun setText(textView: TextView, frequency: String) {
    if (DataPoolDataHandler.AUDIOMANAGER_CHANGE_AUDIOSOURCE.get()?.name.equals(NOWPLAYING_SOURCE_TYPE.AM.name)) {
        val mAmAnimatorListener = ValueAnimator.AnimatorUpdateListener { animation ->
            textView.text = AM_FORMATTER.format(animation.animatedValue)
        }
        animateRadioChannelChange(previousFrequency.toFloat(), frequency.toFloat(), mAmAnimatorListener)
    } else {
        val mFmAnimatorListener = ValueAnimator.AnimatorUpdateListener { animation ->
            textView.text = FM_FORMATTER.format(animation.animatedValue)
        }
        animateRadioChannelChange(previousFrequency.toFloat(), frequency.toFloat(), mFmAnimatorListener)
    }
    previousFrequency = frequency
}

val FM_CHANNEL_FORMAT = "###.#"
val AM_CHANNEL_FORMAT = "####"

/**
 * The formatter for AM radio stations.
 */
val FM_FORMATTER = DecimalFormat(FM_CHANNEL_FORMAT)
/**
 * The formatter for FM radio stations.
 */
val AM_FORMATTER = DecimalFormat(AM_CHANNEL_FORMAT)

/**
 * To set the animateRadioChannelChange.
 * @param startValue the Float.
 * @param endValue the Float.
 * @param listener the AnimatorUpdateListener.
 */
private fun animateRadioChannelChange(startValue: Float, endValue: Float,
                                      listener: ValueAnimator.AnimatorUpdateListener) {
    val channel_change_duration_ms = 200
    val animator = ValueAnimator()
    animator.setObjectValues(startValue, endValue)
    animator.duration = channel_change_duration_ms.toLong()
    animator.addUpdateListener(listener)
    animator.start()
}

@BindingAdapter("bind:width")
fun setWidth(view: View, dummy: Int) {
    if (DataPoolDataHandler.isSkewEnabled.get()!!) {
        val params = view.layoutParams
        val windowManager = GMAudioApp.appContext.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = windowManager.defaultDisplay
        val displayMetrics = DisplayMetrics()
        display.getMetrics(displayMetrics)

        val vto = view.viewTreeObserver
        vto.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {

            override fun onGlobalLayout() {

                view.viewTreeObserver.removeOnGlobalLayoutListener(this)
                val height = view.measuredHeight
                val mWidth = view.measuredWidth
                val width = (height * 0.275).toInt()
                params.width = mWidth - width
                if (view.id == R.id.controlButtonsLayout)
                    GMAudioApp.appContext.skewNowPlayingWidth = params.width
                view.layoutParams = params

            }
        })
    }
}




@BindingAdapter("bind:textwidth")
fun setwidth(view: TextView, dummy: Int) {
    if (DataPoolDataHandler.isSkewEnabled.get()!!) {
        val params = view.layoutParams
        view.measure(0,0)
        val textwidth = view.measuredWidth + 40
        params.width = textwidth
    }
    else{
        val params = view.layoutParams
        view.measure(0,0)
        val textwidth = view.measuredWidth
        params.width = textwidth
    }
}






var myCountDownTimer: MyCountDownTimer? = MyCountDownTimer(1000, 1000)
var isOnFInishCalled: Boolean = false
var UPDOWN: String = ""
/**
 * To set the setTouchListener.
 * @param self the ImageButton.
 * @param textAbove the String.
 */
@SuppressLint("ClickableViewAccessibility")
@BindingAdapter("app:onClickORonLongPress")
fun setTouchListener(self: ImageButton, textAbove: String) {

    self.setOnTouchListener(View.OnTouchListener { v, motionEvent ->
        if (DataPoolDataHandler.audioControlDisabled.get()!!) {
            showQuickNotice(GMAudioApp.appContext.resources.getString(R.string.audio_action_not_supported))
            return@OnTouchListener true
        }
        when (motionEvent.action) {
            MotionEvent.ACTION_DOWN -> {
                self.isPressed = true
                when (v.id) {
                    R.id.img_audio_timeshift_leftseek -> {
                        UPDOWN = "DOWN"
                    }
                    R.id.img_audio_timeshift_rightseek -> {
                        UPDOWN = "UP"
                    }
                }
                UPDOWN = textAbove
                myCountDownTimer!!.start()
            }
            MotionEvent.ACTION_UP -> {
                self.isPressed = false
                myCountDownTimer!!.cancel()
                if (isOnFInishCalled) {
                    updateHardSeekStation()
                } //main if close on finished
                else {
                    val eNumType: eAMFMSeekType = when {
                        UPDOWN.equals("UP") -> eAMFMSeekType.AMFM_SEEK_TYPE_SEEKUP
                        else -> eAMFMSeekType.AMFM_SEEK_TYPE_SEEKDOWN
                    }
                    if (DataPoolDataHandler.AUDIOMANAGER_CHANGE_AUDIOSOURCE.get()?.name == NOWPLAYING_SOURCE_TYPE.AM.name)
                        EventProcessor.triggerEvent(null, "process", "onAMFM_REQ_AMSEEKSTATION", eNumType, "NowPlayingActivity", true)
                    else if (DataPoolDataHandler.AUDIOMANAGER_CHANGE_AUDIOSOURCE.get()?.name == NOWPLAYING_SOURCE_TYPE.FM.name)
                        EventProcessor.triggerEvent(null, "process", "onAMFM_REQ_FMSEEKSTATION", eNumType, "NowPlayingActivity", true)
                } //else close
            }
        }
        true
    })
}

/**
 * This class have MyCountDownTimer information.
 * @property millisInFuture the Long.
 * @property countDownInterval the Long.
 */
//our own countdown timer class
class MyCountDownTimer(millisInFuture: Long, countDownInterval: Long) : CountDownTimer(millisInFuture, countDownInterval) {

    override fun onTick(millisUntilFinished: Long) {
    }

    override fun onFinish() {
        updateHardSeekStation()
        isOnFInishCalled = true
        myCountDownTimer!!.start()
    }
}

/**
 * To update eHardSeekStation.
 */
//common method for update hard seek station
fun updateHardSeekStation() {
    val eNumType: eAMFMSeekType = when {
        UPDOWN.equals("UP") -> eAMFMSeekType.AMFM_SEEK_TYPE_FASTSEEKUP
        else -> eAMFMSeekType.AMFM_SEEK_TYPE_FASTSEEKDOWN
    }
    if (DataPoolDataHandler.AUDIOMANAGER_CHANGE_AUDIOSOURCE.get()?.name == NOWPLAYING_SOURCE_TYPE.AM.name)
        EventProcessor.triggerEvent(null, "process", "onAMFM_REQ_AMHARDSEEKSTATION", eNumType, "NowPlayingActivity", true)
    else if (DataPoolDataHandler.AUDIOMANAGER_CHANGE_AUDIOSOURCE.get()?.name == NOWPLAYING_SOURCE_TYPE.FM.name)
        EventProcessor.triggerEvent(null, "process", "onAMFM_REQ_FMHARDSEEKSTATION", eNumType, "NowPlayingActivity", true)
}

/**
 * To show QuickNotice.
 * @param message passing message.
 */
fun showQuickNotice(message: String) {
    Toast.makeText(GMAudioApp.appContext, message, Toast.LENGTH_SHORT).show()
}

@BindingAdapter("bind:marginStart", "bind:marginEnd", "bind:marginTop")
fun setInfoCurvedMargins(view: View, marginStart: Int, marginEnd: Int, marginTop: Int) {
    Log.v("BindAdapters", marginStart.toString() + "----" + marginEnd)
    val layoutParams = view.layoutParams as ViewGroup.MarginLayoutParams
    layoutParams.marginStart = marginStart
    layoutParams.marginEnd = marginEnd
    layoutParams.topMargin = marginTop
    view.layoutParams = layoutParams
}

/**
 * To set the setTouchListener.
 * @param view the ImageView.
 * @param value the Boolean.
 */
@BindingAdapter("touchListener")
fun setTouchListener(view: View, value: Boolean) {
    Log.v("setTouchListener", "View ----" + view)
    // SetTouchListener for StationList Progress Cancel.
    // Onclick was getting called inappropriately, hence handling the cancel functionality through touch
    view.setOnTouchListener { view, motionEvent ->
        when (motionEvent.action) {
            MotionEvent.ACTION_DOWN -> {
                val currentFragment = GMAudioApp.appContext.activityContext.supportFragmentManager.findFragmentById(GMAudioApp.appContext.activityContext.getContainerId())
                if (currentFragment is UIUpdateListener) {
                    currentFragment.onUIUpdateListener(view)
                }
            }
            MotionEvent.ACTION_UP -> {
                // Do nothing here. Just consume the event
            }
        }
        true
    }
}

//To set Manual tune height percentage based on orientation.
@BindingAdapter("bind:guideline_percentage")
fun setGuideLinePercentage(view: View, dummy: Int) {
    val params = (view as Guideline).getLayoutParams() as ConstraintLayout.LayoutParams
    val orientation = GMAudioApp.appContext.activityContext.resources.configuration.orientation
    if (orientation == Configuration.ORIENTATION_PORTRAIT)
        params.guidePercent = 0.5f
    else
        params.guidePercent = 0.2f
    view.layoutParams = params
}

@BindingAdapter("bind:slopedAngle")
fun setSloppedAngle(view: View, angle: Float) {
    if (DataPoolDataHandler.isSkewEnabled.get()!!) {
        if (view is com.gm.audioapp.common.customclasses.SkewLinearLayout) view.setAngle(angle)
        else if (view is com.gm.audioapp.common.customclasses.SkewRelativeLayout) view.setAngle(angle)
        else if (view is com.gm.audioapp.common.customclasses.SkewConstraintLayout) view.setAngle(angle)
        else if (view is com.gm.audioapp.common.customclasses.SkewTextView) view.setAngle(angle)
        else if (view is com.gm.audioapp.common.customclasses.SkewMaterialButton) view.setAngle(angle)
    }
}
