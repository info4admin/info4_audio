package com.gm.audioapp.ui.navigator

import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.R
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader

/**
 * This screen maps the screen 'tag' or event name with fully qualified class name or action name.
 * It uses map to hold event name and there corresponding screen name or class name.
 *
 * @author Aswin
 */

object ScreenMapper {

    private const val basePackage = "com.gm.audioapp"

    private const val action = "$basePackage.action"

    private const val fragment = "$basePackage.ui.activities.fragments"

    /**
     * contains key/value pairs as (event name)/(action name)
     */
    var eventTable: Map<String, String> = HashMap()
    /**
     * contains key/value pairs as (event name)/(screen name)
     */
    var eventFragmentTable: Map<String, String> = HashMap()

    /**
     * It initialises screen mapper object
     */
    init {
        eventTable = getEventTable()
        eventFragmentTable = getEventFragmentTable()
    }

    /**
     * Constructs HashMap that contains the event name as key and action name as value
     *
     * @return HashMap
     */
    private fun getEventTable(): HashMap<String, String> {
        val eventTable = HashMap<String, String>()
        val jsonEventObject = getJsonObject()
        val iterator = jsonEventObject.keys()
        iterator.forEach {
            eventTable[it] = "$action.${jsonEventObject.getString(it)}"
        }
        return eventTable
    }

    /**
     * Constructs HashMap that contains the event name as key and fragment as value
     *
     * @return HashMap
     */
    private fun getEventFragmentTable(): HashMap<String, String> {
        val eventTable = HashMap<String, String>()
        val jsonEventObject = getJsonObject()
        val iterator = jsonEventObject.keys()
        iterator.forEach {
            eventTable[it] = jsonEventObject.getString(it)
            //eventTable[it] = "$fragment.${jsonEventObject.getString(it)}"
        }
        return eventTable
    }

    /**
     * Extracts event data from R.raw.events
     *
     * @return JSONObject
     */
    private fun getJsonObject(): JSONObject {
        val stream = GMAudioApp.appContext.resources.openRawResource(R.raw.events)
        return JSONObject(getJsonString(stream)).getJSONObject("events_action_map")
    }

    /**
     * Converts input stream to string
     *
     * @param stream input stream to be read
     * @return string
     */
    private fun getJsonString(stream: InputStream): String {
        val strBuilder = StringBuilder()
        var jsonString: String? = null
        val bfReader = BufferedReader(InputStreamReader(stream,"UTF-8"))
        while ({ jsonString = bfReader.readLine(); jsonString }() != null) {
            strBuilder.append(jsonString)
        }
        return strBuilder.toString()
    }

}