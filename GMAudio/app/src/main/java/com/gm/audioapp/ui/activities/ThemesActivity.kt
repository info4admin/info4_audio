package com.gm.audioapp.ui.activities

import android.databinding.DataBindingUtil
import android.os.Bundle
import com.gm.audioapp.R
import com.gm.audioapp.databinding.ActivityThemeBinding
import com.gm.media.models.DataPoolDataHandler
import com.gm.audioapp.viewmodels.EventHandler

/**
 * This screen allows user to select cadillac and non cadillac themes
 *
 * @author Aswin on 3/12/2018.
 */


class ThemesActivity : BaseActivity() {

    override fun enableNavigationDrawer(): Boolean = false

    override fun navigationDrawerItemsWithKeyMap(): HashMap<String, String> = emptyHashMap()

    override fun navigationDrawerTitle(): String = emptyString()

    var binding: ActivityThemeBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var binding: ActivityThemeBinding = DataBindingUtil.setContentView(this, R.layout.ics_themes)
        binding.let {

            it.clickHandler = EventHandler
            it.dataPoolHandler = DataPoolDataHandler
        }
    }

}