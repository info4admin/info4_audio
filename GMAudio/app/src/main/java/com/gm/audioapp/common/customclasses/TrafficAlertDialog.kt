package com.gm.audioapp.common.customclasses

import android.app.Dialog
import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Window
import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.R
import com.gm.audioapp.databinding.TrafficAlertBinding
import com.gm.media.models.DataPoolDataHandler
import com.gm.audioapp.viewmodels.EventHandler

abstract class SuperDialog(context: Context?): Dialog(context)

class TrafficAlertDialog : SuperDialog(GMAudioApp.appContext.activityContext) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)

        val binding: TrafficAlertBinding = DataBindingUtil.inflate(
                LayoutInflater.from(context), R.layout.ics_audio_am_fm_traffic_alert_dialog, null, false)

        binding.let {
            it.dataPoolHandler = DataPoolDataHandler
            it.clickHandler = EventHandler
        }

        setContentView(binding.root)
    }
}