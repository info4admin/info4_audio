package com.gm.audioapp.ui.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.graphics.Point
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import com.android.car.view.PagedListView
import com.gm.audioapp.BR
import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.R
import com.gm.media.utils.ManualTunerController
import com.gm.media.models.AMFMStationInfo_t
import com.gm.media.models.DataPoolDataHandler
import com.gm.media.models.DataPoolDataHandler.isInfoCurvedView
import com.gm.audioapp.viewmodels.EventHandler


@Suppress("PrivatePropertyName")
/**
 * This adapter binds List data to any Recycler view. For preset list, call [setPresetListView] with true.
 * @author Aswin on 3/22/2018.
 */
class RecyclerViewAdapter() : RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHolder>(), PagedListView.ItemCap {

    var size = 0
    private var isVisible: Boolean = false
    private var isTuned: Boolean = true
    private var currentVisible: Int = 0
    var isScrolling = false

    /** To differentiate if this adapter is for Preset or Carousel list. Default is for Carousel list */
    private var isPreset = false
    private val PRESETS_VIEW_TYPE_STATIONLIST = 0
    private val PRESETS_VIEW_TYPE_STATIONLIST_UPDATE = -2
    private var mListObjects: List<*>? = null
    private var mChildLayoutId: Int = 0
    lateinit var recyclerView: RecyclerView

    constructor(view: View, mListObjects: List<*>?, mChildLayoutId: Int) : this() {
        this.mListObjects = mListObjects!!
        this.mChildLayoutId = mChildLayoutId
        if(view is RecyclerView)
        this.recyclerView = view
    }


    /**
     * This class is the holder for  the views.
     * @param binding observable used to bind data to view
     */
    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    inner class RecyclerViewHolder(// each data item is just a string in this case
            private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Any, position: Int, isVisible: Boolean,isPageList:Boolean) {
            binding.setVariable(BR.obj, item)
            binding.setVariable(BR.clickHandler, EventHandler)
            binding.setVariable(BR.position, position)
            binding.setVariable(BR.dataPoolHandler, DataPoolDataHandler)
            if(isPageList)
            {
                binding.setVariable(BR.isactive, isVisible)
                try {
                    if (DataPoolDataHandler.BROWSE_SOURCE_TYPE.get().equals(GMAudioApp.appContext.getString(R.string.category))) {
                        val amfm_frequency: View = binding.root.findViewById(R.id.preset_station_channel)
                        amfm_frequency.visibility = View.GONE
                    }

                } catch (exception: Exception) {
                    exception.printStackTrace()
                }
                setPresetCardBackground()
                binding.executePendingBindings()
            }
            else {
                binding.setVariable(BR.isVisible, isVisible)
                binding.executePendingBindings()
            }

        }

        // ########################### Preset List ##################################
        //var isWidthSet: Boolean = false // to set width for row item only once
        /**
         * Sets the appropriate background on the card containing the preset information. The cards
         * need to have rounded corners depending on its position in the list and the number of items
         * in the list.
         * @param itemCount total no of item in list
         */
        private fun setPresetCardBackground() {

            //if (!isWidthSet) {
                val size = Point()

                val display = (GMAudioApp.appContext.getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay
                // Correctly set the background for each card. Only the top and last card should
                // have rounded corners.
                display.getRealSize(size)
                val mScreenWidth = size.x
                val width = if (isInfoCurvedView.get()!!) mScreenWidth / 6 else mScreenWidth / 2
                itemView.layoutParams.width = width
               // isWidthSet = true
            //}
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewAdapter.RecyclerViewHolder {
        val viewHolder: RecyclerView.ViewHolder
        val inflater = LayoutInflater.from(parent.context)

        viewHolder = when (viewType) {
            PRESETS_VIEW_TYPE_STATIONLIST_UPDATE -> {
                val binding = DataBindingUtil.inflate<ViewDataBinding>(inflater!!, R.layout.uil_audio_browse_station_list_update, parent, false)
                RecyclerViewHolder(binding = binding)
            }
            else -> {
                val binding = DataBindingUtil.inflate<ViewDataBinding>(inflater!!, mChildLayoutId, parent, false)
                RecyclerViewHolder(binding = binding)
            }

        }
        size = mListObjects!!.size
        return viewHolder

    }

    override fun onBindViewHolder(holder: RecyclerViewAdapter.RecyclerViewHolder, position: Int) {
        if (isPreset) {
            val station = getItem(position)
            var isActiveStation = false
            if (station!! is AMFMStationInfo_t) {
                isActiveStation = (station as AMFMStationInfo_t).frequency.toString() ==
                        DataPoolDataHandler.aMFMStationInfo_t.get()!!.frequency.toString()
                if (isActiveStation) DataPoolDataHandler.nowPlayingBrowseListPosition.set(position)
            }

            holder.bind(station, position, isActiveStation,true)
        } else {
            val item = mListObjects!![position]
            if (::recyclerView.isInitialized) {
                isVisible = currentVisible == position
                holder.bind(item!!, position, isVisible,false)
            }
        }
    }

    override fun getItemCount(): Int {
        return mListObjects!!.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        if (isPreset) {
            if (mListObjects!![position] is AMFMStationInfo_t) {
                if ((mListObjects!![position] as AMFMStationInfo_t).stationName ==
                        GMAudioApp.appContext.getString(R.string.station_list_update) ||
                        (mListObjects!![position] as AMFMStationInfo_t).
                                stationName == GMAudioApp.appContext.getString(R.string.categories))
                    return PRESETS_VIEW_TYPE_STATIONLIST_UPDATE
            } else return PRESETS_VIEW_TYPE_STATIONLIST
        } else
            return position

        return -1
    }

    /**
     * To update the visible item of the radio station list.
     * @param position passing visible item position.
     * @param tuned tuned boolean
     */
    fun updateVisibleItem(position: Int, tuned: Boolean) {
        currentVisible = position
        this.isTuned = tuned
        if (!isTuned) {
            if (mListObjects != null && !mListObjects!!.isEmpty()) {
                val item = mListObjects!![position] as AMFMStationInfo_t
                EventHandler.initEvent(ManualTunerController.eventName, item.frequency.toString())
            }
        }
        notifyDataSetChanged()
    }

    /**
     * To update the isScrolling boolean of the station list.
     * @param isScrolling passing boolean.

     */
    fun updateIsScrolling(isScrolling: Boolean) {
        this.isScrolling = isScrolling
    }

    override fun setMaxItems(maxItems: Int) {
        // No-op. A PagedListView needs the ItemCap interface to be implemented. However, the
        // list of presets should not be limited.
    }

    fun setPresetListView(isPreset: Boolean) {
        this.isPreset = isPreset
    }

    /**
     * This is called to refresh the list data
     * @param mList list containing object to be binded to views row
     * @param progress progress of manaul update
     * @param showUpdateProgress TRue/False to show/hide progress bar respectively
     * @param sourceType source type NOWPLAYING_SOURCE_TYPE.AM/Fm
     */
    fun updateData(mList: List<*>?) {
        mListObjects = mList!!
    }

    /**
     * This returns the object to be binded for the given position
     * @param position position of row in the list view
     * @return AMFMStationInfo_t object with default values
     */
    private fun getItem(position: Int): Any? {
        return mListObjects!![position]
    }
}

