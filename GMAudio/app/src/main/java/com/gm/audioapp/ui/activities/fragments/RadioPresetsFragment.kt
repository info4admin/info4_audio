package com.gm.audioapp.ui.activities.fragments

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v7.widget.RecyclerView
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.FrameLayout
import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.R
import com.gm.audioapp.databinding.RadioPresetsListBinding
import com.gm.media.models.Builder
import com.gm.media.models.DataPoolDataHandler
import com.gm.audioapp.viewmodels.EventHandler
import com.gm.audioapp.viewmodels.RegisterListener
import com.gm.audioapp.viewmodels.UIUpdateListener

/**
 * A screen that displays all the detected station. It has Controls to start manual update.
 */
open class RadioPresetsFragment : BaseFragment(), com.gm.audioapp.common.animations.AnimationManager.OnExitCompleteListener, RegisterListener,UIUpdateListener {

    //private val TAG = "RadioPresetsFragment"

    private val ANIM_DURATION_MS = 200
    private var mRootView: View? = null
    private var mCurrentRadioCard: View? = null
    private var controlButtonsLayout: View? = null

    private var mPresetsList: RecyclerView? = null
    private var mAnimManager: com.gm.audioapp.common.animations.AnimationManager? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<RadioPresetsListBinding>(inflater,
                R.layout.ics_audio_am_station_list, container, false)
        mRootView = binding.root
        binding.let {
            it?.clickHandler = EventHandler
            it?.dataPoolHandler = DataPoolDataHandler
        }

        DataPoolDataHandler.BROWSE_SOURCE_TYPE.set(GMAudioApp.appContext.getString(R.string.radio))
        mCurrentRadioCard = mRootView?.findViewById(R.id.current_radio_station_card)
        controlButtonsLayout = mRootView?.findViewById(R.id.controlButtonsLayout)
        GMAudioApp.appContext.recyclerViewAdapter = null
        GMAudioApp.appContext.carouselRecyclerViewAdapter = null
        if (DataPoolDataHandler.isSkewEnabled.get()!!) {
            val params = controlButtonsLayout!!.layoutParams
            params.width = GMAudioApp.appContext.skewNowPlayingWidth
            controlButtonsLayout!!.layoutParams = params
        }
        mPresetsList = mRootView?.findViewById(R.id.stations_list)
        setPresetsListMarginTop(mPresetsList)
        DataPoolDataHandler.amfmCategoryList.clear()
        DataPoolDataHandler.AMFMTUNER_CATEGORYSTATIONLIST_OBJECTLIST.clear()
        mAnimManager = com.gm.audioapp.common.animations.AnimationManager(context!!, mRootView!!, true)
        mAnimManager?.playEnterAnimation()

        return mRootView
    }

    /**
     * sets margin to given PageListView
     *
     * @param mPresetsList PagedListView whose margin is to be set
     */
    private fun setPresetsListMarginTop(mPresetsList: RecyclerView?) {
        val windowManager = GMAudioApp.appContext.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = windowManager.defaultDisplay
        val dis = DisplayMetrics()
        display.getMetrics(dis)
        val marginTop = if(DataPoolDataHandler.isInfoCurvedView.get()!!)  dis.densityDpi else dis.densityDpi/2

        val params = FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT
        )
        val mPresetFinalHeight = GMAudioApp.appContext.resources.getDimensionPixelSize(R.dimen.car_preset_item_height)
        params.setMargins(0, marginTop + mPresetFinalHeight + 20, 0, 0)
        mPresetsList?.layoutParams = params
    }

    private fun fadeOutContent() {
        val containerAlphaAnimator = ObjectAnimator.ofFloat(mCurrentRadioCard, View.ALPHA, 0f)
        containerAlphaAnimator.duration = ANIM_DURATION_MS.toLong()
        containerAlphaAnimator.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {}
            override fun onAnimationEnd(animation: Animator) {
                mCurrentRadioCard?.visibility = View.GONE
            }

            override fun onAnimationCancel(animation: Animator) {}
            override fun onAnimationRepeat(animation: Animator) {}
        })

        val presetListAlphaAnimator = ObjectAnimator.ofFloat(mPresetsList, View.ALPHA, 0f)
        presetListAlphaAnimator.duration = ANIM_DURATION_MS.toLong()
        presetListAlphaAnimator.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {}
            override fun onAnimationEnd(animation: Animator) {
                mPresetsList?.visibility = View.GONE
            }
            override fun onAnimationCancel(animation: Animator) {}
            override fun onAnimationRepeat(animation: Animator) {}
        })

        val animatorSet = AnimatorSet()
        animatorSet.playTogether(containerAlphaAnimator, presetListAlphaAnimator)
        animatorSet.start()
    }

    private fun fadeInContent() {
        // Only the current radio card needs to be faded in as that is the only part
        // of the fragment that will peek over the manual tuner.
        val containerAlphaAnimator = ObjectAnimator.ofFloat(mCurrentRadioCard, View.ALPHA, 1f)
        containerAlphaAnimator.duration = ANIM_DURATION_MS.toLong()
        containerAlphaAnimator.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {
                mCurrentRadioCard?.visibility = View.VISIBLE
            }
            override fun onAnimationEnd(animation: Animator) {}
            override fun onAnimationCancel(animation: Animator) {}
            override fun onAnimationRepeat(animation: Animator) {}
        })

        val presetListAlphaAnimator = ObjectAnimator.ofFloat(mPresetsList, View.ALPHA, 1f)
        presetListAlphaAnimator.duration = ANIM_DURATION_MS.toLong()
        presetListAlphaAnimator.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {
                mPresetsList?.visibility = View.GONE
            }
            override fun onAnimationEnd(animation: Animator) {}
            override fun onAnimationCancel(animation: Animator) {}
            override fun onAnimationRepeat(animation: Animator) {}
        })

        val animatorSet = AnimatorSet()
        animatorSet.playTogether(containerAlphaAnimator, presetListAlphaAnimator)
        animatorSet.start()
    }

    //close the fragment
    private fun closeFragment() {
        mAnimManager!!.playExitAnimation(this)
    }

    //next fragment
    private fun nextFragment(eventName: String, any: Any?) {
        GMAudioApp.navigator.triggerFragment(eventName, any)
    }

    //previous fragment
    private fun previousFragment() {
        GMAudioApp.appContext.activityContext.supportFragmentManager.popBackStack()
    }


    override fun onRegisterListener(view: View, eventName: String, any: Any?) {
        when (view.tag.toString()) {
            "eNowPlayingAM", "eNowPlayingFM" -> {
                closeFragment()
            }
            "eNowPlayingBrowse1" -> {
                nextFragment(eventName, any)
            }
        }
    }


    override fun getFragmentTransaction(containerId: Int, fragment: Fragment): FragmentTransaction? {
        val fragmentTransaction = GMAudioApp.appContext.activityContext.supportFragmentManager.beginTransaction()
                ?.add(containerId, fragment, null)
                ?.addToBackStack(null)
        fragmentTransaction?.commit()
        return fragmentTransaction
    }

    override fun entryAnimation() {
        fadeInContent()
    }

    override fun exitAnimation() {
        fadeOutContent()
    }

    override fun onExitAnimationComplete() {
        GMAudioApp.appContext.recyclerViewAdapter = null
        previousFragment()
        GMAudioApp.navigator.clearBackStack()
    }


    override fun onResume() {
        super.onResume()
        if(DataPoolDataHandler.browseList.size>0 && (!DataPoolDataHandler.browseList[0].stationName.equals(GMAudioApp.appContext.getString(R.string.categories)) || !DataPoolDataHandler.browseList[1].stationName.equals(GMAudioApp.appContext.getString(R.string.station_list_update)))) {
            DataPoolDataHandler.browseList.add(0, Builder().stationName(GMAudioApp.appContext.getString(R.string.categories)).build())
            DataPoolDataHandler.browseList.add(1, Builder().stationName(GMAudioApp.appContext.getString(R.string.station_list_update)).build())
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        GMAudioApp.appContext.recyclerViewAdapter = null
        if (DataPoolDataHandler.browseList.size > 0 && DataPoolDataHandler.browseList[0].frequency == 0f && (DataPoolDataHandler.browseList[0].stationName.equals(GMAudioApp.appContext.getString(R.string.categories)) || DataPoolDataHandler.browseList[1].stationName.equals(GMAudioApp.appContext.getString(R.string.station_list_update)))) {
            DataPoolDataHandler.browseList.removeAt(0)
            DataPoolDataHandler.browseList.removeAt(0)
        }
    }

    override fun onUIUpdateListener(view: View) {
        when (view.tag.toString()) {
            "eAMCancelUpdate" -> {
                EventHandler.onClickHandler(view)
            }
        }
    }

}
