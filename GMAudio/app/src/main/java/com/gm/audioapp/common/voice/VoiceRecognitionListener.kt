package com.gm.audioapp.common.voice

import android.os.Bundle
import android.speech.RecognitionListener
import android.speech.SpeechRecognizer

/**
 * Used for receiving notifications from the SpeechRecognizer when the recognition related events occur
 */
object VoiceRecognitionListener : RecognitionListener {

    private lateinit var listener: IVoiceControl
    /**
     * To set IVoiceControl
     * @param listener IVoiceControl
     */
    fun setListener(listener: IVoiceControl) {
        VoiceRecognitionListener.listener = listener
    }

    /**
     * To process the voice based commands
     * @voiceCommands voice commands
     */
    private fun processVoiceCommands(vararg voiceCommands: String) {
        listener.processVoiceCommands(*voiceCommands)
    }

    // This method will be executed when voice commands were found
    override fun onResults(data: Bundle) {
        val matches = data.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
        val commands: Array<String>
        for (command in matches) {
            println(command)
        }
        commands = matches.toTypedArray()
        processVoiceCommands(*commands)
    }

    // User starts speaking
    override fun onBeginningOfSpeech() {
        println("Starting to listen")
    }

    override fun onBufferReceived(buffer: ByteArray) {}

    // User finished speaking
    override fun onEndOfSpeech() {
        println("Waiting for result...")
    }

    // If the user said nothing the service will be restarted
    override fun onError(error: Int) {
        listener.restartListeningService()
    }

    override fun onEvent(eventType: Int, params: Bundle) {}

    override fun onPartialResults(partialResults: Bundle) {}

    override fun onReadyForSpeech(params: Bundle) {}

    override fun onRmsChanged(rmsdB: Float) {}

}