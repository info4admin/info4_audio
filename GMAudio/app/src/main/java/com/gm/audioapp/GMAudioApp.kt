package com.gm.audioapp

//import gm.cluster.overlay.QuickNotice
import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.media.MediaPlayer
import android.os.Bundle
import android.support.multidex.MultiDex
import android.view.View
import com.gm.audioapp.ui.activities.BaseActivity
import com.gm.audioapp.ui.activities.NowPlayingActivity
import com.gm.audioapp.ui.activities.fragments.*
import com.gm.audioapp.ui.adapters.RecyclerViewAdapter
import com.gm.audioapp.ui.navigator.ActivityNavigator
import com.gm.audioapp.viewmodels.ResponseListener
import com.gm.audioapp.viewmodels.Utility.getLanguage
import com.gm.audioapp.viewmodels.Utility.setLocale
import com.gm.media.models.DataPoolDataHandler
import com.gm.media.models.eLHD_RHD_RTL
import java.util.*


/**
 * Base Class for maintaining global application state.
 * It initialises global object that lives through out the application life.
 * To Create singleton object for VoiceLifeCycle and assigning context of current activity.
 * Initialising the fragments and classes
 */

class GMAudioApp : Application(), Application.ActivityLifecycleCallbacks {

    lateinit var activityContext: BaseActivity
    lateinit var themeChange: NowPlayingActivity
    val nowPlayingFragment = NowPlayingFragment()
    /**
     *  It holds the instance of CarouselRecyclerViewAdapter
     */
    var carouselRecyclerViewAdapter: RecyclerViewAdapter? = null
    var recyclerViewAdapter: RecyclerViewAdapter? = null
    // var voiceLifeCycle: VoiceLifeCycle ?= null // TODO: Need to uncomment after demo
    var skewNowPlayingWidth:Int = 0
    var visibleItemPosition: Int = -1

    override fun onCreate() {
        super.onCreate()
        appContext = this
        ResponseListener
        setLayoutDirection()
        registerActivityLifecycleCallbacks(this)
        DataPoolDataHandler.themeType.set(true)
        when (getLanguage(appContext)) {
            "en" -> {
                DataPoolDataHandler.LHD_RHD_RTL.set(eLHD_RHD_RTL.LHD)
            }
            "uk" -> {
                DataPoolDataHandler.LHD_RHD_RTL.set(eLHD_RHD_RTL.RHD)
            }
            "ar" ->{
                DataPoolDataHandler.LHD_RHD_RTL.set(eLHD_RHD_RTL.RTL)
            }
        }
    }
    companion object {
        lateinit var appContext: GMAudioApp
        var navigator= ActivityNavigator()
        var useCadillacTheme: Boolean? = false

        var fragmentMgr = FragmentPoolManager
        var mMediaPlayer: MediaPlayer?=null
        var IS_RTL = true

        init {
            Thread{
                fragmentMgr.add(NowPlayingFragment(), ManualTunerFragment(), RadioPresetsFragment(), AMFMCategoriesFragment(), AMFMCategoriesInfoFragment())
            }.start()
        }
    }

    /**
     * Creating singleton object for VoiceLifeCycle class
     * @param activity current activity
     * @return VoiceLifeCycle object
     * TODO: Need to uncomment after demo
     */
    /*private fun getVoiceLifeCycle(activity: Activity?): VoiceLifeCycle {
        if (voiceLifeCycle == null) {
            voiceLifeCycle = VoiceLifeCycle(activity)
        }
        return voiceLifeCycle!!
    }*/

    /**
     * If app is in background, stopping the voice recognization functionality
     * @param activity current activity
     */
    override fun onActivityPaused(activity: Activity?) {
        // TODO: Need to uncomment after demo
        /*if (voiceLifeCycle != null) {
            voiceLifeCycle!!.stopListening()
            voiceLifeCycle = null
        }*/
    }

    /**
     * If activity resumed, starting the  voice recognization functionality
     * @param activity current activity
     */
    override fun onActivityResumed(activity: Activity?) {

        /*// TODO: Need to uncomment after demo
        getVoiceLifeCycle(activity)
        voiceLifeCycle!!.requestPermission()*/
        activityContext = activity!! as BaseActivity

    }

    override fun onActivityStarted(activity: Activity?) {
    }

    override fun onActivityDestroyed(activity: Activity?) {
    }

    override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {
    }

    override fun onActivityStopped(activity: Activity?) {
    }

    override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {
        //getVoiceLifeCycle(activity)
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(setLocale(base))
        MultiDex.install(this)
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        setLocale(this)
    }

    /**
     * To change Layout direction
     */
    fun changeLayoutDirection(localString: String) {
        val locale = Locale(localString)
        Locale.setDefault(locale)
        val config = resources.configuration
        config.setLayoutDirection(locale)
        resources.updateConfiguration(config, resources.displayMetrics)
    }

     private fun setLayoutDirection() {
        val resources = resources
        val config = resources.configuration
        val direction = config.layoutDirection
         IS_RTL = direction == View.LAYOUT_DIRECTION_LTR
    }

    /**
     * To show the quick notification
     * @param message notification description
     * @param onClickListener  View.OnClickListener object of current activity
     */
    /*fun showQuickNotice(message: String, onClickListener: View.OnClickListener) {
        val quickNotice = QuickNotice(this)
        quickNotice.setContent(message)
        quickNotice.duration = QuickNotice.LENGTH_LONG
        quickNotice.show()
    }*/
}