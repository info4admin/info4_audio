package com.gm.audioapp.ui.activities.fragments

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.R
import com.gm.media.utils.ManualTunerController
import com.gm.audioapp.databinding.ManualTunerBinding
import com.gm.audioapp.ui.activities.NowPlayingActivity
import com.gm.audioapp.ui.activities.NowPlayingActivity.Companion.CONTENT_FRAGMENT_TAG
import com.gm.audioapp.ui.activities.NowPlayingActivity.Companion.MANUAL_TUNER_BACKSTACK
import com.gm.audioapp.ui.activities.fragments.FragmentPoolManager.getFragmentByTag
import com.gm.media.models.DataPoolDataHandler
import com.gm.audioapp.viewmodels.EventHandler
import com.gm.audioapp.viewmodels.RegisterListener


/**
 * This screen allows the user to directly enter any station/channel
 * they would like to tune to on the keypad
 */
class ManualTunerFragment : BaseFragment(), RegisterListener {

    private var mRootView: View? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var binding = DataBindingUtil.inflate<ManualTunerBinding>(inflater, R.layout.ics_audio_am_direct_tune_keypad, container, false)

        mRootView = binding.root
        binding.let {

            it?.clickHandler = EventHandler
            it?.dataPoolHandler = DataPoolDataHandler
            it?.manualTune = ManualTunerController
        }

        ManualTunerController.validateSourceType(DataPoolDataHandler.AUDIOMANAGER_CHANGE_AUDIOSOURCE.get()!!.name)

        return mRootView

    }

    //previous fragment  with/without selected tune
    private fun previousFragment() {
        val supportFragmentManager = GMAudioApp.appContext.activityContext.supportFragmentManager
        supportFragmentManager.popBackStack()

        //below code is temporary condition for satisfy the functionality. Once I will get better idea i will implement
        supportFragmentManager.beginTransaction()
                .replace(GMAudioApp.appContext.activityContext.getContainerId(), getFragmentByTag("NowPlayingFragment"), NowPlayingActivity.CONTENT_FRAGMENT_TAG)
                .commit()
    }

    override fun onRegisterListener(view: View, eventName: String, any: Any?) {
        when (view.tag.toString()) {
            "eNowPlayingAM", "eNowPlayingFM", "eAMFMTuneAM", "eAMFMTuneFM" -> {
                previousFragment()
            }
        }
    }


    override fun getFragmentTransaction(containerId: Int, fragment: Fragment): FragmentTransaction? {
        var fragmentTransaction= GMAudioApp.appContext.activityContext.supportFragmentManager.beginTransaction()
                ?.setCustomAnimations(R.anim.slide_up, R.anim.slide_down,
                        R.anim.slide_up, R.anim.slide_down)
                ?.add(containerId, fragment)
                ?.addToBackStack(MANUAL_TUNER_BACKSTACK)
        fragmentTransaction?.commit()
        return fragmentTransaction
    }

    override fun onResume() {
        super.onResume()
        val frag = GMAudioApp.appContext. activityContext.supportFragmentManager.findFragmentByTag(CONTENT_FRAGMENT_TAG)
        (frag as NowPlayingFragment).exitAnimation()
    }

    override fun onPause() {
        super.onPause()
        val frag = GMAudioApp.appContext.activityContext.supportFragmentManager.findFragmentByTag(CONTENT_FRAGMENT_TAG)
        (frag as NowPlayingFragment).entryAnimation()
    }


    override fun entryAnimation() {
        // Implement entry animations, if any
    }

    override fun exitAnimation() {
        // Implement exit animations, if any
    }

}