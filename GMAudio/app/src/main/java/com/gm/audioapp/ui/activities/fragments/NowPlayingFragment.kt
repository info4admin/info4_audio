package com.gm.audioapp.ui.activities.fragments

import android.animation.ObjectAnimator
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v4.view.animation.FastOutSlowInInterpolator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.R
import com.gm.audioapp.databinding.NowPlayingFragmentBinding
import com.gm.audioapp.ui.activities.NowPlayingActivity
import com.gm.media.models.DataPoolDataHandler
import com.gm.audioapp.viewmodels.EventHandler

/**
 *  This screen is used to tune to AM/FM station.
 *  It shows the current playing station and other media metadata
 */
class NowPlayingFragment : BaseFragment() {

    private var mRootView: View? = null
    private var mMainDisplay: View? = null
   /* private var fm_nowplaying_include: View? = null
    private var am_nowplaying_include: View? = null*/
    private var cardView: View? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var binding = DataBindingUtil.inflate<NowPlayingFragmentBinding>(inflater, R.layout.ics_audio_nowplaying_layout, container, false)

        mRootView = binding.root
        binding.let {

            it?.clickHandler = EventHandler
            it?.dataPoolHandler = DataPoolDataHandler
        }

        /*am_nowplaying_include = mRootView!!.findViewById(R.id.am_nowplaying_include)
        fm_nowplaying_include = mRootView!!.findViewById(R.id.fm_nowplaying_include)*/
        mMainDisplay = mRootView!!.findViewById(R.id.recycler_view)
        cardView = mRootView!!.findViewById(R.id.controlButtonsLayout)

        return mRootView
    }

    private fun fadeOutContent() {
        fadeOut(mMainDisplay!!)
        //fadeOut(am_nowplaying_include!!)
        //fadeOut(fm_nowplaying_include!!)
        fadeOut(cardView!!)
    }

    /**
     * Fades out the given view
     *
     * @param view view to be fade out
     */
    private fun fadeOut(view: View) {
        val containerAlphaAnimator = ObjectAnimator.ofFloat(view, View.ALPHA, 1f, 0f)
        containerAlphaAnimator.interpolator = NowPlayingFragment.sInterpolator
        containerAlphaAnimator.startDelay = FADE_OUT_START_DELAY_MS.toLong()
        containerAlphaAnimator.duration = FADE_ANIM_TIME_MS.toLong()
        containerAlphaAnimator.start()
    }

    private fun fadeInContent() {
        fadeIn(mMainDisplay!!)
        //fadeIn(am_nowplaying_include!!)
        //fadeIn(fm_nowplaying_include!!)
        fadeIn(cardView!!)
    }

    /**
     * Fades in the given view
     *
     * @param view view to be fade in
     */
    private fun fadeIn(view: View) {
        val containerAlphaAnimator = ObjectAnimator.ofFloat(view, View.ALPHA, 0f, 1f)
        containerAlphaAnimator.interpolator = NowPlayingFragment.sInterpolator
        containerAlphaAnimator.duration = FADE_ANIM_TIME_MS.toLong()
        containerAlphaAnimator.start()
    }


    override fun getFragmentTransaction(containerId: Int, fragment: Fragment): FragmentTransaction? {
         var fragmentTransaction=GMAudioApp.appContext.activityContext.supportFragmentManager.beginTransaction()
                ?.replace(containerId, fragment, NowPlayingActivity.CONTENT_FRAGMENT_TAG)
        fragmentTransaction?.commit()
        return fragmentTransaction
    }

    override fun entryAnimation() {
        fadeInContent()
    }

    override fun exitAnimation() {
        fadeOutContent()
    }

    /**
     * This object holds the instance of NowPlayingFragment and FastOutSlowInInterpolator
     */
    companion object {
        /**
         * this interpolator is used for fade in/out
         */
        private val sInterpolator = FastOutSlowInInterpolator()
        private val FADE_OUT_START_DELAY_MS = 150
        private val FADE_ANIM_TIME_MS = 100
       // private val fragment = NowPlayingFragment()

        /**
         * @return returns instance of NowPlayingFragment
         */
       // fun getInstance(): NowPlayingFragment = fragment
    }

}