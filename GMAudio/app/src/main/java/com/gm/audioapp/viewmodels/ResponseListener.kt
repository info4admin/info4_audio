package com.gm.audioapp.viewmodels

import android.support.v7.widget.LinearLayoutManager
import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.R
import com.gm.audioapp.ui.activities.BaseActivity
import com.gm.audioapp.ui.activities.fragments.RadioPresetsFragment
import com.gm.media.apiintegration.SystemListener
import com.gm.media.apiintegration.apiinterfaces.IAMFMManagerRes
import com.gm.media.models.*
import com.gm.media.models.DataPoolDataHandler.aMFMStationInfo_t

/**
 * A controller decides which platform api call must be invoked.
 *
 * @author GM on 3/9/2018.
 */
object ResponseListener : IAMFMManagerRes {

    init {
        SystemListener.registerApiCallback(this)
    }


    override fun onAMFM_RES_AMCURRENTSTATIONINFO(amfmstationinfo: AMFMStationInfo_t) {
        updateCarouselView(DataPoolDataHandler.AMTUNER_CURRENTSTATIONINFO_FREQUENCY.get()!!.toFloat().toString())
        updateRadioPresetsList(aMFMStationInfo_t.get()!!)
    }

    override fun onAMFM_RES_FMCURRENTSTATIONINFO(amfmstationinfo: AMFMStationInfo_t) {
        updateCarouselView(DataPoolDataHandler.FMTUNER_CURRENTSTATIONINFO_FREQUENCY.get().toString())
        updateRadioPresetsList(aMFMStationInfo_t.get()!!)
    }

    override fun onAMFM_RES_AMCATEGORYSTATIONLIST(tunercategorylist: AMFMStationInfo_t) {
    }

    override fun onAMFM_RES_FMCATEGORYSTATIONLIST(tunercategorylist: AMFMStationInfo_t) {
    }

    override fun onAMFM_RES_AMSTRONGSTATIONSLIST(amfmstationlist: AMFMStationList_t) {
        //in hardware device we have check multiple times this condition when we are in radiopresetfragment
        addCategoryStationListUpdate()
        setCarouselView()
    }

    override fun onAMFM_RES_FMSTRONGSTATIONSLIST(amfmstationlist: AMFMStationList_t) {
        //in hardware device we have check multiple times this condition when we are in radiopresetfragment
        addCategoryStationListUpdate()
        setCarouselView()
    }

    //add category and station list update to list
    fun addCategoryStationListUpdate() {
        try {
            if (GMAudioApp.appContext.recyclerViewAdapter != null) {
                DataPoolDataHandler.browseList.add(0, Builder().stationName(GMAudioApp.appContext.getString(R.string.categories)).build())
                DataPoolDataHandler.browseList.add(1, Builder().stationName(GMAudioApp.appContext.getString(R.string.station_list_update)).build())
            }
        } catch (exception: Exception) {
            exception.printStackTrace()
        }
    }

    override fun onAMFM_RES_AMSTRONGSTATIONLISTPROGRESS(pData: Int) {
    }

    override fun onAMFM_RES_FMSTRONGSTATIONLISTPROGRESS(pData: Int) {
    }

    override fun onAMFM_RES_AMTUNEBYPARTIALFREQUENCY(amfmtuneinfo: ManualTuneData) {
    }

    override fun onAMFM_RES_FMTUNEBYPARTIALFREQUENCY(amfmtuneinfo: ManualTuneData) {
    }

    override fun onAMFM_RES_FMRDSSWITCH(pData: Int) {
    }

    override fun onAMFM_RES_REGIONSETTING(pData: Int) {
    }

    override fun onAMFM_RES_TPSTATUS(amfmtpstatus: AMFMTPStatus_t) {
    }

    override fun onAMFM_RES_TRAFFICALERT(pData: String) {
    }

    override fun onAMFM_RES_TRAFFICALERTACTIVECALL(amfmtrafficinfo: AMFMTrafficInfo_t) {
    }

    override fun onAMFM_RES_PROGRAMTYPERDSNOTIFICATION(amfmprogramtypealert: AMFMProgramTypeAlert_t) {
    }

    override fun onAMFM_RES_AMSTRONGSTATIONLISTUPDATEFAILED(pData: String) {
    }

    override fun onAMFM_RES_FMSTRONGSTATIONLISTUPDATEFAILED(pData: String) {
    }

    override fun onAMFM_RES_CANCELAMSTRONGSTATIONLISTUPDATE(pData: String) {
    }

    override fun onAMFM_RES_CANCELFMSTRONGSTATIONLISTUPDATE(pData: String) {
    }

    override fun onAMFM_RES_TRAFFICALERT_END(pData: String) {
    }

    override fun onAMFM_RES_TRAFFICINFOSTART(pData: String) {
    }

    override fun onAMFM_RES_STATIONAVAILABALITYSTATUS(fmstationavailabilityinfo: FMStationAvailabilityInfo_t) {
    }

    override fun onAMFM_RES_ACTIONUNAVAILABLE(pData: String) {
    }

    /**
     * It updates the carousal selected item
     */
    private fun setCarouselView() {
        if (DataPoolDataHandler.AUDIOMANAGER_CHANGE_AUDIOSOURCE.get().toString() == NOWPLAYING_SOURCE_TYPE.AM.name)
            updateCarouselView(DataPoolDataHandler.AMTUNER_CURRENTSTATIONINFO_FREQUENCY.get()!!.toFloat().toString())
        else if (DataPoolDataHandler.AUDIOMANAGER_CHANGE_AUDIOSOURCE.get().toString() == NOWPLAYING_SOURCE_TYPE.FM.name)
            updateCarouselView(DataPoolDataHandler.FMTUNER_CURRENTSTATIONINFO_FREQUENCY.get().toString())
    }

    /**
     * It displays the given frequency as selected item in carousal view
     *
     * @param frequency to be selected/highlighted in carousal view
     */
    private fun updateCarouselView(frequency: String) {

        if (GMAudioApp.appContext.carouselRecyclerViewAdapter != null) // TODO
            if (!GMAudioApp.appContext.carouselRecyclerViewAdapter!!.isScrolling) {
                val filterItem = DataPoolDataHandler.browseList.filter { it -> it.frequency.toString() == frequency }
                if (filterItem.isNotEmpty()) {
                    val index = DataPoolDataHandler.browseList.indexOf(filterItem[0])
                    (GMAudioApp.appContext.carouselRecyclerViewAdapter?.recyclerView?.layoutManager as LinearLayoutManager).scrollToPositionWithOffset(index, 0)
                    GMAudioApp.appContext.carouselRecyclerViewAdapter?.updateVisibleItem(index, true)
                }
            }
    }

    /**
     * This is called to update the metadata for given station
     * @param stationInfo this object will be added in the station list
     */
    private fun updateRadioPresetsList(stationInfo: AMFMStationInfo_t) {
        try {
            if (BaseActivity.currentFragment is RadioPresetsFragment) {
                val filterItem = DataPoolDataHandler.browseList.filter { it -> it.frequency.toString() == stationInfo.frequency.toString() }
                if (filterItem.isNotEmpty())
                    DataPoolDataHandler.nowPlayingBrowseListPosition.set(DataPoolDataHandler.browseList.indexOf(filterItem[0]))
            }
        } catch (exception: Exception) {
            exception.printStackTrace()
        }

    }

}

