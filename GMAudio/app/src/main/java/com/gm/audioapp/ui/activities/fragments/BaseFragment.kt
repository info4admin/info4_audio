package com.gm.audioapp.ui.activities.fragments

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction

/**
 * Base fragment for all other fragments for Audio module. Implement entry and exit animations, if any.
 */
abstract class BaseFragment: Fragment(), FragmentAnimation {

    /**
     * Configure fragment transaction with given fragment
     *
     * @return [FragmentTransaction] object with all configured properties
     */
    abstract fun getFragmentTransaction(containerId: Int, fragment: Fragment): FragmentTransaction?

}