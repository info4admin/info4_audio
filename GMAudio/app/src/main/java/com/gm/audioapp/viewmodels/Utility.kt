package com.gm.audioapp.viewmodels

import android.content.Context
import android.content.res.Configuration
import android.preference.PreferenceManager
import com.gm.audioapp.GMAudioApp
import com.gm.media.R
import java.util.*

object Utility {

    fun getStringById(id: Int): String {
        return GMAudioApp.appContext.resources.getString(id)
    }

    fun setLocale(c: Context): Context {
        return updateResources(c, getLanguage(c))
    }

    fun setNewLocale(c: Context, language: String): Context {
        persistLanguage(c, language)
        return updateResources(c, language)
    }

    fun getLanguage(c: Context): String {
        val prefs = PreferenceManager.getDefaultSharedPreferences(c)
        return prefs.getString(c.getString(R.string.shared_pref_language_key), c.getString(R.string.language_type))
    }

    private fun updateResources(context: Context, language: String): Context {
        val locale = Locale(language)
        Locale.setDefault(locale)
        val res = context.resources
        val config = Configuration(res.configuration)
        config.setLocale(locale)
        return context.createConfigurationContext(config)
    }

    private fun persistLanguage(c: Context, language: String) {
        val prefs = PreferenceManager.getDefaultSharedPreferences(c)
        prefs.edit().putString(getStringById(R.string.shared_pref_language_key), language).apply()
    }

}