package com.gm.media.apiintegration.mock

import android.os.Handler
import android.os.Looper
import android.os.Message
import android.text.TextUtils
import android.widget.Toast
import com.gm.media.R
import com.gm.media.apiintegration.AudioService
import com.gm.media.apiintegration.SystemListener
import com.gm.media.apiintegration.SystemListener.onAMFM_RES_PROGRAMTYPERDSNOTIFICATION
import com.gm.media.apiintegration.apiinterfaces.IManager
import com.gm.media.models.*
import com.gm.media.models.DataPoolDataHandler.aMFMStationInfo_t
import com.gm.media.models.DataPoolDataHandler.browseList
import com.gm.media.models.DataPoolDataHandler.favoriteList
import com.gm.media.apiintegration.mock.AmFmMediaPlayer.initAudioPlay
import com.gm.media.apiintegration.mock.AmFmMediaPlayer.releasePlayer
import com.gm.media.utils.Log
import com.gm.media.utils.ManualTunerController.amLowerValue
import com.gm.media.utils.ManualTunerController.amStepValue
import com.gm.media.utils.ManualTunerController.amUpperValue
import com.gm.media.utils.ManualTunerController.fmLowerValue
import com.gm.media.utils.ManualTunerController.fmStepValue
import com.gm.media.utils.ManualTunerController.fmUpperValue
import com.gm.media.utils.ManualTunerController.getEnteredFrequency
import com.gm.media.utils.ManualTunerController.getFreqTxtForDisplay
import com.gm.media.utils.ManualTunerController.getFrequency
import com.gm.media.utils.ManualTunerController.getPossibleValues
import com.gm.media.utils.ManualTunerController.isValidEntry
import com.gm.media.utils.ManualTunerController.mCurrentModeChart
import com.gm.media.utils.ManualTunerController.prepareChart
import com.gm.media.utils.ManualTunerController.updateButtons

/**
 * This class provides implementation for IManager Interface using simulation data. This class is used when audio app
 * needs to work completely with static data. This class provides functionality for AM/FM tuning, direct tuning,favorite selection,browsing channels
 */
class SimulationManager : IManager {

    private val mTAG = SimulationManager::class.simpleName
    private var isTrafficProgramSupported = false
    private var isTrafficAlertPlaying: Boolean = false
    private var isTPStation = false
    //private var dialog: TrafficAlertDialog? = null
    var tpStatusChecked = 0
    private var simulationManager: SimulationManager

    /**
     * Initialises Simulation Manager
     */
    init {
        simulationManager = this
        INIT_AMFM_REQUEST()
    }

    override fun onAMFM_REQ_AMSTATIONLIST() {
        playFMAMStationList(NOWPLAYING_SOURCE_TYPE.AM)
    }

    override fun onAMFM_REQ_FMSTATIONLIST() {
        playFMAMStationList(NOWPLAYING_SOURCE_TYPE.FM)
    }


    override fun onAMFMTuneRequest(any: Any?) {
        if (DataPoolDataHandler.AUDIOMANAGER_CHANGE_AUDIOSOURCE.get()?.name.equals(NOWPLAYING_SOURCE_TYPE.AM.name)) {
            onAMFM_REQ_AMTUNEBYFREQUENCY(any)
        } else {
            onAMFM_REQ_FMTUNEBYFREQUENCY(any)
        }
    }

    override fun onFavoriteRequest(any: Any?) {
        if (DataPoolDataHandler.audioControlDisabled.get()!!) {
            showQuickNotice(AudioService.instance!!.getString(R.string.favorite_unavailable))
            return
        }
        if (any != null) {
            SharedPreferencesController.loadPreferences()
            SystemListener.updateFavorites(any as AMFMStationInfo_t)
            SharedPreferencesController.savePreferences()
        }
    }

    override fun INIT_AMFM_REQUEST() {
        SharedPreferencesController.loadPreferences()
        val stationInfo = getCurrentStation(null)
        setBrowseStationList()
        if (stationInfo.rdsStationInfo == NOWPLAYING_SOURCE_TYPE.AM.name) {
            SystemListener.onAMFM_RES_AMCURRENTSTATIONINFO(addAMFMStation(stationInfo, DataPoolDataHandler.favoriteList.contains(stationInfo.frequency.toString()), eTPStationStatus.AMFM_STATION_SUPPORTS_TP))
        } else if (stationInfo.rdsStationInfo == NOWPLAYING_SOURCE_TYPE.FM.name) {
            SystemListener.onAMFM_RES_FMCURRENTSTATIONINFO(addAMFMStation(stationInfo, DataPoolDataHandler.favoriteList.contains(stationInfo.frequency.toString()), eTPStationStatus.AMFM_STATION_SUPPORTS_TP))
        }
        //delay initAudioPlay so it start playing after screen is visible
        Handler().postDelayed({ initAudioPlay(getCurrentStationIndex()) }, 200)
    }

    /**
     * It updates the carousel view list
     */
    private fun setBrowseStationList() {
        if (DataPoolDataHandler.AUDIOMANAGER_CHANGE_AUDIOSOURCE.get()?.name.equals(NOWPLAYING_SOURCE_TYPE.AM.name)) {
            SystemListener.onAMFM_RES_AMSTRONGSTATIONSLIST(getStaticStationList())
        } else if (DataPoolDataHandler.AUDIOMANAGER_CHANGE_AUDIOSOURCE.get()?.name.equals(NOWPLAYING_SOURCE_TYPE.FM.name)) {
            SystemListener.onAMFM_RES_FMSTRONGSTATIONSLIST(getStaticStationList())
        }
    }

    /**
     * returns randon station from the list
     * @return AMFMStationInfo_t
     */
    private fun getRandomStation(): AMFMStationInfo_t {
        val list = DataSourceProvider.nowPlayingDataList(DataPoolDataHandler.previousSourceType)
        return list[0]
    }

    /**
     * It fetches data from DataSourceProvider
     * @return AMFMStationList_t
     */
    private fun getStaticStationList(): AMFMStationList_t {
        val list = DataSourceProvider.nowPlayingDataList(DataPoolDataHandler.previousSourceType)
        list.forEachIndexed { index, nowPlayingData ->
            kotlin.run {
                list[index].isFavorite = favoriteList.contains(nowPlayingData.frequency.toString())
            }
        }
        return AMFMStationList_t(list.size, list)
    }

    override fun onSourceReset(sourceTYPE: NOWPLAYING_SOURCE_TYPE?) {
        when (DataPoolDataHandler.previousSourceType) {
            NOWPLAYING_SOURCE_TYPE.AM -> {
                resetTuner()

            }
            NOWPLAYING_SOURCE_TYPE.FM -> {
                resetTuner()
            }
            NOWPLAYING_SOURCE_TYPE.USBMSD, NOWPLAYING_SOURCE_TYPE.AUX -> {
                resetMedia()
            }
        }
        SystemListener.onCurrentSourceChanged(sourceTYPE!!)
    }

    private fun resetMedia() {
        Log.v("SeekRelease!.....", "resetMedia")
    }

    private fun resetTuner() {
        Log.v("SeekRelease!.....", "resetTuner")
    }

    //play the next or previous AM stations
    override fun onAMFM_REQ_AMSEEKSTATION(any: Any?) {
        playNextPreviousSeekStation(any)
    }

    //play the next or previous FM stations
    override fun onAMFM_REQ_FMSEEKSTATION(any: Any?) {
        playNextPreviousSeekStation(any)
        try {

            if (isTrafficProgramSupported && isTPStation && DataPoolDataHandler.FMTUNER_TPSTATUS.get() == 1) {
//                if (null != dialog && dialog!!.isShowing)
//                    dialog!!.dismiss()
                isTrafficAlertPlaying = false
                val aMFMProgramTYPE = AMFMProgramTypeAlert_t(DataPoolDataHandler.FMTUNER_PROGRAMTYPERDSNOTIFICATION_NAME.get().toString(), DataPoolDataHandler.FMTUNER_PROGRAMTYPERDSNOTIFICATION_CONTENT.get().toString())
                onAMFM_RES_PROGRAMTYPERDSNOTIFICATION(aMFMProgramTYPE)
                onAMFM_REQ_TRAFFICALERT_LISTEN()
            }
        } catch (e: Exception) {
            Log.e(mTAG!!, e.printStackTrace().toString())

        }

    }

    override fun onAMFM_REQ_AMTUNESTATION(any: Any?) {
        nextPreviousTunerStation(any)
    }

    override fun onAMFM_REQ_FMTUNESTATION(any: Any?) {
        nextPreviousTunerStation(any)
    }


    override fun onAMFM_REQ_AMTUNEBYOBJECTID(any: Any?) {
        try {
            playStationByTunerObjectID(any)
        } catch (e: Exception) {
            Log.e(mTAG!!, e.printStackTrace().toString())
        }
    }

    override fun onAMFM_REQ_FMTUNEBYOBJECTID(any: Any?) {
        try {
            playStationByTunerObjectID(any)
        } catch (e: Exception) {
            Log.e(mTAG!!, e.printStackTrace().toString())
        }
    }

    override fun onAMFM_REQ_AMTUNEBYPARTIALFREQUENCY(any: Any?) {
        val frequencyKeys: String = getFrequency(any)!!
        mCurrentModeChart = prepareChart(amLowerValue.toLong(), amUpperValue.toLong(), amStepValue.toLong())
        val enteredFrequency: Long = getEnteredFrequency(frequencyKeys)
        val buttonValues = updateButtons(amLowerValue == amUpperValue, getPossibleValues(enteredFrequency))
        SystemListener.onAMFM_RES_AMTUNEBYPARTIALFREQUENCY(ManualTuneData(getFreqTxtForDisplay(frequencyKeys), isValidEntry(frequencyKeys), buttonValues))
    }

    override fun onAMFM_REQ_FMTUNEBYPARTIALFREQUENCY(any: Any?) {
        val frequencyKeys: String = getFrequency(any)!!
        mCurrentModeChart = prepareChart(fmLowerValue.toLong(), fmUpperValue.toLong(), fmStepValue.toLong())
        val enteredFrequency: Long = getEnteredFrequency(frequencyKeys)
        val buttonValues = updateButtons(fmLowerValue == fmUpperValue, getPossibleValues(enteredFrequency))
        SystemListener.onAMFM_RES_FMTUNEBYPARTIALFREQUENCY(ManualTuneData(getFreqTxtForDisplay(frequencyKeys), isValidEntry(frequencyKeys), buttonValues))
    }

    override fun onAMFM_REQ_CANCELAMSTRONGSTATIONSLISTUPDATE() {
        SystemListener.onAMFM_RES_CANCELAMSTRONGSTATIONLISTUPDATE("")
    }

    override fun onAMFM_REQ_CANCELFMSTRONGSTATIONSLISTUPDATE() {
        SystemListener.onAMFM_RES_CANCELFMSTRONGSTATIONLISTUPDATE("")
    }

    override fun onAMFM_REQ_AMHARDSEEKSTATION(any: Any?) {

        playNextPreviousSeekStation(any)
        val index = getCurrentStationIndex()
        SystemListener.onAMFM_RES_AMCURRENTSTATIONINFO(addAMFMStation(aMFMStationInfo_t.get()!!, DataPoolDataHandler.favoriteList.contains(aMFMStationInfo_t.get()!!.frequency.toString()), eTPStationStatus.AMFM_STATION_SUPPORTS_TP))
        initAudioPlay(index)
    }

    override fun onAMFM_REQ_FMHARDSEEKSTATION(any: Any?) {

        playNextPreviousSeekStation(any)
        val index = getCurrentStationIndex()
        SystemListener.onAMFM_RES_FMCURRENTSTATIONINFO(addAMFMStation(aMFMStationInfo_t.get()!!, DataPoolDataHandler.favoriteList.contains(aMFMStationInfo_t.get()!!.frequency.toString()), eTPStationStatus.AMFM_STATION_SUPPORTS_TP))
        initAudioPlay(index)
    }


    override fun onAMFM_REQ_UPDATEAMSTRONGSTATIONSLIST() {
        updateAMFMStrongStationList()
    }

    override fun onAMFM_REQ_UPDATEFMSTRONGSTATIONSLIST() {
        updateAMFMStrongStationList()
    }

    override fun onAMFM_REQ_AMTUNEBYFREQUENCY(any: Any?) {
        if (any != null) {
            playAMFMTunerByFrequency(any)
        }
    }

    override fun onAMFM_REQ_FMTUNEBYFREQUENCY(any: Any?) {
        if (any != null) { //if open
            val it = playAMFMTunerByFrequency(any)
            isTPStation = it!!.tpStationStatus == eTPStationStatus.AMFM_STATION_SUPPORTS_TP
            isTrafficProgramSupported = it.tpStationStatus == eTPStationStatus.AMFM_STATION_SUPPORTS_TP
            try {
                if (isTrafficProgramSupported && isTPStation && DataPoolDataHandler.FMTUNER_TPSTATUS.get() == 1) {
//                    if (null != dialog && dialog!!.isShowing)
//                        dialog!!.dismiss()
                    isTrafficAlertPlaying = false
                    val aMFMProgramTYPE = AMFMProgramTypeAlert_t(DataPoolDataHandler.FMTUNER_PROGRAMTYPERDSNOTIFICATION_NAME.get().toString(),
                            DataPoolDataHandler.FMTUNER_PROGRAMTYPERDSNOTIFICATION_CONTENT.get().toString())
                    onAMFM_RES_PROGRAMTYPERDSNOTIFICATION(aMFMProgramTYPE)
                    onAMFM_REQ_TRAFFICALERT_LISTEN()
                }
            } catch (e: Exception) {
                Log.e(mTAG!!, e.printStackTrace().toString())

            }
        } //if close
    }


    override fun onAMFM_REQ_FMCATEGORYSTATIONLIST(any: Any?) {
        SystemListener.onAMFM_RES_FMCATEGORYSTATIONLIST(any as AMFMStationInfo_t)
    }

    override fun onAMFM_REQ_FMRDSSWITCH(rdsswitch: Int) {
    }

    override fun onAMFM_REQ_TPSTATUS(any: Any?) {
        val checked: Int
        var station = aMFMStationInfo_t.get()
        if (any == 0) {
            if (aMFMStationInfo_t.get()!!.tpStationStatus.value != eTPStationStatus.AMFM_STATION_SUPPORTS_TP.value) {
                tpStatusChecked = 1
                // TrafficTask(simulationManager).execute()
            }
        } else {
            tpStatusChecked = 0
            val amfmtpStatus_t = AMFMTPStatus_t(eTPStationState.AMFM_TP_SEARCH_NONE, tpStatusChecked)
            SystemListener.onAMFM_RES_TPSTATUS(amfmtpStatus_t)
//            try {
//                if (null != dialog) {
//                    isTrafficAlertPlaying = false
//                    dialog!!.dismiss()
//                }
//            } catch (e: TunerServiceStatusException) {
//                e.printStackTrace()
//            } catch (e: TunerException) {
//                e.printStackTrace()
//            }
        }

    }


    override fun onAMFM_REQ_FMTUNETMCSTATION(programIdentifier: Int) {
    }

    override fun onAMFM_REQ_TRAFFICALERT_DISMISS() {
//        if (null != dialog && dialog!!.isShowing) {
//            isTrafficAlertPlaying = false
//            dialog!!.dismiss()
//        }
    }

    override fun onAMFM_REQ_TRAFFICALERT_LISTEN() {

        try {
            if (!isTrafficAlertPlaying) {
//                dialog = TrafficAlertDialog()
//                dialog!!.setCancelable(false)
//                dialog!!.setCanceledOnTouchOutside(false)
//                dialog!!.show()

                isTrafficAlertPlaying = true
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onAMFM_REQ_SETREGIONSETTING(state: Int) {
    }

    override fun onAMFM_REQ_GREYEDBUTTON_CLICK() {
        SystemListener.showQuickNotice(AudioService.instance!!.resources.getString(R.string.audio_action_not_supported))
    }

    /**
     * Displays Quick Notice using given message
     * @param message
     */
    private fun showQuickNotice(message: String) {
        Toast.makeText(AudioService.instance!!, message, Toast.LENGTH_SHORT).show()
    }


    override fun onAMFM_REQ_AMCATEGORYSTATIONLIST(any: Any?) {
        SystemListener.onAMFM_RES_AMCATEGORYSTATIONLIST(any as AMFMStationInfo_t)
    }

    override fun onCategoryList() {
        SystemListener.updateCategoriesList(null)
    }

    /**
     *  call this method to play AM/FM based on type
     *  @param sourceType can be NOWPLAYING_SOURCE_TYPE.AM/FM
     */
    private fun playFMAMStationList(sourceType: NOWPLAYING_SOURCE_TYPE) {
        onSourceReset(sourceType)
        setBrowseStationList()
        val station = getCurrentStation(sourceType.name)
        setFMAMCurrentStationInfo(station)
        initAudioPlay(getCurrentStationIndex())
    }

    /**
     * This method returns current station if station is already tuned
     * Otherwise it returns any random station from list.
     *
     * @param sourceType type of waveband AM/FM
     * @return AMFMStationInfo_t
     */
    private fun getCurrentStation(sourceType: String?): AMFMStationInfo_t {
        return if (sourceType != null && (aMFMStationInfo_t.get() == null ||
                        TextUtils.isEmpty(aMFMStationInfo_t.get()?.frequency.toString()) ||
                        aMFMStationInfo_t.get()?.rdsStationInfo!! != sourceType))
            getRandomStation()
        else if (aMFMStationInfo_t.get() == null || TextUtils.isEmpty(aMFMStationInfo_t.get()?.frequency.toString()))
            getRandomStation()
        else
            aMFMStationInfo_t.get()!!
    }

    /**
     * returns index of station that is tuned
     *
     * @return index of current station in the list
     */
    fun getCurrentStationIndex(): Int {
        var index = -1
        val filterItem = browseList.filter { its -> its.frequency == aMFMStationInfo_t.get()!!.frequency }
        if (filterItem.isNotEmpty()) {
            index = browseList.indexOf(filterItem[0])
        }
        return index
    }

    /**
     * call this method to seek station. It plays up/down station
     *
     * @param any can be eAMFMSeekType.AMFM_SEEK_TYPE_SEEKUP/AMFM_SEEK_TYPE_SEEKDOWN
     */
    private fun playNextPreviousSeekStation(any: Any?) {

        var index = getCurrentStationIndex()
        if (any as eAMFMSeekType == eAMFMSeekType.AMFM_SEEK_TYPE_SEEKUP || any == eAMFMSeekType.AMFM_SEEK_TYPE_FASTSEEKUP) {
            if (++index < browseList.size) else index = 0
        } else if (any == eAMFMSeekType.AMFM_SEEK_TYPE_SEEKDOWN || any == eAMFMSeekType.AMFM_SEEK_TYPE_FASTSEEKDOWN) {
            if (--index >= 0) else index = browseList.size - 1
        }
        val station = browseList[index]
        setFMAMCurrentStationInfo(station)
        isTPStation = station.tpStationStatus == eTPStationStatus.AMFM_STATION_SUPPORTS_TP
        isTrafficProgramSupported = station.tpStationStatus == eTPStationStatus.AMFM_STATION_SUPPORTS_TP
        initAudioPlay(index)
    }

    /**
     * call this method to tune station. It plays up/down station
     *
     * @param any can be eAMFMSeekType.AMFM_SEEK_TYPE_SEEKUP/AMFM_SEEK_TYPE_SEEKDOWN
     */

    private fun nextPreviousTunerStation(any: Any?) {

        var index = getCurrentStationIndex()
        if (any as eAMFMTuneType == eAMFMTuneType.AMFM_TUNE_TYPE_TUNEUP) {
            if (++index < browseList.size) else index = 0
        } else if (any == eAMFMTuneType.AMFM_TUNE_TYPE_TUNEDOWN) {
            if (--index >= 0) else index = browseList.size - 1
        }

        val station = browseList[index]
        setFMAMCurrentStationInfo(station)
    }

    /**
     * called this method to directly tune  to selected station object.
     *
     * @param any object id
     */
    private fun playStationByTunerObjectID(any: Any?) {
        //if (StationLisProgressUpdater.isStationListUpdaterOn) {
        if (DataPoolDataHandler.showUpdateProgress.get()!!) {
            showQuickNotice(AudioService.instance!!.resources.getString(R.string.audio_action_not_supported))
            return
        }
        if (any != null) {
            val filterItem = browseList.filter { its -> its.objectId == any }
            if (filterItem.isNotEmpty()) {
                val index = browseList.indexOf(filterItem[0])
                val it = browseList[index]
                setFMAMCurrentStationInfo(it)
                initAudioPlay(index)
            }
        }
    }

    /**
     * Sets selected station info on screen
     *
     * @param it instance of AMFMStationInfo_t
     */
    private fun setFMAMCurrentStationInfo(station: AMFMStationInfo_t) {
        val amfmStationInfo_t = addAMFMStation(station, DataPoolDataHandler.favoriteList.contains(station.frequency.toString()), station.tpStationStatus)
        if (station.rdsStationInfo == NOWPLAYING_SOURCE_TYPE.AM.name)
            SystemListener.onAMFM_RES_AMCURRENTSTATIONINFO(amfmStationInfo_t)
        else
            SystemListener.onAMFM_RES_FMCURRENTSTATIONINFO(amfmStationInfo_t)
    }

    /**
     * call this method to show progress bar for Manual Update
     */
    private fun updateAMFMStrongStationList() {
        //releasePlayer()
        //StationLisProgressUpdater.startProgressUpdater(null)

        if (!(DataPoolDataHandler.showUpdateProgress.get() as Boolean)) {
            releasePlayer()
            DataPoolDataHandler.audioControlDisabled.set(true)
            DataPoolDataHandler.showUpdateProgress.set(true)
            DataPoolDataHandler.updateProgress.set(0)
            var progress: Int = DataPoolDataHandler.updateProgress.get()!!


            val t = Thread({

                while (progress < 100 && DataPoolDataHandler.showUpdateProgress.get() as Boolean) {
                    try {
                        Thread.sleep(1000)
                        mHandler.sendMessage(mHandler.obtainMessage())
                        progress = DataPoolDataHandler.updateProgress.get()!!

                    } catch (e: InterruptedException) {
                        //GMLog.d("suresh","Thread Exception "+e.message)
                        DataPoolDataHandler.audioControlDisabled.set(false)
                        break
                    }

                }
            }, "progress_bar_thread")

            t.start()
        }
    }

    /**
     * called this method to directly tune  to selected frequency.
     *
     * @param any frequency
     */
    private fun playAMFMTunerByFrequency(any: Any?): AMFMStationInfo_t? {
        var it: AMFMStationInfo_t? = null
        val filterItem = browseList.filter { its -> its.frequency.toString() == any.toString().toFloat().toString() }
        if (filterItem.isNotEmpty()) {
            val index = browseList.indexOf(filterItem[0])
            it = browseList[index]
            setFMAMCurrentStationInfo(it)
            initAudioPlay(index)
        }
        return it
    }

    private val mHandler: Handler = object : Handler(Looper.getMainLooper()) {
        /*
         * handleMessage() defines the operations to perform when
         * the Handler receives a new Message to process.
         */
        override fun handleMessage(inputMessage: Message) {
            if (DataPoolDataHandler.showUpdateProgress.get() as Boolean) {
                DataPoolDataHandler.updateProgress.set(DataPoolDataHandler.updateProgress.get() as Int + 5)
                if (DataPoolDataHandler.updateProgress.get() as Int == 100) {
                    DataPoolDataHandler.showUpdateProgress.set(false)
                    DataPoolDataHandler.updateProgress.set(0)
                    DataPoolDataHandler.audioControlDisabled.set(false)
                }
            } else {
                val filterItem = Thread.getAllStackTraces().keys.filter { t -> t.name == "progress_bar_thread" && t.isAlive }
                if (filterItem.isNotEmpty())
                    filterItem[0].interrupt()
            }
        }
    }

    //add FMAM station data
    private fun addAMFMStation(stationInfo: AMFMStationInfo_t, isFavorite: Boolean, eTPStationStatus: eTPStationStatus): AMFMStationInfo_t {
        return Builder()
                .frequency(stationInfo.frequency)
                .stationName(stationInfo.stationName!!)
                .rdsStatus(eRdsStatus.AMFM_RDS_AVAILABLE)
                .objectId(stationInfo.objectId)
                .rdsStationInfo(stationInfo.rdsStationInfo!!)
                .ptyCategory(stationInfo.ptyCategory)
                .tpStationStatus(eTPStationStatus)
                .isFavorite(isFavorite)
                .build()
    }
}