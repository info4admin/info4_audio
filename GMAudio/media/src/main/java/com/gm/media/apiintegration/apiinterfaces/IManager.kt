package com.gm.media.apiintegration.apiinterfaces

import com.gm.media.models.NOWPLAYING_SOURCE_TYPE

/**
 * API calls for three platforms: GM SDK, Android SDK and Simulation.
 *
 * @author Aswin
 */
interface IManager : IAMFMManagerReq {

    /**
     * Initialize for NowPlaying screen
     */
    fun INIT_AMFM_REQUEST()

    /**
     * To get the current station info on init or on seek actions
     */
    //fun getCurrentStationInfo()

    /**
     * Calls to show a list of AM/FM Stations and any media list
     */
    //fun updateStrongStationList()

    /**
     * calls to show list of AM/FM stations in category specific manner
     */
    fun onCategoryList()

    /**
     * Set the current selected source type.
     *
     * @param sourceTYPE Audio source type
     */
    fun onSourceReset(sourceTYPE: NOWPLAYING_SOURCE_TYPE?)

    /**
     * Set the manually entered channel
     * @param any [int], manually entered frequency
     */
    fun onAMFMTuneRequest(any: Any?)

    /**
     * Set the station as favorite
     * @param any [AMFMStationInfo_t], select station object
     */
    fun onFavoriteRequest(any: Any?)

    /**
     * To display quick notice when user click on control button while manual update is in progress
     */
    fun onAMFM_REQ_GREYEDBUTTON_CLICK()

    override fun onAMFM_REQ_AMSEEKSTATION(any: Any?)
    override fun onAMFM_REQ_FMSEEKSTATION(any: Any?)
    override fun onAMFM_REQ_AMTUNEBYFREQUENCY(any: Any?)
    override fun onAMFM_REQ_FMTUNEBYFREQUENCY(any: Any?)
    override fun onAMFM_REQ_AMTUNESTATION(any: Any?)
    override fun onAMFM_REQ_FMTUNESTATION(any: Any?)
    override fun onAMFM_REQ_AMSTATIONLIST()
    override fun onAMFM_REQ_FMSTATIONLIST()
    override fun onAMFM_REQ_FMTUNEBYOBJECTID(any: Any?)
    override fun onAMFM_REQ_AMTUNEBYOBJECTID(any: Any?)
    override fun onAMFM_REQ_UPDATEAMSTRONGSTATIONSLIST()
    override fun onAMFM_REQ_CANCELAMSTRONGSTATIONSLISTUPDATE()
    override fun onAMFM_REQ_UPDATEFMSTRONGSTATIONSLIST()
    override fun onAMFM_REQ_CANCELFMSTRONGSTATIONSLISTUPDATE()

    override fun onAMFM_REQ_TPSTATUS(any: Any?)
    override fun onAMFM_REQ_AMHARDSEEKSTATION(any: Any?)
    override fun onAMFM_REQ_FMHARDSEEKSTATION(any: Any?)
    override fun onAMFM_REQ_AMTUNEBYPARTIALFREQUENCY(any: Any?)
    override fun onAMFM_REQ_FMTUNEBYPARTIALFREQUENCY(any: Any?)

    override fun onAMFM_REQ_TRAFFICALERT_DISMISS()


    override fun onAMFM_REQ_AMCATEGORYSTATIONLIST(any: Any?)
    override fun onAMFM_REQ_FMCATEGORYSTATIONLIST(any: Any?)
}