package com.gm.media.models

/*********************************************************************
Module        : AMFM
Type          : GCI headers ),  Specific
Component     : HMI
Usage         : E Includes), UI Bridge Includes and Model External file
File Path     : Model/ScreenManager/ScreenModel_rpy/AMFMGCI.h

 *these should be generated from LGAMFMGCI.h. Remove LG wherever present in file
 *********************************************************************/


/**
 *
 * This class contains information related to station frequency and other information broadcast by channel.
 *
 */
class AMFMStationInfo_t
(
        val frequency: Float,//TODO need to convert to int
        var stationName: String?,
        val rdsStatus: eRdsStatus,
        val objectId: Int,
        val rdsStationInfo: String?,
        val ptyCategory: Int,
        var tpStationStatus: eTPStationStatus,
        var isFavorite: Boolean = false,
        val categoryData: MutableMap<String, ArrayList<AMFMStationInfo_t>>? = null
)


/**
 * This class wraps all the station in List .
 * This class has AMFMStationList_t variables.
 */
data class AMFMStationList_t
(
        var stationlistCount: Int = 0,
        var objectList: List<AMFMStationInfo_t>
)

/**
 * This class contains station list filtered by category type
 * This class has TunerCategoryList_t variables.
 */

data class TunerCategoryList_t
(
        var categoryType: eRDSPtyNACategory = eRDSPtyNACategory.AMFM_RDS_PTY_NA_ALL,
        var stationlistCount: Int = 0,
        var objectList: List<AMFMStationInfo_t>
)

/**

 * This class has AMFMTrafficInfo_t variables.

 */

data class AMFMTrafficInfo_t
(
        var name: String = "",
        var programIdentifier: Int = 0
)

/**
 * This class has AMFMProgramTypeAlert_t variables.
 */
data class AMFMProgramTypeAlert_t
(
        var name: String = "",
        var content: String = ""
)


/**
 * This class has AMFMTPStatus_t variables.
 */
data class AMFMTPStatus_t
(
        var state: eTPStationState = eTPStationState.AMFM_TP_SEARCH_NONE,
        var status: Int = 0
)

/**
 * This class has FMStationAvailabilityInfo_t variables.
 */
data class FMStationAvailabilityInfo_t
(
        var metadata: String = "",
        var stationStatus: eFMStationAvailability = eFMStationAvailability.FM_STATION_NOT_AVAILABLE
)

/**
 * This class has eTPStationStatus values.
 * @property value the value of eTPStationStatus class.
 */
enum class eTPStationStatus(val value: Number) {
    AMFM_STATION_DOES_NOT_SUPPORT_TP(0),
    AMFM_STATION_SUPPORTS_TP(1)
}

/**
 * This class has eRdsStatus values.
 * @property value the value of eRdsStatus class.
 */
enum class eRdsStatus(val value: Number) {
    AMFM_RDS_NOT_AVAILABLE(0),
    AMFM_RDS_AVAILABLE(1)
}

/**

 * This class has eRDSPtyNACategory values.
 * @property value the value of eRDSPtyNACategory class.
 */
enum class eRDSPtyNACategory(val value: Number) {
    AMFM_RDS_PTY_NA_NO_PTY(-1) {
        override fun toString(): String {
            return "NOPTY"
        }
    },
    AMFM_RDS_PTY_NA_ALL(0) {
        override fun toString(): String {
            return "ALL"
        }
    },
    AMFM_RDS_PTY_NA_POP(1) {
        override fun toString(): String {
            return "POP"
        }
    },
    AMFM_RDS_PTY_NA_ROCK(2) {
        override fun toString(): String {
            return "ROCK"
        }
    },
    AMFM_RDS_PTY_NA_TALK(3) {
        override fun toString(): String {
            return "TALK"
        }
    },
    AMFM_RDS_PTY_NA_COUNTRY(4) {
        override fun toString(): String {
            return "COUNTRY"
        }
    },
    AMFM_RDS_PTY_NA_CLASSICAL(5) {
        override fun toString(): String {
            return "CLASSICAL"
        }
    },
    AMFM_RDS_PTY_NA_JAZZ(6) {
        override fun toString(): String {
            return "JAZZ"
        }
    },
    AMFM_RDS_PTY_NA_INFORMATION(7) {
        override fun toString(): String {
            return "INFORMATION"
        }
    },
    AMFM_RDS_PTY_NA_MUSIC(8) {
        override fun toString(): String {
            return "MUSIC"
        }
    },
    AMFM_RDS_PTY_NA_WEATHER(9) {
        override fun toString(): String {
            return "WEATHER"
        }
    }
}

/**

 * This class has eRDSPtyEUCategory values.
 * @property value the value of eRDSPtyEUCategory class.
 */
enum class eRDSPtyEUCategory(val value: Number) {
    AMFM_RDS_PTY_EU_ALL(0),
    AMFM_RDS_PTY_EU_POP(1),
    AMFM_RDS_PTY_EU_MUSIC(2),
    AMFM_RDS_PTY_EU_CLASSIC(3),
    AMFM_RDS_PTY_EU_INFORMATION(4),
    AMFM_RDS_PTY_EU_ROCK(5)
}

/**

 * This class has eAMFMSeekType values.
 * @property value the value of eAMFMSeekType class.
 */
enum class eAMFMSeekType(val value: Number) {
    AMFM_SEEK_TYPE_SEEKSTOP(0),
    AMFM_SEEK_TYPE_SEEKUP(1),
    AMFM_SEEK_TYPE_SEEKDOWN(2),
    AMFM_SEEK_TYPE_FASTSEEKUP(3),
    AMFM_SEEK_TYPE_FASTSEEKDOWN(4)
}

/**

 * This class has eAMFMTuneType values.
 * @property value the value of eAMFMTuneType class.
 */
enum class eAMFMTuneType {
    AMFM_TUNE_TYPE_TUNESTOP,
    AMFM_TUNE_TYPE_TUNEUP,
    AMFM_TUNE_TYPE_TUNEDOWN,
    AMFM_TUNE_TYPE_FASTTUNEUP,
    AMFM_TUNE_TYPE_FASTTUNEDOWN
}

/**

 * This class has eTPStationState values.
 * @property value the value of eTPStationState class.
 */
enum class eTPStationState(val value: Number) {
    AMFM_TP_SEARCH_INITIATED(0),
    AMFM_TP_SEARCH_COMPLETED(1),
    AMFM_TP_SEARCH_NONE(2)
}


/**

 * This class has eFMStationAvailability values.
 * @property value the value of eFMStationAvailability class.
 */

enum class eFMStationAvailability(val value: Number) {
    FM_STATION_AVAILABLE(0),
    FM_STATION_NOT_AVAILABLE(1)
}


/**

 * This class has eLHD_RHD_RTL values.
 * @property value the value of eLHD_RHD_RTL class.
 */

enum class eLHD_RHD_RTL(val value: Number) {
    LHD(0) {
        override fun toString(): String {
            return "LHD"
        }
    },
    RHD(1) {
        override fun toString(): String {
            return "RHD"
        }
    },
    RTL(2) {
        override fun toString(): String {
            return "RTL"
        }
    }
}

/**

 * This class has NOWPLAYING_SOURCE_TYPE values.
 * It represents the source type selected for audio entertainment
 * @property value the value of NOWPLAYING_SOURCE_TYPE class.

 */

enum class NOWPLAYING_SOURCE_TYPE(val value: Number) {
    AM(0) {
        override fun toString(): String {
            return "AM Stations"
        }
    },
    FM(1) {
        override fun toString(): String {
            return "FM Stations"
        }
    },
    USBMSD(2) {
        override fun toString(): String {
            return "USB"
        }
    },
    AUX(3) {
        override fun toString(): String {
            return "AUX"
        }
    }
}

/**

 * This class has EQ_MODE_TYPE values.
 * @property value the value of EQ_MODE_TYPE class.

 */

enum class EQ_MODE_TYPE(val value: Number) {
    EQ_TALK(0),
    EQ_ROCK(1),
    EQ_JAZZ(2),
    EQ_POP(3),
    EQ_COUNTRY(4),
    EQ_CLASSICAL(5),
    EQ_OFF(6),
    EQ_MAX_SIZE(7)
}

/**

 * This class has TUNER_TYPE_E values.
 * @property value the value of TUNER_TYPE_E class.

 */

enum class TUNER_TYPE_E(val value: Number) {
    eSINGLE_TUNER(0x01),
    eDUAL_TUNER(0x02),
    eNO_TUNER_TYPE(0x03)
}

/**

 * This class has RADIO_MODE values.
 * @property value the value of RADIO_MODE class.

 */

enum class RADIO_MODE(val value: Number) {
    RADIO_FM(0),
    RADIO_AM(1),
    RADIO_XM(2),
    RADIO_DAB(3),
    RADIO_MAX_SIZE(4)
}

/**

 * This class has XM_LIST_TYPE values.
 * @property value the value of XM_LIST_TYPE class.

 */
enum class XM_LIST_TYPE(val value: Number) {
    XM_LIST_NONE(0),
    XM_LIST_CAT(1),
    XM_LIST_CHN(2),
    XM_LIST_SIZE(3)
}

/**

 * This class has PRESET_TYPE values.
 * @property value the value of PRESET_TYPE class.

 */

enum class PRESET_TYPE(val value: Number) {
    PRESET_FAVOURITE(0),
    PRESET_AS(1)
}

/**

 * This class has RDS_TYPE values.
 * @property value the value of RDS_TYPE class.

 */
enum class RDS_TYPE(val value: Number) {
    RDS_NONE(0), ///< calibaration default
    RDS_EXTEND(1), ///< US, CANADA
    RDS_FULL(2), ///< EU
    RDS_EXTEND_2(3) ///< AU, NZ, Brazil
}
