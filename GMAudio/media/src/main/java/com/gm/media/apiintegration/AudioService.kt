package com.gm.media.apiintegration

import android.app.Service
import android.content.Intent
import android.os.IBinder
import com.gm.media.BuildConfig


class AudioService() : Service() {
    override fun onBind(p0: Intent?): IBinder {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent!!.hasExtra("eventName"))
            SystemController.execute(intent.getStringExtra("eventName"), any)
        return START_REDELIVER_INTENT
    }

    companion object {
        var instance: AudioService? = null
        var any: Any? = null
        /**
         * Check if this application is running on GM's OS build
         */
        fun isSDKAvailable(): Boolean =
                android.os.Build.MANUFACTURER == BuildConfig.VENDOR

    }

}