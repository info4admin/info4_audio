package com.gm.media.apiintegration.mock

import android.content.Context.MODE_PRIVATE
import com.gm.media.apiintegration.AudioService
import com.gm.media.apiintegration.SystemListener
import com.gm.media.models.DataPoolDataHandler.favoriteList

/**
 * To load and save favorite items in shared preferences
 */

class SharedPreferencesController {

    companion object {
        val SHARED_PREFS_FILE = "GMAudio_SharedPreferences"
        val FAVORITES_KEY = "Favorites_Key"
        var IS_LAYOUT_DIRECTION_LTR = true
        val IS_LAYOUT_DIRECTION_KEY = "LAYOUT_DIRECTION"

        val prefs = AudioService.instance!!.getSharedPreferences(SHARED_PREFS_FILE, MODE_PRIVATE)
        val editor = prefs.edit()

        /**
         * To load favorites when app launches
         * */
        fun loadPreferences() {

            val set = prefs.getStringSet(FAVORITES_KEY, null)
            if (set != null && !set.isEmpty()) {
                favoriteList.clear()
                set.forEach({
                    favoriteList.add(it)
                })
            }

            IS_LAYOUT_DIRECTION_LTR = prefs.getBoolean(IS_LAYOUT_DIRECTION_KEY, IS_LAYOUT_DIRECTION_LTR)

        }
        /**
         * To save station as favorite when favorites button clicked
         * */
        fun savePreferences() {

            val set = HashSet<String>()
            set.addAll(favoriteList)
            editor.putStringSet(FAVORITES_KEY, set)
            editor.putBoolean(IS_LAYOUT_DIRECTION_KEY, IS_LAYOUT_DIRECTION_LTR)
            editor.commit()
        }


        fun saveStringinPreference(key:String, value:String){
            editor.putString(key, value)
            editor.commit()

        }


        fun getStringFromPreference(key:String,defaultValue:String):String{
            return prefs.getString(key, defaultValue)
        }


    }
}