package com.gm.media.apiintegration.apiinterfaces

import com.gm.media.models.*


/**
 *     It should contain AMFM_RES funcs "DECLARATION" only present under Recv Data func definition from CHVACProxy.cpp
 */

interface IAMFMManagerRes {
    /**
     *it is used to set current station information by using AMFMStationInfo_t data class.
     *  @param amfmstationinfo having the amfmstation data.
     */
    fun onAMFM_RES_AMCURRENTSTATIONINFO(amfmstationinfo: AMFMStationInfo_t)

    /**
     *it is used to set current station information by using AMFMStationInfo_t data class.
     *  @param amfmstationinfo having the amfmstation data.
     */
    fun onAMFM_RES_FMCURRENTSTATIONINFO(amfmstationinfo: AMFMStationInfo_t)

    /**
     *it is used to set fmstation category list information by using TunerCategoryList_t data class.
     *  @param tunercategorylist having the amfmstation data,stationlist count and categorytype.
     */
    fun onAMFM_RES_FMCATEGORYSTATIONLIST(tunercategorylist: AMFMStationInfo_t)

    /**
     *it is used to set amstation category list information by using TunerCategoryList_t data class.
     *  @param tunercategorylist having the amfmstation data,stationlist count and categorytype.
     */
    fun onAMFM_RES_AMCATEGORYSTATIONLIST(tunercategorylist: AMFMStationInfo_t)

    /**
     *it is used to set amstation list by using AMFMStationInfo_t data class.
     *  @param amfmstationlist having the amfmstation data.
     */
    fun onAMFM_RES_AMSTRONGSTATIONSLIST(amfmstationlist: AMFMStationList_t)

    /**
     *it is used to set the fmstation list information by using AMFMStationInfo_t data class.
     *  @param amfmstationlist having the amfmstation data.
     */
    fun onAMFM_RES_FMSTRONGSTATIONSLIST(amfmstationlist: AMFMStationList_t)

    /**
     *it is used to set progress of manual update for am station.

     */
    fun onAMFM_RES_AMSTRONGSTATIONLISTPROGRESS(pData: Int)

    /**
     *it is used to set progress of manual update for fm station.

     */
    fun onAMFM_RES_FMSTRONGSTATIONLISTPROGRESS(pData: Int)

    /**
     *it is used to set manual data by using ManualTuneData data class.
     *  @param amfmtuneinfo having the information about ManualTuneData data class.
     */
    fun onAMFM_RES_AMTUNEBYPARTIALFREQUENCY(amfmtuneinfo: ManualTuneData)

    /**
     *it provides manual data by using ManualTuneData data class.
     *  @param amfmtuneinfo having the information about ManualTuneData data class.
     */
    fun onAMFM_RES_FMTUNEBYPARTIALFREQUENCY(amfmtuneinfo: ManualTuneData)

    /**
     *it is used to set rds status.

     */
    fun onAMFM_RES_FMRDSSWITCH(pData: Int)

    /**
     *it is used to set the region.
     *  @param pData having the information about TunerManager.
     */

    fun onAMFM_RES_REGIONSETTING(pData: Int)

    /**
     *it is used to set the TPSTATUS.
     *  @param amfmtpstatus having the information about eTPStationState and status.
     */
    fun onAMFM_RES_TPSTATUS(amfmtpstatus: AMFMTPStatus_t)

    /**
     *it is used to set the TPSTATUS.

     */
    fun onAMFM_RES_TRAFFICALERT(pData: String)

    /**
     *it is used to set the traffic alert enable/disable.
     *  @param amfmtrafficinfo having the information about programIdentifier.
     */
    fun onAMFM_RES_TRAFFICALERTACTIVECALL(amfmtrafficinfo: AMFMTrafficInfo_t)

    /**
     *it is used to set the traffic alert enable/disable.
     *  @param amfmtrafficinfo having the information about programIdentifier.
     */
    fun onAMFM_RES_PROGRAMTYPERDSNOTIFICATION(amfmprogramtypealert: AMFMProgramTypeAlert_t)

    /**
     *it is used when am manual update fails..

     */

    fun onAMFM_RES_AMSTRONGSTATIONLISTUPDATEFAILED(pData: String)

    /**
     *it is used when fm manual update fails.

     */
    fun onAMFM_RES_FMSTRONGSTATIONLISTUPDATEFAILED(pData: String)

    /**
     *it is used to cancel manual update for the am station .

     */
    fun onAMFM_RES_CANCELAMSTRONGSTATIONLISTUPDATE(pData: String)

    /**
     *it is used to cancel manual update for the fm station.

     */
    fun onAMFM_RES_CANCELFMSTRONGSTATIONLISTUPDATE(pData: String)

    /**
     *it is used to set the TP status.

     */
    fun onAMFM_RES_TRAFFICALERT_END(pData: String)

    /**
     *it is used to set the traffic information.

     */
    fun onAMFM_RES_TRAFFICINFOSTART(pData: String)

    /**
     *it is used to set the station availabality status.
     * @param fmstationavailabilityinfo having the information about availabality status.
     */
    fun onAMFM_RES_STATIONAVAILABALITYSTATUS(fmstationavailabilityinfo: FMStationAvailabilityInfo_t)


    /**
     *it is used to set the station unavailable.
     */
    fun onAMFM_RES_ACTIONUNAVAILABLE(pData: String)


}