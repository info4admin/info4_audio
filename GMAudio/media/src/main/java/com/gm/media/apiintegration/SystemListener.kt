package com.gm.media.apiintegration

import android.content.Context
import android.widget.Toast
import com.gm.media.R
import com.gm.media.apiintegration.apiinterfaces.IAMFMManagerRes
import com.gm.media.models.*
import com.gm.media.models.DataPoolDataHandler.AMFMTUNER_CATEGORYSTATIONLIST_OBJECTLIST
import com.gm.media.models.DataPoolDataHandler.aMFMStationInfo_t
import com.gm.media.models.DataPoolDataHandler.browseList
import com.gm.media.models.DataPoolDataHandler.deviceData
import com.gm.media.models.DataPoolDataHandler.previousSourceType
import com.gm.media.utils.Log
import com.gm.media.utils.ManualTunerController
import gm.media.interfaces.IDevice

/**
 * Simply updates [DataPoolDataHandler] objects
 */
object SystemListener : IAMFMManagerRes {

    /**
     *     It should contain AMFM_RES, MEDIA_RES, AUDIOMGR_RES, DAB_RES, SDARS_RES funcs "DEFINITIONS"
     *     only present under RecvData func definition from CAMFMProxy.cpp, CAudioManagerProxy.cpp,
     *     CMediaProxy.cpp, CSDARSProxy.cpp & CDABProxy.cpp
     *     It should implement ImanagerRes
     */

    var mCallback:IAMFMManagerRes?=null

    //create object for IAMFMManagerRes and use context
    fun registerApiCallback(callback:IAMFMManagerRes)
    {
        this.mCallback = callback
    }

    override fun onAMFM_RES_AMCURRENTSTATIONINFO(amfmstationinfo: AMFMStationInfo_t) {
        if(amfmstationinfo!=null) {
            aMFMStationInfo_t.set(addAMFMStation(amfmstationinfo, DataPoolDataHandler.favoriteList.contains(amfmstationinfo.frequency.toString())))
            DataPoolDataHandler.AMTUNER_CURRENTSTATIONINFO_FREQUENCY.set(amfmstationinfo.frequency.toInt().toString())
            DataPoolDataHandler.AMTUNER_CURRENTSTATIONINFO_STATIONNAME.set(amfmstationinfo.stationName)
            DataPoolDataHandler.AMTUNER_CURRENTSTATIONINFO_RDSSTATIONINFO.set(amfmstationinfo.rdsStationInfo)
            DataPoolDataHandler.AMTUNER_CURRENTSTATIONINFO_OBJECTID.set(amfmstationinfo.objectId)
            DataPoolDataHandler.AMTUNER_CURRENTSTATIONINFO_RDSSTATUS.set(eRdsStatus.AMFM_RDS_AVAILABLE.value.toFloat())
            DataPoolDataHandler.AMTUNER_CURRENTSTATIONINFO_PTYCATEGORY.set(amfmstationinfo.ptyCategory)
            DataPoolDataHandler.AMTUNER_CURRENTSTATIONINFO_TPSTATIONSTATUS.set(amfmstationinfo.tpStationStatus.value.toFloat())
            DataPoolDataHandler.AMTUNER_DIRECTTUNE_PARTIAL_FREQUENCY.set("")
        }
        mCallback!!.onAMFM_RES_AMCURRENTSTATIONINFO(amfmstationinfo)
    }

    override fun onAMFM_RES_FMCURRENTSTATIONINFO(stationInfo: AMFMStationInfo_t) {
        if(stationInfo!=null) {
            aMFMStationInfo_t.set(addAMFMStation(stationInfo, DataPoolDataHandler.favoriteList.contains(stationInfo.frequency.toString())))
            DataPoolDataHandler.FMTUNER_CURRENTSTATIONINFO_FREQUENCY.set(stationInfo.frequency.toString())
            DataPoolDataHandler.FMTUNER_CURRENTSTATIONINFO_STATIONNAME.set(stationInfo.stationName)
            DataPoolDataHandler.FMTUNER_CURRENTSTATIONINFO_OBJECTID.set(stationInfo.objectId)
            DataPoolDataHandler.FMTUNER_CURRENTSTATIONINFO_PTYCATEGORY.set(stationInfo.ptyCategory)
            DataPoolDataHandler.FMTUNER_CURRENTSTATIONINFO_TPSTATIONSTATUS.set(stationInfo.tpStationStatus.value.toFloat())
            DataPoolDataHandler.FMTUNER_CURRENTSTATIONINFO_RDSSTATUS.set(stationInfo.rdsStatus.value.toFloat())
            DataPoolDataHandler.FMTUNER_CURRENTSTATIONINFO_RDSSTATIONINFO.set(stationInfo.rdsStationInfo)
            DataPoolDataHandler.FMTUNER_DIRECTTUNE_PARTIAL_FREQUENCY.set("")
        }
        mCallback!!.onAMFM_RES_FMCURRENTSTATIONINFO(stationInfo)
    }

    override fun onAMFM_RES_AMCATEGORYSTATIONLIST(tunercategorylist: AMFMStationInfo_t) {
        DataPoolDataHandler.AMFMTUNER_CATEGORYSTATIONLIST_OBJECTLIST.clear()
        DataPoolDataHandler.AMFMTUNER_CATEGORYSTATIONLIST_OBJECTLIST.addAll(tunercategorylist.categoryData!!.get(tunercategorylist.stationName)!!)
        DataPoolDataHandler.AMTUNER_CATEGORYSTATIONLIST_CATEGORYTYPE.set(tunercategorylist.stationName)
        DataPoolDataHandler.AMTUNER_CATEGORYSTATIONLIST_STATIONLISTCOUNT.set(tunercategorylist.categoryData!!.get(tunercategorylist.stationName)!!.size)
    }

    override fun onAMFM_RES_FMCATEGORYSTATIONLIST(tunercategorylist: AMFMStationInfo_t) {
        DataPoolDataHandler.AMFMTUNER_CATEGORYSTATIONLIST_OBJECTLIST.clear()
        DataPoolDataHandler.AMFMTUNER_CATEGORYSTATIONLIST_OBJECTLIST.addAll(tunercategorylist.categoryData!!.get(tunercategorylist.stationName)!!)
        DataPoolDataHandler.FMTUNER_CATEGORYSTATIONLIST_CATEGORYTYPE.set(tunercategorylist.stationName)
        DataPoolDataHandler.FMTUNER_CATEGORYSTATIONLIST_STATIONLISTCOUNT.set(tunercategorylist.categoryData!!.get(tunercategorylist.stationName)!!.size)
    }

    override fun onAMFM_RES_AMSTRONGSTATIONSLIST(amfmstationlist: AMFMStationList_t) {
        DataPoolDataHandler.AMTUNER_STRONGSTATIONS_LIST_LISTID.set(amfmstationlist.stationlistCount)
        DataPoolDataHandler.AMTUNER_STRONGSTATIONS_LIST_UPDATE_STATUS.set(0F)
        DataPoolDataHandler.browseList.clear()
        DataPoolDataHandler.browseList.addAll(amfmstationlist.objectList)
        DataPoolDataHandler.browseListAM.clear()
        DataPoolDataHandler.browseListAM.addAll(amfmstationlist.objectList)
        mCallback!!.onAMFM_RES_AMSTRONGSTATIONSLIST(amfmstationlist)
    }

    override fun onAMFM_RES_FMSTRONGSTATIONSLIST(amfmstationlist: AMFMStationList_t) {
        DataPoolDataHandler.FMTUNER_STRONGSTATIONS_LIST_LISTID.set(amfmstationlist.stationlistCount)
        DataPoolDataHandler.FMTUNER_STRONGSTATIONS_LIST_UPDATE_STATUS.set(0F)
        DataPoolDataHandler.browseList.clear()
        DataPoolDataHandler.browseList.addAll(amfmstationlist.objectList)
        DataPoolDataHandler.browseListFM.clear()
        DataPoolDataHandler.browseListFM.addAll(amfmstationlist.objectList)
        mCallback!!.onAMFM_RES_FMSTRONGSTATIONSLIST(amfmstationlist)
    }

    override fun onAMFM_RES_AMSTRONGSTATIONLISTPROGRESS(pData: Int) {
        //DataPoolDataHandler.AMTUNER_STRONGSTATIONS_LIST_UPDATE_PROGRESS.set(pData)
        DataPoolDataHandler.updateProgress.set(pData)

        if (pData >= 95 || pData < 0) {
            DataPoolDataHandler.showUpdateProgress.set(false)
            DataPoolDataHandler.audioControlDisabled.set(false)
        } else {
            DataPoolDataHandler.showUpdateProgress.set(true)
        }
    }

    override fun onAMFM_RES_FMSTRONGSTATIONLISTPROGRESS(pData: Int) {
        //DataPoolDataHandler.FMTUNER_STRONGSTATIONS_LIST_UPDATE_PROGRESS.set(pData)
        DataPoolDataHandler.updateProgress.set(pData)
        if (pData >= 95 || pData < 0) {
            DataPoolDataHandler.showUpdateProgress.set(false)
            DataPoolDataHandler.audioControlDisabled.set(false)
        } else {
            DataPoolDataHandler.showUpdateProgress.set(true)
        }
    }

    override fun onAMFM_RES_AMTUNEBYPARTIALFREQUENCY(amfmtuneinfo: ManualTuneData) {
        DataPoolDataHandler.AMTUNER_DIRECTTUNE_PARTIAL_FREQUENCY.set(amfmtuneinfo.frequency)
        DataPoolDataHandler.AMTUNER_DIRECTTUNE_AVAILABLE_KEYS.set(amfmtuneinfo.buttonValues.toString())
        DataPoolDataHandler.manualTuneData.set(amfmtuneinfo)
    }

    override fun onAMFM_RES_FMTUNEBYPARTIALFREQUENCY(amfmtuneinfo: ManualTuneData) {
        DataPoolDataHandler.FMTUNER_DIRECTTUNE_PARTIAL_FREQUENCY.set(amfmtuneinfo.frequency)
        DataPoolDataHandler.FMTUNER_DIRECTTUNE_AVAILABLE_KEYS.set(amfmtuneinfo.buttonValues.toString())
        DataPoolDataHandler.manualTuneData.set(amfmtuneinfo)
    }

    override fun onAMFM_RES_FMRDSSWITCH(pData: Int) {
        DataPoolDataHandler.FMTUNER_RDSSWITCH.set(pData)
    }

    override fun onAMFM_RES_REGIONSETTING(pData: Int) {
        DataPoolDataHandler.FMTUNER_REGIONSETTING.set(pData)
    }

    override fun onAMFM_RES_TPSTATUS(amfmtpstatus: AMFMTPStatus_t) {
        DataPoolDataHandler.FMTUNER_TPSTATUS.set(amfmtpstatus.status)
        DataPoolDataHandler.FMTUNER_TPSTATUS_NOWPLAYING.set(0F)
        DataPoolDataHandler.FMTUNER_TPSTATUS_LABELINDICATOR_NOWPLAYING.set(0F)
        DataPoolDataHandler.FMTUNER_TPSTATUS_SEARCHSTATE.set(0F)
        DataPoolDataHandler.FMTUNER_PROGRAMTYPERDSNOTIFICATION_NAME.set("")
        DataPoolDataHandler.FMTUNER_PROGRAMTYPERDSNOTIFICATION_CONTENT.set("")

        if (amfmtpstatus.state == eTPStationState.AMFM_TP_SEARCH_INITIATED) {
            DataPoolDataHandler.FMTUNER_TPSEARCH_UPDATE.set(AudioService.instance!!.getString(R.string.tp_search_fm))
            DataPoolDataHandler.audioControlDisabled.set(true)
            DataPoolDataHandler.audioControlDisabledforFM.set(true)
            Log.i(SystemListener::class.java.simpleName, "success")
        } else {
            DataPoolDataHandler.FMTUNER_TPSEARCH_UPDATE.set("")
            DataPoolDataHandler.audioControlDisabled.set(false)
            DataPoolDataHandler.audioControlDisabledforFM.set(false)
        }
    }

    override fun onAMFM_RES_TRAFFICALERT(pData: String) {
        DataPoolDataHandler.FMTUNER_TRAFFICALERT_NAME.set(pData)
    }

    override fun onAMFM_RES_TRAFFICALERTACTIVECALL(amfmtrafficinfo: AMFMTrafficInfo_t) {
        DataPoolDataHandler.FMTUNER_TRAFFICALERTACTIVECALL_NAME.set(amfmtrafficinfo.name)
        DataPoolDataHandler.FMTUNER_TRAFFICALERTACTIVECALL_PROGRAMIDENTIFIER.set(amfmtrafficinfo.programIdentifier)
    }

    override fun onAMFM_RES_PROGRAMTYPERDSNOTIFICATION(amfmprogramtypealert: AMFMProgramTypeAlert_t) {
        DataPoolDataHandler.FMTUNER_PROGRAMTYPERDSNOTIFICATION_NAME.set(amfmprogramtypealert.name)
        DataPoolDataHandler.FMTUNER_PROGRAMTYPERDSNOTIFICATION_CONTENT.set(amfmprogramtypealert.content)
    }

    override fun onAMFM_RES_AMSTRONGSTATIONLISTUPDATEFAILED(pData: String) {
    }

    override fun onAMFM_RES_FMSTRONGSTATIONLISTUPDATEFAILED(pData: String) {
    }

    override fun onAMFM_RES_CANCELAMSTRONGSTATIONLISTUPDATE(pData: String) {
        DataPoolDataHandler.showUpdateProgress.set(false)
        DataPoolDataHandler.audioControlDisabled.set(false)
    }

    override fun onAMFM_RES_CANCELFMSTRONGSTATIONLISTUPDATE(pData: String) {
        DataPoolDataHandler.showUpdateProgress.set(false)
        DataPoolDataHandler.audioControlDisabled.set(false)
    }

    override fun onAMFM_RES_TRAFFICALERT_END(pData: String) {
    }

    override fun onAMFM_RES_TRAFFICINFOSTART(pData: String) {
    }

    override fun onAMFM_RES_STATIONAVAILABALITYSTATUS(fmstationavailabilityinfo: FMStationAvailabilityInfo_t) {
        DataPoolDataHandler.FMTUNER_STATIONAVAILABILITY_METADATA.set(fmstationavailabilityinfo.metadata)
        DataPoolDataHandler.FMTUNER_STATIONAVAILABILITY_STATUS.set(0F)
    }

    override fun onAMFM_RES_ACTIONUNAVAILABLE(pData: String) {
    }

    /**
     * call this method to set current source type
     * @param waveBand constant fom NOWPLAYING_SOURCE_TYPE
     */
    fun onCurrentSourceChanged(waveBand: NOWPLAYING_SOURCE_TYPE) {
        previousSourceType = waveBand
        ManualTunerController.validateSourceType(waveBand.name)
        DataPoolDataHandler.AUDIOMANAGER_CHANGE_AUDIOSOURCE.set(waveBand)
    }

    /**
     * call this method when device(audio source) is connected/disconnected. It saves the device info for later use
     * @param iDevice connected device
     */
    fun onDeviceChanged(iDevice: IDevice) {
        val dData = DeviceData()
        dData.type = iDevice.type.toString()
        dData.id = iDevice.objectID.toString()
        dData.isReadable = iDevice.deviceMightBePlayable.toString()
        dData.isBrowsable = iDevice.canBrowse().toString()
        if (iDevice.volumeName.isNotEmpty()) {
            dData.name = iDevice.volumeName.toString()
        }
        deviceData.set(dData)
    }

    /**
     * this method is called to add/remove station as favorite.
     * Station is added as favorite if not available in the favorite list otherwise it will be removed from the list
     * @param stationInfo station to be added/removed as favorite
     */
    fun updateFavorites(stationInfo: AMFMStationInfo_t) {

        if (AMFMTUNER_CATEGORYSTATIONLIST_OBJECTLIST.size <= 0) {
            val filterItem = browseList.filter { it -> it.frequency.toString() == stationInfo.frequency.toString() }
            if (filterItem.isNotEmpty()) {
                val index = browseList.indexOf(filterItem[0])
                if (stationInfo.isFavorite) {
                    stationInfo.isFavorite = false
                    DataPoolDataHandler.browseList[index] = stationInfo
                    DataPoolDataHandler.browseList[index].isFavorite = false
                    DataPoolDataHandler.favoriteList.remove(stationInfo.frequency.toString())
                } else {
                    DataPoolDataHandler.favoriteList.add(stationInfo.frequency.toString())
                    stationInfo.isFavorite = true
                    DataPoolDataHandler.browseList[index] = stationInfo
                    DataPoolDataHandler.browseList[index].isFavorite = true
                }
                if (filterItem[0].frequency.toString() == DataPoolDataHandler.aMFMStationInfo_t.get()!!.frequency.toString()) {
                    aMFMStationInfo_t.set(addAMFMStation(stationInfo, stationInfo.isFavorite))
                }
            }

        } else {
            val filterItem = DataPoolDataHandler.AMFMTUNER_CATEGORYSTATIONLIST_OBJECTLIST.filter { it -> it.frequency.toString() == stationInfo.frequency.toString() }
            if (filterItem.isNotEmpty()) {
                val index = DataPoolDataHandler.AMFMTUNER_CATEGORYSTATIONLIST_OBJECTLIST.indexOf(filterItem[0])
                if (stationInfo.isFavorite) {
                    stationInfo.isFavorite = false
                    DataPoolDataHandler.AMFMTUNER_CATEGORYSTATIONLIST_OBJECTLIST[index] = stationInfo
                    DataPoolDataHandler.favoriteList.remove(stationInfo.frequency.toString())
                } else {
                    DataPoolDataHandler.favoriteList.add(stationInfo.frequency.toString())
                    stationInfo.isFavorite = true
                    DataPoolDataHandler.AMFMTUNER_CATEGORYSTATIONLIST_OBJECTLIST[index] = stationInfo
                }
                if (filterItem[0].frequency.toString() == DataPoolDataHandler.aMFMStationInfo_t.get()!!.frequency.toString()) {
                    aMFMStationInfo_t.set(addAMFMStation(stationInfo, stationInfo.isFavorite))
                }
            }
        }
    }

    /**
     * Returns list of station based on category
     * @param categoryType any constant of [eRDSPtyNACategory]
     * @return ArrayList<AMFMStationInfo_t>
     */
    private fun getAMFMCategoryListByType(categoryType: eRDSPtyNACategory): MutableMap<String, ArrayList<AMFMStationInfo_t>> {
        val stationList: ArrayList<AMFMStationInfo_t> = ArrayList()
        val map: MutableMap<String, ArrayList<AMFMStationInfo_t>> = hashMapOf()
        val filterItems = browseList.filter { it -> it.ptyCategory == categoryType.value }
        stationList.addAll(filterItems)
        map["" + categoryType] = stationList
        return map
    }

    //update category list
    fun updateCategoriesList(categoryList: MutableList<AMFMStationInfo_t>?) {
        DataPoolDataHandler.amfmCategoryList.clear()
        Log.e("CATEGORYSTATIONLIST", "updateCategoriesList :: " + categoryList?.size)
        if (categoryList != null && categoryList.isNotEmpty()) {
            DataPoolDataHandler.amfmCategoryList.addAll(categoryList)
        } else {
            DataPoolDataHandler.amfmCategoryList.add(addAMFMCategory_t(eRDSPtyNACategory.AMFM_RDS_PTY_NA_POP, getAMFMCategoryListByType(eRDSPtyNACategory.AMFM_RDS_PTY_NA_POP)))
            DataPoolDataHandler.amfmCategoryList.add(addAMFMCategory_t(eRDSPtyNACategory.AMFM_RDS_PTY_NA_ROCK, getAMFMCategoryListByType(eRDSPtyNACategory.AMFM_RDS_PTY_NA_ROCK)))
            DataPoolDataHandler.amfmCategoryList.add(addAMFMCategory_t(eRDSPtyNACategory.AMFM_RDS_PTY_NA_TALK, getAMFMCategoryListByType(eRDSPtyNACategory.AMFM_RDS_PTY_NA_TALK)))
            DataPoolDataHandler.amfmCategoryList.add(addAMFMCategory_t(eRDSPtyNACategory.AMFM_RDS_PTY_NA_COUNTRY, getAMFMCategoryListByType(eRDSPtyNACategory.AMFM_RDS_PTY_NA_COUNTRY)))
            DataPoolDataHandler.amfmCategoryList.add(addAMFMCategory_t(eRDSPtyNACategory.AMFM_RDS_PTY_NA_CLASSICAL, getAMFMCategoryListByType(eRDSPtyNACategory.AMFM_RDS_PTY_NA_CLASSICAL)))
            DataPoolDataHandler.amfmCategoryList.add(addAMFMCategory_t(eRDSPtyNACategory.AMFM_RDS_PTY_NA_JAZZ, getAMFMCategoryListByType(eRDSPtyNACategory.AMFM_RDS_PTY_NA_JAZZ)))
        }
    }

    //add category data
    private fun addAMFMCategory_t(categoryType: eRDSPtyNACategory, map: MutableMap<String, ArrayList<AMFMStationInfo_t>>): AMFMStationInfo_t {
        return Builder()
                .stationName("" + categoryType)
                .categoryData(map).build()
    }

    //add FMAM station data
    private fun addAMFMStation(stationInfo: AMFMStationInfo_t, isFavorite: Boolean): AMFMStationInfo_t {
        return Builder()
                .frequency(stationInfo.frequency)
                .stationName(stationInfo.stationName!!)
                .rdsStatus(eRdsStatus.AMFM_RDS_AVAILABLE)
                .objectId(stationInfo.objectId)
                .rdsStationInfo(stationInfo.rdsStationInfo!!)
                .ptyCategory(stationInfo.ptyCategory)
                .tpStationStatus(eTPStationStatus.AMFM_STATION_DOES_NOT_SUPPORT_TP)
                .isFavorite(isFavorite)
                .build()
    }

    /**
     * Displays Quick Notice using given message
     * @param message Description for quick notice
     */
    fun showQuickNotice(message: String) {
        Toast.makeText(AudioService.instance, message, Toast.LENGTH_SHORT).show() // TODO
    }

}