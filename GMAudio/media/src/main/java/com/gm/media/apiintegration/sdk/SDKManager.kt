package com.gm.media.apiintegration.sdk

import android.graphics.Bitmap
import com.gm.media.R
import com.gm.media.apiintegration.AudioService
import com.gm.media.apiintegration.SystemListener
import com.gm.media.apiintegration.apiinterfaces.IManager
import com.gm.media.models.*
import com.gm.media.models.DataPoolDataHandler.aMFMStationInfo_t
import com.gm.media.models.DataPoolDataHandler.browseList
import com.gm.media.utils.CategoriesUtil
import com.gm.media.utils.FavoriteDataProcessor
import com.gm.media.utils.Log
import com.gm.media.utils.ManualTunerController
import com.gm.media.utils.ManualTunerController.amLowerValue
import com.gm.media.utils.ManualTunerController.amStepValue
import com.gm.media.utils.ManualTunerController.amUpperValue
import com.gm.media.utils.ManualTunerController.fmLowerValue
import com.gm.media.utils.ManualTunerController.fmStepValue
import com.gm.media.utils.ManualTunerController.fmUpperValue
import com.gm.media.utils.ManualTunerController.getEnteredFrequency
import com.gm.media.utils.ManualTunerController.getFreqTxtForDisplay
import com.gm.media.utils.ManualTunerController.getFrequency
import com.gm.media.utils.ManualTunerController.getPossibleValues
import com.gm.media.utils.ManualTunerController.isValidEntry
import com.gm.media.utils.ManualTunerController.mCurrentModeChart
import com.gm.media.utils.ManualTunerController.prepareChart
import com.gm.media.utils.ManualTunerController.updateButtons
import gm.media.CMediaPlayer
import gm.media.enums.*
import gm.media.exceptions.ExceptionDeviceFunctionNotEnabled
import gm.media.exceptions.ExceptionInitializationFailed
import gm.media.exceptions.ExceptionPlayFunctionNotEnabled
import gm.media.interfaces.*
import gm.media.utilities.MediaRunnable
import gm.tuner.*


/**
 *  It should contain AMFM_REQ, MEDIA_REQ, AUDIOMGR_REQ, DAB_REQ, SDARS_REQ only func from CAMFMProxy.cpp, CAudioManagerProxy.cpp, CMediaProxy.cpp, CSDARSProxy.cpp & CDABProxy.cpp.
 *  This file will also contain SDK calls from GM SDK.
 *  only some requests that are being used contain "AMFM_REQ" in function definition, only these should be added
 *  these funcs usually start after proxy constructor func in CAMFMProxy.cpp
 *  This should exclude all the functions under RecvData func, as the func in RecvData only corresponds to RES functions
 */
class SDKManager : TunerListener, IDeviceListener, IPlaybackListener, IManager {

    private var mTunerManager: TunerManager? = null
    private var mTunerInitializationState: Int? = 0
    private val mTAG = "SDKManager"
    private var mIDevice: IDevice? = null
    private var mCMediaPlayer: CMediaPlayer? = null
    private var isTrafficAlertPlaying: Boolean = false
    private var regionCode: RegionCodeInfo? = null
    private var mCurrentCategoryType: String? = eRDSPtyNACategory.AMFM_RDS_PTY_NA_NO_PTY.name

    /**
     * It initializes TunerManager. Sets waveband of TunerManager and updating to [SystemListener]
     *
     * @throws exception throws exception of Type ExceptionInInitializerError, TunerException
     */
    @Throws(Exception::class)
    private fun registerTunerListener() {

        try {
            Log.d(mTAG, "registerTunerListener...")
            // If tuner manager is null then initialise it
            if(mTunerManager == null){
                Log.d(mTAG, "before TunerManager initialized ")
                mTunerManager = TunerManager()
                Log.d(mTAG, "after  TunerManager initialized ")
                // Region code for Manual tune values
                regionCode = mTunerManager?.regionCodeInfo
            }
            mTunerInitializationState = mTunerManager?.tunerAudioStatus
            tunerListenerRegistration()

        } catch (e: ExceptionInInitializerError) {
            e.printStackTrace()
        } catch (e: TunerException) {
            e.printStackTrace()
        } catch (exception: Exception) {
            exception.printStackTrace()
        }

    }

    /**
     * Adds this class as listener for Tuner event
     * @throws exception throws exception of Type TunerServiceStatusException, TunerException Exceptions
     */
    private fun tunerListenerRegistration() {
        Log.d(mTAG, "Going to add listener for tuner...")
        try {
            mTunerManager?.addListener(this)
            Log.d(mTAG, "Going to add listener for tuner...")
        } catch (e: TunerServiceStatusException) {
            Log.e(mTAG, "Exception in TunerServiceStatusException..." + e.localizedMessage)
            e.printStackTrace()
        } catch (e: TunerException) {
            Log.e(mTAG, "Tuner Exception while setting tuner listener" + e.localizedMessage)
        } catch (e: Exception) {
            Log.e(mTAG, e.localizedMessage)
        }
        Log.d(mTAG, "going to notifyTunerApiStateChanged ")
    }

    /************************Tuner Listener Call Back Methods*******************************/

    override fun onCurrentStationChanged(stationData: TunerStation?) {
        if (stationData == null) {
            Log.d(mTAG, "onCurrentStationChanged Null")
            return
        }

        when (stationData.getWaveband()) {
            NOWPLAYING_SOURCE_TYPE.AM.name -> {
                SystemListener.onAMFM_RES_AMCURRENTSTATIONINFO(addAMFMHardWareStation(stationData, DataPoolDataHandler.favoriteList.contains(stationData.frequency.toFloat().toString()),
                        eTPStationStatus.AMFM_STATION_DOES_NOT_SUPPORT_TP, stationData.frequency.toFloat()))
            }
            else -> {
                val freq = stationData.frequency.toDouble() / 1000
                val eTPStationStatus = if (stationData.isTrafficProgramme)
                    eTPStationStatus.AMFM_STATION_SUPPORTS_TP
                else
                    eTPStationStatus.AMFM_STATION_DOES_NOT_SUPPORT_TP

                SystemListener.onAMFM_RES_FMCURRENTSTATIONINFO(addAMFMHardWareStation(stationData, DataPoolDataHandler.favoriteList.contains(freq.toFloat().toString()),
                        eTPStationStatus, freq.toFloat()))
                isTrafficAlertRDSNotification(stationData,"","")

            }
        }

    }


    override fun onAvailableStationListChanged(stationList: MutableList<TunerStation>?) {

            processStationList(stationList)

    }

    override fun onStaticStationListChanged(stationList: MutableList<TunerStation>?) {
        if (stationList == null) {
            Log.d(mTAG, "onStaticStationListChanged Null")
            return
        }
    }

    override fun onRadioTextChanged(p0: RadioText?) {
        //TODO Need to Update Radio Text when signal RDS signal gets notified
    }

    override fun onRadioTextPlusChanged(p0: MutableList<RadioTextPlus>?) {
        //TODO Need to Update Radio Text when signal RDS signal gets notified
    }

    override fun onFavoritesChanged(p0: MutableList<TunerFavorite>?) {
        Log.d("List::", "List::" + p0)
    }

    override fun onAutoStoreStatusChanged(autoStoreStatus: AutoStoreStatus?) {
        if (autoStoreStatus!!.status == AutoStoreStatus.Status.ERROR) {
            SystemListener.showQuickNotice(AudioService.instance!!.getString(R.string.update_failed))
        }else if(autoStoreStatus.status == AutoStoreStatus.Status.IN_PROGRESS){
                when (DataPoolDataHandler.AUDIOMANAGER_CHANGE_AUDIOSOURCE.get()) {
                    NOWPLAYING_SOURCE_TYPE.AM -> {
                        SystemListener.onAMFM_RES_AMSTRONGSTATIONLISTPROGRESS(autoStoreStatus.getProgress())
                    }
                    NOWPLAYING_SOURCE_TYPE.FM-> {
                        SystemListener.onAMFM_RES_FMSTRONGSTATIONLISTPROGRESS(autoStoreStatus.getProgress())
                    }
                }
        }
        else if(autoStoreStatus.status == AutoStoreStatus.Status.ABORTED){
            when (DataPoolDataHandler.AUDIOMANAGER_CHANGE_AUDIOSOURCE.get()) {
                NOWPLAYING_SOURCE_TYPE.AM -> {
                    SystemListener.onAMFM_RES_CANCELAMSTRONGSTATIONLISTUPDATE("")
                }
                NOWPLAYING_SOURCE_TYPE.FM-> {
                    SystemListener.onAMFM_RES_CANCELFMSTRONGSTATIONLISTUPDATE("")
                }
            }
        }
    }

    override fun onHdStatusInfoChanged(p0: String?) {
    }


    override fun onRdsTimeChanged(p0: RdsTime?) {
    }

    override fun onSeekStatusChanged(p0: String?) {
        if (p0.equals("SEEK_UP")) {

        } else if (p0.equals("SEEK_DOWN")) {

        } else if (p0.equals("SEEK_STOP")) {

        }
    }

    /*SDK Response Listener Implementation*/
    override fun onRdsStateChanged(p0: Boolean) {
    }

    /************************Tuner Listener Call Back Methods*******************************/


    /************************Device Listener Call Back Methods*******************************/

    override fun onDeviceAvailableChange(p0: Boolean, iDevice: IDevice?): Boolean {
        if (iDevice == null) {
            setIDevice(null)
            return false
        }
        setIDevice(iDevice)
        Log.i(mTAG, "device.getObjectID() :: ${iDevice.objectID}")
        Log.i(mTAG, "device.getType() :: ${iDevice.type}")
        Log.i(mTAG, "device.canBrowse() :: ${iDevice.canBrowse()}")
        Log.i(mTAG, "device.getVolumeName() :: ${iDevice.volumeName}")
        if (iDevice.type == EnumDeviceType.USBMSD && iDevice.isMounted &&
                aMFMStationInfo_t.get()?.rdsStationInfo == NOWPLAYING_SOURCE_TYPE.USBMSD.name) {
            try {
                mCMediaPlayer?.playDevice(mIDevice)
            } catch (e: ExceptionPlayFunctionNotEnabled) {
                Log.e(mTAG!!, e.localizedMessage)
            }
        }
        return true
    }

    override fun onDeviceAttributeChange(iDevice: IDevice?): Boolean {
        Log.i(mTAG, "onDeviceAttributeChange :: $iDevice")
        if (iDevice == null) {
            setIDevice(null)
            return false
        }
        setIDevice(iDevice)
        Log.i(mTAG, "device.getObjectID() :: ${iDevice.objectID}")
        Log.i(mTAG, "device.getType() :: ${iDevice.type}")
        Log.i(mTAG, "device.canBrowse() :: ${iDevice.canBrowse()}")
        Log.i(mTAG, "device.getVolumeName() :: ${iDevice.volumeName}")
        if (iDevice.type == EnumDeviceType.USBMSD) {
            SystemListener.onDeviceChanged(iDevice)
        }
        return true
    }

    override fun getListAdapter(): IDeviceListAdapter? {
        return null
    }

    override fun onDeviceSyncError(iDevice: IDevice?, p1: String?): Boolean {
        return false
    }

    override fun onVolumeRemoved(p0: Long): Boolean {
        return false
    }
    /************************Device Listener Call Back Methods*******************************/


    /************************PlayBack Listener Call Back Methods*******************************/

    override fun onAlbumArtChange(p0: Bitmap?): Boolean {
        return false
    }

    override fun onMetadataChange(iMetadata: IMetadata?): Boolean {
        if (iMetadata == null) {
            return false
        }
        return true
    }

    override fun onPlayStateChange(p0: EnumPlayState?): Boolean {
        return false
    }

    override fun onRepeatStateChange(p0: EnumRepeatState?): Boolean {
        return false
    }

    override fun onShuffleStateChange(p0: EnumShuffleState?): Boolean {
        return false
    }

    override fun onTrackTimeChange(p0: Int, p1: Int, p2: Float): Boolean {
        return false
    }

    override fun onTrackCountChange(p0: Int, p1: Int): Boolean {
        return false
    }

    override fun onActiveDeviceChange(p0: Long) {
    }


    /************************PlayBack Listener Call Back Methods*******************************/


    /************************IPC Request Methods*******************************/

    override fun onAMFM_REQ_AMSEEKSTATION(any: Any?) {
        seekAmFMStation(any)
    }

    override fun onAMFM_REQ_FMSEEKSTATION(any: Any?) {
        seekAmFMStation(any)
    }

    private fun seekAmFMStation(any: Any?) {
        if (isTrafficAlertPlaying) {
            onAMFM_REQ_TRAFFICALERT_DISMISS()
        }
        try {
            mTunerManager?.seekTuneRelease()
            when (any as eAMFMSeekType) {
                eAMFMSeekType.AMFM_SEEK_TYPE_SEEKUP -> mTunerManager?.seekNextAvailable(TunerManager.Direction.UP)
                eAMFMSeekType.AMFM_SEEK_TYPE_SEEKDOWN -> mTunerManager?.seekNextAvailable(TunerManager.Direction.DOWN)
                eAMFMSeekType.AMFM_SEEK_TYPE_FASTSEEKUP -> mTunerManager?.seekUp()
                eAMFMSeekType.AMFM_SEEK_TYPE_FASTSEEKDOWN -> mTunerManager?.seekDown()
                else -> {
                    mTunerManager?.seekNextAvailable(TunerManager.Direction.UP)
                }
            }
        } catch (e: TunerException) {
            Log.e(mTAG!!, e.printStackTrace().toString())
        } catch (e: Exception) {
            Log.e(mTAG!!, e.printStackTrace().toString())
        }
    }

    override fun onAMFM_REQ_AMTUNEBYFREQUENCY(any: Any?) {
        if (any != null) {
            val frequency = any.toString().toDouble()
            tuneTo(frequency.toInt())
        }
    }

    override fun onAMFM_REQ_FMTUNEBYFREQUENCY(any: Any?) {
        if (any != null) {
            val frequency = (any.toString()).toDouble()
            tuneTo((frequency * 1000).toInt())

            isTrafficAlertRDSNotification(mTunerManager?.currentStation, DataPoolDataHandler.FMTUNER_PROGRAMTYPERDSNOTIFICATION_NAME.get().toString(),
                    DataPoolDataHandler.FMTUNER_PROGRAMTYPERDSNOTIFICATION_CONTENT.get().toString())

        }
    }

    override fun onAMFM_REQ_AMTUNESTATION(any: Any?) {
        tuneStation(any)
    }

    override fun onAMFM_REQ_FMTUNESTATION(any: Any?) {
        tuneStation(any)
    }

    private fun tuneStation(any: Any?) {
        try {
            mTunerManager?.seekTuneRelease()
            when (any as eAMFMTuneType) {
                eAMFMTuneType.AMFM_TUNE_TYPE_TUNEUP -> mTunerManager?.tuneOneStep(TunerManager.Direction.UP)
                eAMFMTuneType.AMFM_TUNE_TYPE_TUNEDOWN -> mTunerManager?.tuneOneStep(TunerManager.Direction.DOWN)
                eAMFMTuneType.AMFM_TUNE_TYPE_FASTTUNEUP -> mTunerManager?.tuneNext()
                eAMFMTuneType.AMFM_TUNE_TYPE_FASTTUNEDOWN -> mTunerManager?.tunePrevious()
                else -> {
                    mTunerManager?.tuneNext()
                }
            }
        } catch (e: TunerException) {
            Log.e(mTAG!!, e.localizedMessage)
        } catch (e: Exception) {
            Log.e(mTAG!!, e.localizedMessage)
        }
    }

    override fun onAMFM_REQ_AMSTATIONLIST() {
        if (mTunerManager == null) {
            Log.e(mTAG!!, "Tuner Manager is Null")
            return
        }
        try {
            onSourceReset(NOWPLAYING_SOURCE_TYPE.AM)
            setWaveBand(TunerManager.Waveband.AM)
            val amfmStationInfo_t:AMFMStationInfo_t?=null
            SystemListener.onAMFM_RES_AMCURRENTSTATIONINFO(amfmStationInfo_t!!)
            previousList!!.clear()
            mCurrentCategoryType = eRDSPtyNACategory.AMFM_RDS_PTY_NA_ALL.name
            CategoriesUtil.getPTYValues(mCurrentCategoryType)
        } catch (e: TunerException) {
            Log.e(mTAG!!, e.localizedMessage)
        } catch (e: Exception) {
            Log.e(mTAG!!, e.localizedMessage)
        }
    }

    override fun onAMFM_REQ_FMSTATIONLIST() {
        if (mTunerManager == null) {
            Log.e(mTAG!!, "Tuner Manager is Null")
            return
        }
        try {
            onSourceReset(NOWPLAYING_SOURCE_TYPE.FM)
            setWaveBand(TunerManager.Waveband.FM)
            val amfmStationInfo_t:AMFMStationInfo_t?=null
            SystemListener.onAMFM_RES_FMCURRENTSTATIONINFO(amfmStationInfo_t!!)
            previousList!!.clear()
            mCurrentCategoryType = eRDSPtyNACategory.AMFM_RDS_PTY_NA_ALL.name
            CategoriesUtil.getPTYValues(mCurrentCategoryType)
        } catch (e: TunerException) {
            Log.e(mTAG!!, e.localizedMessage)
        } catch (e: Exception) {
            Log.e(mTAG!!, e.localizedMessage)
        }

    }

    override fun onAMFM_REQ_AMHARDSEEKSTATION(any: Any?) {
        seekAmFMStation(any)
    }

    override fun onAMFM_REQ_FMHARDSEEKSTATION(any: Any?) {
        seekAmFMStation(any)
    }

    override fun onAMFM_REQ_UPDATEAMSTRONGSTATIONSLIST() {
        if (mTunerManager == null) {
            Log.e(mTAG!!, "Tuner Manager is Null")
            return
        }
        try {
            if (mTunerManager?.autoStoreStatus?.status != AutoStoreStatus.Status.IN_PROGRESS) {
                mTunerManager?.seekTuneRelease()
                mTunerManager!!.autoStoreUpdate()
            }
        } catch (e: TunerException) {
            Log.e(mTAG!!, e.localizedMessage)
        } catch (e: Exception) {
            Log.e(mTAG!!, e.localizedMessage)
        }
    }

    override fun onAMFM_REQ_UPDATEFMSTRONGSTATIONSLIST() {
        //updateStrongStationList()
        if (mTunerManager == null) {
            Log.e(mTAG!!, "Tuner Manager is Null")
            return
        }
        try {
            if (mTunerManager?.autoStoreStatus?.status != AutoStoreStatus.Status.IN_PROGRESS) {
                mTunerManager?.seekTuneRelease()
                mTunerManager!!.autoStoreUpdate()
            }
        } catch (e: TunerException) {
            Log.e(mTAG!!, e.localizedMessage)
        } catch (e: Exception) {
            Log.e(mTAG!!, e.localizedMessage)
        }
    }

    override fun onAMFM_REQ_AMTUNEBYOBJECTID(any: Any?) {
        if (mTunerManager == null) {
            Log.e(mTAG!!, "Tuner Manager is Null")
            return
        }
        try {
            mTunerManager?.setStationByListId(any as Int)
        } catch (e: TunerException) {
            Log.e(mTAG!!, e.localizedMessage)
        } catch (e: Exception) {
            Log.e(mTAG!!, e.localizedMessage)
        }
    }

    override fun onAMFM_REQ_AMTUNEBYPARTIALFREQUENCY(any: Any?) {
        val frequencyKeys: String = getFrequency(any)!!
        if (regionCode != null) {
            ManualTunerController.amLowerValue = regionCode?.amLower?.toInt()!!
            amUpperValue = regionCode?.amUpper?.toInt()!!
            amStepValue = regionCode?.amStep?.toInt()!!
        }
        mCurrentModeChart = prepareChart(amLowerValue.toLong(), amUpperValue.toLong(), amStepValue.toLong())
        val enteredFrequency: Long = getEnteredFrequency(frequencyKeys)
        val buttonValues = updateButtons(amLowerValue == amUpperValue, getPossibleValues(enteredFrequency))
        SystemListener.onAMFM_RES_AMTUNEBYPARTIALFREQUENCY(ManualTuneData(getFreqTxtForDisplay(frequencyKeys), isValidEntry(frequencyKeys), buttonValues))

    }

    override fun onAMFM_REQ_CANCELAMSTRONGSTATIONSLISTUPDATE() {
        abortAutoStore()
    }

    override fun onAMFM_REQ_CANCELFMSTRONGSTATIONSLISTUPDATE() {
        abortAutoStore()
    }

    private fun abortAutoStore() {
        if (mTunerManager == null) {
            Log.e(mTAG!!, "Tuner Manager is Null")
            return
        }
        try {
            mTunerManager?.autoStoreAbort()
        } catch (e: TunerException) {
            Log.e(mTAG!!, e.printStackTrace().toString())
        } catch (e: Exception) {
            Log.e(mTAG!!, e.printStackTrace().toString())
        }
    }

    override fun onAMFM_REQ_FMTUNEBYOBJECTID(any: Any?) {
        try {
            mTunerManager?.setStationByListId(any as Int)
        } catch (e: TunerException) {
            Log.e(mTAG!!, e.printStackTrace().toString())
        } catch (e: Exception) {
            Log.e(mTAG!!, e.printStackTrace().toString())
        }
    }

    override fun onAMFM_REQ_FMTUNEBYPARTIALFREQUENCY(any: Any?) {
        val frequencyKeys: String = getFrequency(any)!!
        if (regionCode != null) {
            fmLowerValue = regionCode?.fmLower?.toInt()!!
            fmUpperValue = regionCode?.fmUpper?.toInt()!!
            fmStepValue = regionCode?.fmStep?.toInt()!!
        }
        mCurrentModeChart = prepareChart(fmLowerValue.toLong(), fmUpperValue.toLong(), fmStepValue.toLong())
        val enteredFrequency: Long = getEnteredFrequency(frequencyKeys)
        val buttonValues = updateButtons(fmLowerValue == fmUpperValue, getPossibleValues(enteredFrequency))
        SystemListener.onAMFM_RES_FMTUNEBYPARTIALFREQUENCY(ManualTuneData(getFreqTxtForDisplay(frequencyKeys), isValidEntry(frequencyKeys), buttonValues))
    }

    override fun onAMFM_REQ_AMCATEGORYSTATIONLIST(any: Any?) {
        mCurrentCategoryType = (any as AMFMStationInfo_t).stationName
        val filterList = CategoriesUtil.getPTYValues(mCurrentCategoryType)
        if (filterList != null && filterList.size > 0) {
            mTunerManager?.setPtyListFilter(filterList)
        } else {
            SystemListener.onAMFM_RES_AMCATEGORYSTATIONLIST(any)
        }
    }

    override fun onAMFM_REQ_FMCATEGORYSTATIONLIST(any: Any?) {
        mCurrentCategoryType = (any as AMFMStationInfo_t).stationName
        val filterList = CategoriesUtil.getPTYValues(mCurrentCategoryType)
        if (filterList != null && filterList.size > 0) {
            mTunerManager?.setPtyListFilter(filterList)
        } else {
            SystemListener.onAMFM_RES_FMCATEGORYSTATIONLIST(any)
        }
    }

    override fun onAMFM_REQ_FMRDSSWITCH(rdsswitch: Int) {
        try {
            when (rdsswitch) {
                1 -> mTunerManager?.isRdsEnabled = true
                else -> mTunerManager?.isRdsEnabled = false
            }
        } catch (e: TunerException) {
            Log.e(mTAG!!, e.printStackTrace().toString())
        } catch (e: Exception) {
            Log.e(mTAG!!, e.printStackTrace().toString())
        }
    }

    override fun onAMFM_REQ_TPSTATUS(any: Any?) {

    }

    override fun onAMFM_REQ_FMTUNETMCSTATION(programIdentifier: Int) {
    }

    override fun onAMFM_REQ_TRAFFICALERT_DISMISS() {

    }

    override fun onAMFM_REQ_TRAFFICALERT_LISTEN() {

    }

    override fun onAMFM_REQ_SETREGIONSETTING(state: Int) {
        SystemListener.onAMFM_RES_REGIONSETTING(mTunerManager!!.regionalizationMode)
    }

    /************************IPC Request Methods*******************************/

    /************************IManager Methods*******************************/

    /**
     * Initialises SDK Manager
     */
    init {
        INIT_AMFM_REQUEST()
    }

    override fun INIT_AMFM_REQUEST() {
        registerTunerListener()
        try {

            if (mTunerManager?.waveband == TunerManager.Waveband.AM) {
                setWaveBand(TunerManager.Waveband.AM)
                SystemListener.onCurrentSourceChanged(NOWPLAYING_SOURCE_TYPE.AM)
            } else {
                setWaveBand(TunerManager.Waveband.FM)
                SystemListener.onCurrentSourceChanged(NOWPLAYING_SOURCE_TYPE.FM)
            }

            processStationList(mTunerManager?.availableStationList)//TODO Need to remove code, as onAvailableStationListChanged() is taking 15 seconds of time to get available station list

        } catch (e: TunerException) {
            Log.e(mTAG!!, e.printStackTrace().toString())
        } catch (e: Exception) {
            Log.e(mTAG!!, e.printStackTrace().toString())
        }
        FavoriteDataProcessor
    }

    override fun onCategoryList() {
        val categoryList: MutableList<AMFMStationInfo_t>? = CategoriesUtil.getCategoriesInformationFromStations(browseList)
        SystemListener.updateCategoriesList(categoryList)
    }

    override fun onSourceReset(sourceTYPE: NOWPLAYING_SOURCE_TYPE?) {
        when (DataPoolDataHandler.previousSourceType) {
            NOWPLAYING_SOURCE_TYPE.AM -> {
                resetTuner()
            }
            NOWPLAYING_SOURCE_TYPE.FM -> {
                resetTuner()
            }
            NOWPLAYING_SOURCE_TYPE.USBMSD, NOWPLAYING_SOURCE_TYPE.AUX -> {
                resetMedia()
            }
        }
        SystemListener.onCurrentSourceChanged(sourceTYPE!!)
    }

    override fun onAMFMTuneRequest(any: Any?) {
        try {
            if (mTunerManager?.currentStation!!.waveband == NOWPLAYING_SOURCE_TYPE.AM.name) {
                onAMFM_REQ_AMTUNEBYFREQUENCY(any)
            } else {
                onAMFM_REQ_FMTUNEBYFREQUENCY(any)
            }
        } catch (e: TunerException) {
            Log.e(mTAG!!, e.printStackTrace().toString())
        } catch (e: Exception) {
            Log.e(mTAG!!, e.printStackTrace().toString())
        }
    }

    override fun onFavoriteRequest(any: Any?) {
        if (DataPoolDataHandler.audioControlDisabled.get()!!) {
            SystemListener.showQuickNotice(AudioService.instance!!.getString(R.string.favorite_unavailable))
            return
        }
        if (any != null) {
            FavoriteDataProcessor.addOrRemoveFavorite(any as AMFMStationInfo_t)
            SystemListener.updateFavorites(any)
        }
    }

    override fun onAMFM_REQ_GREYEDBUTTON_CLICK() {
        SystemListener.showQuickNotice(AudioService.instance!!.getString(R.string.audio_action_not_supported))
    }

    /**
     * IConfig is used to Configure the CMediaPlayer
     */
    private val mConfig = IConfig {
        when (it) {
            IConfig.enumConfigItems.AudioZone -> EnumAudioZone.MainCabin
            IConfig.enumConfigItems.BrowseReturnEmptyContainers -> true
            IConfig.enumConfigItems.BrowseReturnUnplayables -> true
            IConfig.enumConfigItems.BrowseSupport -> true
            IConfig.enumConfigItems.CinemoLogLevel -> android.util.Log.WARN
            IConfig.enumConfigItems.DeviceSupport -> true
            IConfig.enumConfigItems.EnableBrowseArt -> true
            IConfig.enumConfigItems.EnableNowPlayingArt -> false
            IConfig.enumConfigItems.EnableTrackListArt -> true
            IConfig.enumConfigItems.EnableVideoSupport -> false
            IConfig.enumConfigItems.MediaLogLevel -> android.util.Log.INFO
            IConfig.enumConfigItems.PlaybackSupport -> true
            IConfig.enumConfigItems.ResumeOnNewTrack -> false
            else -> null
        }
    }

    /**
     * Initialises instance of CMediaPlayer
     *
     * @throws exception throws exception of Type PlayFunctionNotEnabled, ExceptionInitializationFailed Exceptions
     */
    @Throws(Exception::class)
    private fun registerMediaListener() {
        try {
            mCMediaPlayer = CMediaPlayer(mConfig)
            setupMediaEngine()
        } catch (e: ExceptionPlayFunctionNotEnabled) {
            e.printStackTrace()
        } catch (e: ExceptionInitializationFailed) {
            e.printStackTrace()
        }
    }

    /**
     * Sets this(SDKManager) class as listener for event on Playback and Device
     *
     * @throws exception throws exception of Type ExceptionDeviceFunctionNotEnabled, ExceptionPlayFunctionNotEnabled Exceptions
     */
    @Throws(Exception::class)
    private fun setupMediaEngine() {
        try {
            mCMediaPlayer?.addListener(this as IPlaybackListener)
            mCMediaPlayer?.addListener(this as IDeviceListener)
            mCMediaPlayer?.requestDevices()
        } catch (e: ExceptionDeviceFunctionNotEnabled) {
            e.printStackTrace()
        } catch (e: ExceptionPlayFunctionNotEnabled) {
            e.printStackTrace()
        }
    }


    /**
     * It returns current station
     * @return TunerStation
     * @throws exception throws exception of Type TunerException
     */
    @Throws(TunerException::class)
    fun getCurrentStation(): TunerStation {
        return mTunerManager?.currentStation!!
    }

    /**
     * It returns the current tuner waveband
     *
     * @return NOWPLAYING_SOURCE_TYPE
     * @throws exception throws exception of Type TunerException
     */
    @Throws(TunerException::class)
    fun isFMorAM(): NOWPLAYING_SOURCE_TYPE {
        var sourceType: NOWPLAYING_SOURCE_TYPE = NOWPLAYING_SOURCE_TYPE.AM
        when (DataPoolDataHandler.AUDIOMANAGER_CHANGE_AUDIOSOURCE.get()!!.name) {
            "AM" -> {
                sourceType = NOWPLAYING_SOURCE_TYPE.AM
            }
            "FM" -> {
                sourceType = NOWPLAYING_SOURCE_TYPE.FM
            }
        }
        return sourceType
    }

    /**
     * It returns current Tuner Manager RadioText
     *
     * @return instance of RadioText
     * @throws exception throws exception of Type TunerException
     */
    @Throws(TunerException::class)
    fun getRadioText(): RadioText {
        return mTunerManager?.radioText!!
    }

    /**
     * sets  waveband for Tuner Manager
     *
     * @param wband any constant for TunerManager.Waveband
     * @throws exception throws exception of Type TunerException
     */
    @Throws(TunerException::class)
    private fun setWaveBand(wband: TunerManager.Waveband) {
        mTunerManager?.waveband = wband
    }

    /**
     * This method is used to jump directly to a frequency on a different waveband, rather than send a setWaveband followed by a setFrequency call.
     *
     * @param wband any constant for TunerManager.Waveband
     * @param var2 The desired frequency parameter is an integer for both AM and FM.
     * @param var3 secondary program service  values ranges --> AM	0 = analog channel 1 = HD content
     *                                                          FM	Range: 0 - 8
     * @throws exception throws exception of Type TunerException
     */
    @Throws(TunerException::class)
    fun setWavebandAndFrequency(wband: TunerManager.Waveband, var2: Int, var3: Int) {
        mTunerManager?.setWavebandAndFrequency(wband, var2, var3)
    }

    /**
     * this method tunes the tuner manager to given frequency
     * @param frequency
     * @throws exception throws exception of Type TunerException
     */
    @Throws(TunerException::class)
    private fun tuneTo(frequency: Int) {
        mTunerManager?.tuneTo(frequency)
    }

    /**
     * To be called when the user cancels the seek operation
     * @throws exception throws exception of Type TunerException
     */
    @Throws(TunerException::class)
    fun seekCancel() {
        mTunerManager?.seekCancel()
    }

    /**
     * checks whether HD mode is enabled or not
     * @return True/false
     * @throws exception throws exception of Type TunerException
     */
    @Throws(TunerException::class)
    fun isHDModeEnabled(): Boolean? {
        return mTunerManager?.isHDModeEnabled
    }

    /**
     * checks whether RDS is enabled or not
     * @return True/false
     * @throws exception throws exception of Type TunerException
     */
    @Throws(TunerException::class)
    fun isRDSEnabled(): Boolean? {
        return mTunerManager?.isRdsEnabled
    }


    /**
     * Converts MutableList<TunerStation> to ArrayList<AMFMStationInfo_t>
     * @param stationList AM/FM station list
     * @return AMFMStationList_t
     */
    /*SDK Request Listener Implementation*/
    var previousList:MutableList<TunerStation>?=null
    private fun processStationList(stationList: MutableList<TunerStation>?) {
        val stationListData = ArrayList<AMFMStationInfo_t>()

        val list_t: MutableList<TunerStation> = if (stationList!!.size <= 0)
            mTunerManager!!.staticStationList
        else
            stationList

        if(previousList != null)
            if (areListsEqual(previousList!!, list_t)) return //TODO Test these changes

        try {

            when (DataPoolDataHandler.AUDIOMANAGER_CHANGE_AUDIOSOURCE.get()) {
                NOWPLAYING_SOURCE_TYPE.AM -> {
                    list_t.forEach {
                        if (it != null) {
                            stationListData.add(addAMFMHardWareStation(it, DataPoolDataHandler.favoriteList.contains(it.frequency.toFloat().toString()),
                                    eTPStationStatus.AMFM_STATION_DOES_NOT_SUPPORT_TP, it.frequency.toFloat()))
                        }
                    }
                    if (DataPoolDataHandler.BROWSE_SOURCE_TYPE.get().equals(AudioService.instance!!.getString(R.string.categoryinfo)))
                        SystemListener.onAMFM_RES_AMCATEGORYSTATIONLIST(addAMFMCategory_t(mCurrentCategoryType, getAMFMCategoryListByType(mCurrentCategoryType!!, stationListData)))
                    else
                        SystemListener.onAMFM_RES_AMSTRONGSTATIONSLIST(AMFMStationList_t(stationListData.size, stationListData))
                }
                NOWPLAYING_SOURCE_TYPE.FM -> {
                    list_t.forEach {
                        if (it != null) {
                            val freq = it.frequency.toDouble() / 1000
                            val eTPStationStatus = if (it.isTrafficProgramme)
                                eTPStationStatus.AMFM_STATION_SUPPORTS_TP
                            else
                                eTPStationStatus.AMFM_STATION_DOES_NOT_SUPPORT_TP

                            stationListData.add(addAMFMHardWareStation(it, DataPoolDataHandler.favoriteList.contains(freq.toFloat().toString()),
                                    eTPStationStatus, freq.toFloat()))
                        }
                    }
                    if (DataPoolDataHandler.BROWSE_SOURCE_TYPE.get().equals(AudioService.instance!!.getString(R.string.categoryinfo)))
                        SystemListener.onAMFM_RES_FMCATEGORYSTATIONLIST(addAMFMCategory_t(mCurrentCategoryType, getAMFMCategoryListByType(mCurrentCategoryType!!, stationListData)))
                    else
                        SystemListener.onAMFM_RES_FMSTRONGSTATIONSLIST(AMFMStationList_t(stationListData.size, stationListData))
                }
            }

        } catch (e: TunerException) {
            Log.e(mTAG!!, e.printStackTrace().toString())
        } catch (e: Exception) {
            Log.e(mTAG!!, e.printStackTrace().toString())
        }

        previousList=list_t

    }

    /**
     * called this method to release Tuner Manager
     */
    private fun resetTuner() {
        try {
            mTunerManager?.seekTuneRelease()
            Log.v(mTAG, "resetTuner")
        } catch (e: TunerException) {
            Log.e(mTAG!!, e.printStackTrace().toString())
        } catch (e: Exception) {
            Log.e(mTAG!!, e.printStackTrace().toString())
        }
    }

    private fun resetMedia() {
    }


    /*IDeviceListener Implementation*/
    override fun onErrorImpossible(p0: MediaRunnable?) {
    }

    override fun onErrorOperationProhibited(p0: MediaRunnable?) {
    }

    override fun onErrorConnectionFailed(p0: MediaRunnable?) {
    }

    override fun onErrorConnectionRefused(p0: MediaRunnable?) {
    }

    override fun onErrorFailed(p0: MediaRunnable?) {
    }

    override fun onErrorTimeout(p0: MediaRunnable?) {
    }

    override fun onErrorOperationFailed(p0: MediaRunnable?) {
    }

    override fun onErrorUnexpected(p0: MediaRunnable?) {
    }

    override fun onErrorNotConnected(p0: MediaRunnable?) {
    }

    /**
     *  Sets the current connected device
     *  @param iDevice device
     */
    private fun setIDevice(iDevice: IDevice?) {
        mIDevice = iDevice
    }

    //add FMAM Hardware station data
    private fun addAMFMHardWareStation(stationInfo: TunerStation, isFavorite: Boolean, eTPStationStatus: eTPStationStatus, frequency: Float): AMFMStationInfo_t {
        return Builder()
                .frequency(frequency)
                .stationName(stationInfo.hdStationNameLong)
                .rdsStatus(eRdsStatus.AMFM_RDS_AVAILABLE)
                .objectId(stationInfo.id.toInt())
                .rdsStationInfo(DataPoolDataHandler.AUDIOMANAGER_CHANGE_AUDIOSOURCE.get()!!.name)
                .ptyCategory(stationInfo.ptyCode.toInt())
                .tpStationStatus(eTPStationStatus)
                .isFavorite(isFavorite)
                .build()
    }

    //add FMAM station data
    private fun addAMFMStation(stationInfo: AMFMStationInfo_t, isFavorite: Boolean): AMFMStationInfo_t {
        return Builder()
                .frequency(stationInfo.frequency)
                .stationName(stationInfo.stationName)
                .rdsStatus(eRdsStatus.AMFM_RDS_AVAILABLE)
                .objectId(stationInfo.objectId)
                .rdsStationInfo(stationInfo.rdsStationInfo)
                .ptyCategory(stationInfo.ptyCategory)
                .tpStationStatus(eTPStationStatus.AMFM_STATION_DOES_NOT_SUPPORT_TP)
                .isFavorite(isFavorite)
                .build()
    }

    // Compares lists
    fun areListsEqual(list1: List<Any>, list2: List<Any>): Boolean {
        Log.v(mTAG, "Previous.size :: " + list1.size + "---- Current.size :: " + list2.size)
        if (list1.size == list2.size) {
            Log.v(mTAG, "Both lists have same data ?? " + list1.equals(list2))
            return list1.equals(list2)
        } else return false
    }

    fun isTrafficAlertRDSNotification(stationData: TunerStation?,notification:String,content:String){

    }

    /**
     * Returns list of station based on category
     * @param categoryType any constant of String
     * @return ArrayList<AMFMStationInfo_t>
     */
    private fun getAMFMCategoryListByType(categoryType: String, stationListData: ArrayList<AMFMStationInfo_t>): MutableMap<String, ArrayList<AMFMStationInfo_t>> {
        val stationList: ArrayList<AMFMStationInfo_t> = ArrayList()
        val map: MutableMap<String, ArrayList<AMFMStationInfo_t>> = hashMapOf()
        stationList.addAll(stationListData)
        map["" + categoryType] = stationList
        return map
    }

    //add category data
    private fun addAMFMCategory_t(categoryType: String?, map: MutableMap<String, ArrayList<AMFMStationInfo_t>>): AMFMStationInfo_t {
        return Builder()
                .stationName("" + categoryType)
                .categoryData(map).build()
    }

}
