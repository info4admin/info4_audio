package com.gm.media.utils

//HS:

//HS:END

import android.util.Log
import com.gm.media.models.AMFMStationInfo_t
import com.gm.media.models.Builder
import com.gm.media.models.eRDSPtyNACategory
import com.gm.media.models.DataPoolDataHandler.EUOrNA
import gm.calibrations.LANGUAGEANDREGIONALIZATIONGLOBALA_ENUM
import gm.tuner.RBDS_PTY
import gm.tuner.RDS_PTY
import java.util.*


/**
 * This is an utility class for Tuner categories
 *
 */
object CategoriesUtil {

    val TAG = CategoriesUtil::class.java.simpleName
    private lateinit var mPtyMapping: Array<String>
    private var mNAFullPtyList: Array<Int>? = null
    private var mEUFullPtyList: Array<Int>? = null
    val categoriesList = ArrayList<AMFMStationInfo_t>()

    init {
        loadPtyValueMappingWithStrings()
    }

    /**
     * Mapping pty with the corresponding category as per GIS-502 Audio UI
     * V1.4.1 18JUL2014.pdf, 4.1.26.5*MY17 PTY Table In the below mapping key
     * value refers to the PTY type and the value refers to the category. In the
     * GIS documnet column 1 & 2 for US-NA and columns 3 & 4 for EU markets.
     */
    private fun loadPtyValueMappingWithStrings() {
        val AllStr = eRDSPtyNACategory.AMFM_RDS_PTY_NA_ALL.toString()
        val rock = eRDSPtyNACategory.AMFM_RDS_PTY_NA_ROCK.toString()
        val talk = eRDSPtyNACategory.AMFM_RDS_PTY_NA_TALK.toString()
        val pop = eRDSPtyNACategory.AMFM_RDS_PTY_NA_POP.toString()
        val country = eRDSPtyNACategory.AMFM_RDS_PTY_NA_COUNTRY.toString()
        val classical = eRDSPtyNACategory.AMFM_RDS_PTY_NA_CLASSICAL.toString()
        val jazz = eRDSPtyNACategory.AMFM_RDS_PTY_NA_JAZZ.toString()
        val musicCat = eRDSPtyNACategory.AMFM_RDS_PTY_NA_MUSIC.toString()
        val information = eRDSPtyNACategory.AMFM_RDS_PTY_NA_INFORMATION.toString()
        val weather = eRDSPtyNACategory.AMFM_RDS_PTY_NA_WEATHER.toString()
        val region = LANGUAGEANDREGIONALIZATIONGLOBALA_ENUM.REGIONS.GMNA.ordinal
        if (region == LANGUAGEANDREGIONALIZATIONGLOBALA_ENUM.REGIONS.GMNA.ordinal) {
            mPtyMapping = arrayOf(AllStr, talk, talk, talk, talk, rock, rock, pop, rock, pop, country, pop, rock, pop, jazz, classical, rock, rock, talk, AllStr, talk, talk, talk, pop, AllStr, AllStr, AllStr, AllStr, AllStr, talk, AllStr, AllStr)
        } else if (region == LANGUAGEANDREGIONALIZATIONGLOBALA_ENUM.REGIONS.EUROPE.ordinal) {
            mPtyMapping = arrayOf(AllStr, information, information, information, information, information, information, information, information, pop, rock, musicCat, classical, classical, musicCat, information, information,
                    information, information, information, information, information, information, musicCat, musicCat, musicCat, musicCat, musicCat, information, AllStr, AllStr)
        }
    }

    /**
     * returns region PTY values for the given type
     *
     * @param type
     * @return
     */
    fun getPTYValues(type: String?): IntArray? {
        if (type == null || categoriesList == null) {
            return null
        }
        if (EUOrNA == LANGUAGEANDREGIONALIZATIONGLOBALA_ENUM.REGIONS.GMNA) {
            return getNAPtyValues(type)
        } else if (EUOrNA == LANGUAGEANDREGIONALIZATIONGLOBALA_ENUM.REGIONS.EUROPE) {
            return getEUPtyValues(type)
        }
        return null
    }

    /**
     * Return the pty arraylist cvalues for the given type for NA region
     *
     * @param type
     * @return ArrayList<Integer>
    </Integer> */
    private fun getNAPtyValues(type: String): IntArray {
        Log.d(TAG, "getNAPtyValues, type=$type")
        var ptyValues: IntArray? = null
        // RBDS_PTY for NA
        if (type == eRDSPtyNACategory.AMFM_RDS_PTY_NA_POP.toString()) {
            ptyValues = intArrayOf(RBDS_PTY.ADULT_HITS, RBDS_PTY.TOP_40, RBDS_PTY.OLDIES, RBDS_PTY.NOSTALGIA, RBDS_PTY.COLLEGE)
        } else if (type == eRDSPtyNACategory.AMFM_RDS_PTY_NA_ROCK.toString()) {
            ptyValues = intArrayOf(RBDS_PTY.ROCK, RBDS_PTY.CLASSIC_ROCK, RBDS_PTY.SOFT_ROCK, RBDS_PTY.SOFT, RBDS_PTY.RHYTHM_AND_BLUES, RBDS_PTY.SOFT_RHYTHM_AND_BLUES)
        } else if (type == eRDSPtyNACategory.AMFM_RDS_PTY_NA_TALK.toString()) {
            ptyValues = intArrayOf(RBDS_PTY.NEWS, RBDS_PTY.INFORMATION, RBDS_PTY.SPORTS, RBDS_PTY.TALK, RBDS_PTY.FOREIGN_LANGUAGE,
                    RBDS_PTY.RELIGIOUS_TALK, RBDS_PTY.PERSONALITY, RBDS_PTY.PUBLIC, RBDS_PTY.WEATHER)
        } else if (type == eRDSPtyNACategory.AMFM_RDS_PTY_NA_COUNTRY.toString()) {
            ptyValues = intArrayOf(RBDS_PTY.COUNTRY)
        } else if (type == eRDSPtyNACategory.AMFM_RDS_PTY_NA_CLASSICAL.toString()) {
            ptyValues = intArrayOf(RBDS_PTY.CLASSICAL)
        } else if (type == eRDSPtyNACategory.AMFM_RDS_PTY_NA_JAZZ.toString()) {
            ptyValues = intArrayOf(RBDS_PTY.JAZZ)
        } else {
            Log.d(TAG, "getNAPtyValues: NO_PTY")
            ptyValues = intArrayOf(RBDS_PTY.NO_PTY, RBDS_PTY.RELIGIOUS_MUSIC)
        }
        return ptyValues
    }

    /**
     * Return the pty arraylist cvalues for the given type for EU region
     *
     * @param type
     * @return ArrayList<Integer>
    </Integer> */
    private fun getEUPtyValues(type: String): IntArray {
        Log.d(TAG, "getEUPtyValues, type=$type")
        var ptyValues: IntArray? = null
        // RDS_PTY for EU
        if (type == eRDSPtyNACategory.AMFM_RDS_PTY_NA_POP.toString()) {
            ptyValues = intArrayOf(RDS_PTY.POP_MUSIC)
        } else if (type == eRDSPtyNACategory.AMFM_RDS_PTY_NA_ROCK.toString()) {
            ptyValues = intArrayOf(RDS_PTY.ROCK_MUSIC)
        } else if (type == eRDSPtyNACategory.AMFM_RDS_PTY_NA_MUSIC.toString()) {
            ptyValues = intArrayOf(RDS_PTY.EASY_LISTENING, RDS_PTY.OTHER_MUSIC, RDS_PTY.JAZZ_MUSIC, RDS_PTY.COUNTRY_MUSIC, RDS_PTY.NATIONAL_MUSIC, RDS_PTY.OLDIES_MUSIC, RDS_PTY.FOLK_MUSIC)
        } else if (type == eRDSPtyNACategory.AMFM_RDS_PTY_NA_CLASSICAL.toString()) {
            ptyValues = intArrayOf(RDS_PTY.LIGHT_CLASSICAL, RDS_PTY.SERIOUS_CLASSICAL)
        } else if (type == eRDSPtyNACategory.AMFM_RDS_PTY_NA_INFORMATION.toString()) {
            ptyValues = intArrayOf(RDS_PTY.NEWS, RDS_PTY.CURRENT_AFFAIRS, RDS_PTY.INFORMATION, RDS_PTY.SPORT, RDS_PTY.EDUCATION, RDS_PTY.DRAMA, RDS_PTY.CULTURE,
                    RDS_PTY.SCIENCE, RDS_PTY.VARIED, RDS_PTY.WEATHER, RDS_PTY.FINANCE, RDS_PTY.CHILDRENS_PROGRAMMES, RDS_PTY.SOCIAL_AFFAIRS, RDS_PTY.RELIGION,
                    RDS_PTY.PHONE_IN, RDS_PTY.TRAVEL, RDS_PTY.LEISURE, RDS_PTY.DOCUMENTARY)
        } else {
            Log.d(TAG, "getEUPtyValues: NO_PTY")
            ptyValues = intArrayOf(RDS_PTY.NO_PTY)
        }
        return ptyValues
    }

    /**
     * This method will create the list of categories list based on the station
     * data pty values
     *
     * @param stationList
     * @return
     */
    fun getCategoriesInformationFromStations(stationList: ArrayList<AMFMStationInfo_t>?): ArrayList<AMFMStationInfo_t>? {
        if (stationList == null || stationList.size == 0) {
            return null
        }
        categoriesList.clear()
        val categoriesTitleList = ArrayList<String>()
        var isAllRequired = false
        val allStr = eRDSPtyNACategory.AMFM_RDS_PTY_NA_ALL.toString()
        for (i in stationList.indices) {
            if (stationList.get(i) != null && stationList.get(i).ptyCategory != 0) {
                val cat = mPtyMapping[stationList.get(i).ptyCategory]
                if (allStr.equals(cat, ignoreCase = true)) {
                    // Add All categories option in the end of the list, hence
                    // continue with further items
                    // As Per GM All should always be at the Bottom
                    isAllRequired = true
                    continue
                } else if (cat != null && cat.length > 0 && !categoriesTitleList.contains(cat)) {
                    categoriesTitleList.add(cat)
                    addAMFMStationName(cat)
                    categoriesList.add(stationList[i])
                }
            }
        }

        if (isAllRequired) {
            addAMFMStationName(allStr)
        }
        return categoriesList
    }

    //add category station
    private fun addAMFMStationName(categoryName: String): AMFMStationInfo_t {
        return Builder()
                .stationName(categoryName).build()
    }
}
