package com.gm.media.utils

import com.gm.media.BuildConfig


/**
 * @author Nagaraja Reddy Mallu on 7/10/2018.
 */

class Log {
    companion object {

        private var COMMON_PREFIX = "[#GMAudio#]"
        private var isLogsEnable = BuildConfig.isDebug

        // ======= DEBUG =============================
        fun d(tag: String?, msg: String, vararg args: Any) {
            if (isLogsEnable) {
                val sb = StringBuffer()
                if (args != null) {
                    for (i in args.indices) {
                        sb.append(args[i])
                    }
                }
                android.util.Log.d(tag, COMMON_PREFIX +" " + msg + " " + sb.toString())
            }
        }

        // ======= ERROR =============================
        fun e(tag: String?, msg: String, vararg args: Any) {
            if (isLogsEnable) {
                val sb = StringBuffer()
                if (args != null) {
                    for (i in args.indices) {
                        sb.append(args[i])
                    }
                }
                android.util.Log.e(tag, COMMON_PREFIX +" " +msg + " " + sb.toString())
            }
        }

        // ======= INFORMATION =============================
        fun i(tag: String?, msg: String, vararg args: Any) {
            if (isLogsEnable) {
                val sb = StringBuffer()
                if (args != null) {
                    for (i in args.indices) {
                        sb.append(args[i])
                    }
                }
                android.util.Log.i(tag, COMMON_PREFIX +" " +msg + " " + sb.toString())
            }
        }

        // ======= VERBOSE =============================
        fun v(tag: String?, msg: String, vararg args: Any) {
            if (isLogsEnable) {
                val sb = StringBuffer()
                if (args != null) {
                    for (i in args.indices) {
                        sb.append(args[i])
                    }
                }
                android.util.Log.v(tag, COMMON_PREFIX +" " +msg + " " + sb.toString())
            }
        }

        // ======= WARNING =============================
        fun w(tag: String?, msg: String, vararg args: Any) {
            if (isLogsEnable) {
                val sb = StringBuffer()
                if (args != null) {
                    for (i in args.indices) {
                        sb.append(args[i])
                    }
                }
                android.util.Log.w(tag, COMMON_PREFIX +" " +msg + " " + sb.toString())
            }
        }

    }

}