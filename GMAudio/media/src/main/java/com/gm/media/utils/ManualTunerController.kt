package com.gm.media.utils

import android.content.ContentValues
import android.text.TextUtils
import android.util.Log
import com.gm.media.R
import com.gm.media.apiintegration.AudioService
import com.gm.media.apiintegration.SystemListener
import com.gm.media.models.DataPoolDataHandler
import com.gm.media.models.ManualTuneData
import com.gm.media.models.NOWPLAYING_SOURCE_TYPE
import java.util.*

/**
 * This class is to perform manual tune actions
 * Validates the channel number entered by user. It restricts the user from entering the frequency
 * beyond the range of AM/FM
 */
object ManualTunerController {

    const val eventName: String = "onAMFMTuneRequest"
    val resources = AudioService.instance!!.resources!!
    private val mNumberZero: String = resources.getString(R.string.ui_0_short)
    private val mNumberOne: String = resources.getString(R.string.ui_1_short)
    private val mNumberFive: String = resources.getString(R.string.ui_5_short)
    private val mNumberEight: String = resources.getString(R.string.ui_8_short)
    private val mNumberNine: String = resources.getString(R.string.ui_9_short)
    val mPeriod: String = resources.getString(R.string.manual_tuner_period)
    private val FM_UPPER_LIMIT_CHARACTERISTIC =
            resources.getString(R.string.fm_upper_limit_characteristics)
    //The lower limit of FM channels in kilohertz before the decimal point.
    private val FM_LOWER_LIMIT_NO_DECIMAL_KHZ =
            resources.getInteger(R.integer.fm_lower_limit_decimal_khz)
    private val KILOHERTZ_CONVERSION_DIGITS =
            resources.getString(R.string.KILOHERTZ_CONVERSION_DIGITS)
    private val KILOHERTZ_CONVERSION_DIGITS_WITH_DECIMAL =
            resources.getString(R.string.KILOHERTZ_CONVERSION_DIGITS_WITH_DECIMAL)
    private var mChannelValidator: ChannelValidator? = null
    private val mAmChannelValidator = AmChannelValidator()
    private val mFmChannelValidator = FMChannelValidator()

    /**
     * AM frequency starting range is  530
     */
    var amLowerValue = resources.getInteger(R.integer.amLowerValue)
    /**
     * AM frequency Upper range is 1700
     */
    var amUpperValue = resources.getInteger(R.integer.amUpperValue)
    /**
     * AM step range is 10
     */
    var amStepValue = resources.getInteger(R.integer.amStepValue)
    /**
     * FM frequency starting range is 87900
     */
    var fmLowerValue = resources.getInteger(R.integer.fmLowerValue)
    /**
     * FM frequency Upper range is 107900
     */
    var fmUpperValue = resources.getInteger(R.integer.fmUpperValue)
    /**
     * FM step range is 200
     */
    var fmStepValue = resources.getInteger(R.integer.fmStepValue)
    val FM_STATION_FRACTION_VALUE = resources.getInteger(R.integer.FM_STATION_FRACTION_VALUE)
    var mCurrentModeChart: Map<String, ChartDetails>? = null
    private val FIRST_DIGIT = resources.getInteger(R.integer.FIRST_DIGIT)
    private val DECIMAL_AFTER_TWO_INDEXES =
            resources.getInteger(R.integer.DECIMAL_AFTER_TWO_INDEXES)
    private val DECIMAL_AFTER_THREE_INDEXES =
            resources.getInteger(R.integer.DECIMAL_AFTER_THREE_INDEXES)
    val zero = resources.getInteger(R.integer.zero)
    val one = resources.getInteger(R.integer.ONE)
    val two = resources.getInteger(R.integer.two)
    val three = resources.getInteger(R.integer.three)
    val five = resources.getInteger(R.integer.five)
    val seven = resources.getInteger(R.integer.seven)
    val eight = resources.getInteger(R.integer.eight)

    init {
        validateSourceType(DataPoolDataHandler.aMFMStationInfo_t.get()?.rdsStationInfo)
    }

    /**
     * Checking source type whether it is AM/FM
     * @param name it will be AM or FM
     */
    fun validateSourceType(name: String?) {
        DataPoolDataHandler.AMTUNER_DIRECTTUNE_PARTIAL_FREQUENCY.set("")
        DataPoolDataHandler.FMTUNER_DIRECTTUNE_PARTIAL_FREQUENCY.set("")
        mChannelValidator = if (name == NOWPLAYING_SOURCE_TYPE.AM.name)
            mAmChannelValidator
        else
            mFmChannelValidator
        updateAllButtons(StringBuilder())
    }

    /**
     * To validate given channel is valid or not
     */
    interface ChannelValidator {
        /**
         * Appending Manual tune keyboard button values to current frequency
         * @param character index of buttons
         * @param number Manually entered frequency
         */
        fun canAppendCharacterToNumber(character: String, number: String): Boolean

        /**
         * To validate channel is in correct format or not
         * @param number Manually entered frequency through keypad
         * @return Returns true, if channel is valid
         */
        fun isValidChannel(number: String): Boolean

        /**
         * Converting number to Hz's
         * @param number Manually entered frequency through keypad
         */
        fun convertToHz(number: String): Int

        /**
         * To check manually entered frequency is to append or not
         * @param number Returns true, if channel is need to append to period
         */
        fun shouldAppendPeriod(number: String): Boolean
    }

    /**
     * Updating all the keypad buttons according to source type  whether AM/FM
     * @param mCurrentChannel StringBuilder
     */
    private fun updateAllButtons(mCurrentChannel: StringBuilder) {
        val buttonValues = ManualTuneData().buttonValues
        buttonValues.forEachIndexed { index, _ ->
            run {

                val enabled = mChannelValidator!!.canAppendCharacterToNumber(index.toString(),
                        mCurrentChannel.toString())
                buttonValues[index] = enabled
            }
        }
        DataPoolDataHandler.manualTuneData.set(ManualTuneData(mCurrentChannel.toString(),
                isChannelValid(mCurrentChannel), buttonValues))
    }


    /**
     * checking the channel is valid or not according to AM_LOWER_LIMIT is 530,
     * AM_UPPER_LIMIT is 1700, FM_LOWER_LIMIT is 87900, FM_UPPER_LIMIT is 107900
     * @param mCurrentChannel current channel frequency
     * @return True, if channel is valid
     */
    private fun isChannelValid(mCurrentChannel: StringBuilder): Boolean {
        if (!TextUtils.isEmpty(mCurrentChannel.toString())) {
            return mChannelValidator!!.isValidChannel(mCurrentChannel.toString())
        }
        return false
    }
    /**
     * A [ChannelValidator] for the AM band. Note that this validator is for US regions.
     */
    class AmChannelValidator : ChannelValidator {

        override fun canAppendCharacterToNumber(character: String,
                                                number: String): Boolean {
            if (character == mPeriod) {
                return false
            }

            val charValue = Integer.valueOf(character)
            return when (number.length) {
                zero ->
                    charValue >= five || charValue == one
                one ->
                    number != mNumberFive || charValue >= three
                two -> {
                    if (number[zero].toString() == mNumberOne) {
                        true
                    } else character == mNumberZero
                }
                three ->
                    character == mNumberZero
                else ->
                    false
            }
        }

        override fun isValidChannel(number: String): Boolean {
            if (number.isEmpty() or number.contains(mPeriod)) {
                return false
            }
            val value = Integer.valueOf(number)
            return value in amLowerValue..amUpperValue
        }

        override fun convertToHz(number: String): Int {
            return Integer.valueOf(number)
        }

        override fun shouldAppendPeriod(number: String): Boolean {
            return false
        }
    }

    /**
     * A [ChannelValidator] for the FM band. Note that this validator is for US regions.
     */
    class FMChannelValidator : ChannelValidator {

        override fun canAppendCharacterToNumber(character: String,
                                                number: String): Boolean {
            val indexOfPeriod = number.indexOf(mPeriod)
            if (character == mPeriod) {
                return if (indexOfPeriod != -one) {
                    false
                } else number.length >= two
            }
            if (number.isEmpty()) {
                val charValue = Integer.valueOf(character)
                return charValue >= eight || charValue == one
            }
            if (indexOfPeriod == -one) {
                when (number.length) {
                    one -> {
                        if (number == mNumberOne) {
                            return character == mNumberZero
                        }
                        if (number == mNumberEight) {
                            val numberValue = Integer.valueOf(character)
                            return numberValue >= seven
                        }
                        return true
                    }
                    two ->
                        return (number[zero].toString() == mNumberOne
                                && character != mNumberEight
                                && character != mNumberNine)
                    three ->
                        return false
                    else -> return false
                }
            } else if (number.length - one > indexOfPeriod) {
                return false
            }
            if (number == FM_UPPER_LIMIT_CHARACTERISTIC) {
                return character == mNumberNine
            }
            val charValue = Integer.valueOf(character)
            return charValue % two == one
        }

        override fun isValidChannel(number: String): Boolean {
            if (number.isEmpty()) {
                return false
            }
            val updatedNumber = convertNumberToKilohertz(number)
            val value = Integer.valueOf(updatedNumber)
            return value in fmLowerValue..fmUpperValue
        }

        override fun convertToHz(number: String): Int {
            return Integer.valueOf(convertNumberToKilohertz(number))
        }

        override fun shouldAppendPeriod(number: String): Boolean {
            if (number.contains(mPeriod)) {
                return false
            }
            val value = Integer.valueOf(number)
            return value >= FM_LOWER_LIMIT_NO_DECIMAL_KHZ
        }

        /**
         * Converts the given number to its kilohertz representation. For example, 87.9 will be
         * converted to 87900.
         * @param number adding [KILOHERTZ_CONVERSION_DIGITS_WITH_DECIMAL] or
         * [KILOHERTZ_CONVERSION_DIGITS] to change to int
         * @sample 87.9 will be converted to 87900.
         */
        private fun convertNumberToKilohertz(number: String): String {
            return if (number.contains(mPeriod)) {
                number.replace(mPeriod, "") + KILOHERTZ_CONVERSION_DIGITS_WITH_DECIMAL
            } else number + KILOHERTZ_CONVERSION_DIGITS
        }
    }

    /**
     *Preparing chart with [amLowerValue], [amUpperValue],
     *  [fmLowerValue], [fmStepValue], [amStepValue], [fmStepValue]
     * @param lower [amLowerValue], [fmLowerValue]
     * @param upper [amUpperValue], [fmUpperValue]
     * @param step [amStepValue], [fmStepValue]
     * @return ChartDetails object
     */

    fun prepareChart(lower: Long, upper: Long, step: Long): Map<String, ChartDetails> {
        val digitDetails = HashMap<String, ChartDetails>()
        var i = lower
        while (i in (zero + 1)..upper) {
            setValuemap(digitDetails, i)
            i += step
        }
        return Collections.unmodifiableMap(digitDetails)
    }

    /**
     * set values Map with frequency
     * @param digitDetails Map object for ChartDetails
     * @param value frequency value
     */
    private fun setValuemap(digitDetails: MutableMap<String, ChartDetails>, value: Long) {
        val strValue = java.lang.Long.toString(value)
        var holder: ChartDetails? = null
        for (i in zero until strValue.length) {
            if (i == zero) {
                if (!digitDetails.containsKey(strValue.substring(i, i + one))) {
                    holder = ChartDetails()
                    digitDetails[strValue.substring(i, i + one)] = holder
                } else {
                    holder = digitDetails[strValue.substring(i, i + one)]
                }
            } else if (i == one) {
                holder!!.addKeyValues(strValue.substring(i, i + one))
            } else {
                holder = holder!!.addPossibleChildrenValues(strValue.substring(i - one, i),
                        strValue.substring(i, i + one))
            }
        }
    }

    /**
     * Updating button based on AM_LOWER_LIMIT is 530, AM_UPPER_LIMIT is 1700,
     * FM_LOWER_LIMIT is 87900, FM_UPPER_LIMIT is 107900
     * @param requiredAll True, if entered frequency is in range
     * @param availableValues available values
     * @return Updating keyboard buttons according to this array
     */
    fun updateButtons(requiredAll: Boolean,
                      availableValues: java.util.ArrayList<String>?): Array<Boolean> {
        val buttons = DataPoolDataHandler.manualTuneData.get()!!.buttonValues
        for (i in buttons.indices) {
            buttons[i] = requiredAll
        }
        if (availableValues != null && availableValues.size > zero) {
            val iterator = availableValues.iterator()
            while (iterator.hasNext()) {
                val number = java.lang.Long.parseLong(iterator.next())
                buttons[number.toInt()] = true
            }
        }
        return buttons
    }

    /**
     * To get possible values from manual entered frequency
     * @param value Manually entered frequency through keypad
     * @return possible frequencies
     */
    fun getPossibleValues(value: Long): java.util.ArrayList<String>? {
        var values: java.util.ArrayList<String>? = null

        // if value is -1 then it is for the initial screen values when screen
        // direct tune is displayed
        if (value == -1L && mCurrentModeChart != null) {
            values = ArrayList(mCurrentModeChart!!.keys)
            return values
        }
        val numberInString = java.lang.Long.toString(value)
        if (mCurrentModeChart != null) {
            var holder: ChartDetails? = null
            for (i in zero until numberInString.length) {
                if (i == zero) {
                    holder = mCurrentModeChart!![numberInString.substring(i, i + one)]
                } else if (i > zero && holder != null) {
                    holder = holder.getChildrenObject(numberInString.substring(i, i + one))
                }
                if (i == numberInString.length - one) {
                    if (holder != null) {
                        values = holder.keyValues
                    } else {
                        values = null
                        return values
                    }
                }
            }
        }
        return values
    }

    /**
     * Forms the frequency text as per the format
     * @param frequencyKeys Manually entered frequency through keypad
     * @return formatted frequency
     */
    fun getFreqTxtForDisplay(frequencyKeys: String): String {
        val str = StringBuilder()
        val mUserInput = frequencyKeys.toCharArray()
        var firstDigit = -one
        if (!TextUtils.isEmpty(frequencyKeys)
                && !TextUtils.isEmpty(mUserInput[FIRST_DIGIT].toString())) {
            firstDigit = mUserInput[FIRST_DIGIT].toString().toInt()
        }
        for (index in mUserInput.indices) {
            str.append(mUserInput[index])
            if (DataPoolDataHandler.AUDIOMANAGER_CHANGE_AUDIOSOURCE.get()!!.name ==
                    NOWPLAYING_SOURCE_TYPE.FM.name) {
                /*
                 * Frequencies starting with 1 will be usually 4 digits(EX :
                 * 103.5) Frequencies starting with other than 1 will be usually
                 * 3 digits (ex: 89.5, 93.5)
                 */
                if (firstDigit == one) {
                    // For Frequencies starting with 1, decimal should be placed
                    // after 3 digits like(103.5)
                    if (index == DECIMAL_AFTER_THREE_INDEXES) {
                        str.append(".")
                    }
                    // Usually for frequencies other than 1, will be 3 digits.
                    // Decimal should be place after 2 digits
                } else if (index == DECIMAL_AFTER_TWO_INDEXES) {
                    str.append(".")
                }
            }
        }
        return str.toString()
    }

    /**
     * checking entered frequency is valid or not based on  [amLowerValue],
     * [amUpperValue], [fmLowerValue], [fmUpperValue]
     *@param frequencyKeys Manually entered frequency through keypad
     *@return Boolean  returns valid or not
     */
    fun isValidEntry(frequencyKeys: String): Boolean {
        var entry = frequencyKeys
        if (entry.length == zero) {
            return false
        }
        if (DataPoolDataHandler.AUDIOMANAGER_CHANGE_AUDIOSOURCE.get()?.name ==
                NOWPLAYING_SOURCE_TYPE.FM.name) {
            entry = entry.replace(".", "")
        }
        var enteredFrequency = zero
        if (DataPoolDataHandler.AUDIOMANAGER_CHANGE_AUDIOSOURCE.get()?.name ==
                NOWPLAYING_SOURCE_TYPE.FM.name) {
            try {
                enteredFrequency = (java.lang.Float.parseFloat(entry) *
                        FM_STATION_FRACTION_VALUE).toInt()
            } catch (e: NumberFormatException) {
                Log.i(ContentValues.TAG, "Entered Frequency is not vaild @FM")
            }
        } else {
            try {
                enteredFrequency = Integer.parseInt(entry)
            } catch (e: NumberFormatException) {
                Log.i(ContentValues.TAG, "Entered Frequency is not vaild")
            }
        }
        var lowerVal = amLowerValue
        var upperVal = amUpperValue
        var stepVal = amStepValue
        if (DataPoolDataHandler.AUDIOMANAGER_CHANGE_AUDIOSOURCE.get()?.name ==
                NOWPLAYING_SOURCE_TYPE.FM.name) {
            lowerVal = fmLowerValue
            upperVal = fmUpperValue
            stepVal = fmStepValue
        }

        return (enteredFrequency in lowerVal..upperVal
                && stepVal != zero && (enteredFrequency - lowerVal) % stepVal == zero)
    }

    /**
     * Getting manually entered frequency and converting into long
     * @param frequencyKeys frequency in string datatype
     * @return converted frequency
     */
    fun getEnteredFrequency(frequencyKeys: String): Long {
        val freq: String? = frequencyKeys
        if (freq != null && freq.length > zero) {
            return try {
                Integer.parseInt(freq).toLong()
            } catch (e: NumberFormatException) {
                Log.i(ContentValues.TAG, "updateButtons : cannot parse $freq")
                -1
            }
        }
        return -1
    }

    /**
     * To get Manually entered frequency and updating into [DataPoolDataHandler]
     * @param any Manually entered frequency through keypad
     * @return frequency value will return
     */
    fun getFrequency(any: Any?): String? {
        var frequencyKeys: String? = ""

        if (DataPoolDataHandler.AUDIOMANAGER_CHANGE_AUDIOSOURCE.get()?.name
                        .equals(NOWPLAYING_SOURCE_TYPE.FM.name)) {
            frequencyKeys = if (any != null && any is String && !TextUtils.isEmpty(any.toString())) {
                DataPoolDataHandler.FMTUNER_DIRECTTUNE_PARTIAL_FREQUENCY.get() + any.toString()
            } else {
                DataPoolDataHandler.FMTUNER_DIRECTTUNE_PARTIAL_FREQUENCY.get()
            }
        } else if (DataPoolDataHandler.AUDIOMANAGER_CHANGE_AUDIOSOURCE.get()?.name
                        .equals(NOWPLAYING_SOURCE_TYPE.AM.name)) {
            frequencyKeys = if (any != null && any is String && !TextUtils.isEmpty(any.toString())) {
                DataPoolDataHandler.AMTUNER_DIRECTTUNE_PARTIAL_FREQUENCY.get() + any.toString()
            } else {
                DataPoolDataHandler.AMTUNER_DIRECTTUNE_PARTIAL_FREQUENCY.get()
            }
        }
        return frequencyKeys?.replace(".", "")
    }

    /**
     * Deleting numbers when delete button clicked in Manual tune screen
     */
    fun onAMFM_REQ_AMFMDELETEBYPARTIALFREQUENCY() {

        var frequencyKeys = ""
        var freqText = ""
        if (DataPoolDataHandler.AUDIOMANAGER_CHANGE_AUDIOSOURCE.get()?.name
                        .equals(NOWPLAYING_SOURCE_TYPE.FM.name)) {
            freqText = DataPoolDataHandler.FMTUNER_DIRECTTUNE_PARTIAL_FREQUENCY.get()!!
        } else if (DataPoolDataHandler.AUDIOMANAGER_CHANGE_AUDIOSOURCE.get()?.name
                        .equals(NOWPLAYING_SOURCE_TYPE.AM.name)) {
            freqText = DataPoolDataHandler.AMTUNER_DIRECTTUNE_PARTIAL_FREQUENCY.get()!!
        }
        freqText = freqText.replace(".", "")
        var length = freqText.length
        val mUserInput = freqText.toCharArray().toCollection(ArrayList())
        if (freqText.contains(".")) {
            length -= one
        }
        if (length > zero && mUserInput.size >= length) {
            mUserInput.removeAt(length - one)
            mUserInput.forEach { frequencyKeys += it }
        }

        val enteredFrequency: Long
        val buttonValues: Array<Boolean>
        if (DataPoolDataHandler.AUDIOMANAGER_CHANGE_AUDIOSOURCE.get()!!.name ==
                NOWPLAYING_SOURCE_TYPE.FM.name) {
            mCurrentModeChart = prepareChart(fmLowerValue.toLong(), fmUpperValue.toLong(),
                    fmStepValue.toLong())

            enteredFrequency = getEnteredFrequency(frequencyKeys)

            buttonValues = updateButtons(fmLowerValue == fmUpperValue,
                    getPossibleValues(enteredFrequency))

            SystemListener.onAMFM_RES_FMTUNEBYPARTIALFREQUENCY(
                    ManualTuneData(getFreqTxtForDisplay(frequencyKeys),
                            isValidEntry(frequencyKeys), buttonValues))
        } else {

            mCurrentModeChart = prepareChart(amLowerValue.toLong(), amUpperValue.toLong(),
                    amStepValue.toLong())

            enteredFrequency = getEnteredFrequency(frequencyKeys)

            buttonValues = updateButtons(amLowerValue == amUpperValue,
                    getPossibleValues(enteredFrequency))

            SystemListener.onAMFM_RES_AMTUNEBYPARTIALFREQUENCY(
                    ManualTuneData(getFreqTxtForDisplay(frequencyKeys),
                            isValidEntry(frequencyKeys), buttonValues))
        }
    }

    /**
     * Preparing Chart Details for keys
     */
    class ChartDetails {
        var keyValues: ArrayList<String>? = null
            private set
        private var children: ArrayList<ChartDetails>? = null
        /**
         * Adding keys to arraylist
         * @param value frequency
         */
        fun addKeyValues(value: String) {
            if (keyValues == null) {
                keyValues = ArrayList()
            }
            if (!keyValues!!.contains(value)) {
                keyValues!!.add(value)
            }
        }

        /**
         * Adding possible frequencies with manual entered frequency
         * @param key  Manually entered frequency through keypad
         * @param value Manually entered frequency through keypad
         * @return ChartDetails object will return
         */
        fun addPossibleChildrenValues(key: String, value: String): ChartDetails {
            if (children == null) {
                children = ArrayList()
            }

            var index = -one
            if (keyValues!!.contains(key)) {
                index = keyValues!!.indexOf(key)
            }

            val holder: ChartDetails
            if (index != -one && children!!.size > index) {
                holder = children!![index]
            } else {
                holder = ChartDetails()
                children!!.add(holder)
            }

            holder.addKeyValues(value)
            return holder
        }

        /**
         * getting child frequency
         * @param number keyvalues
         * @return ChartDetails object will return
         */
        fun getChildrenObject(number: String): ChartDetails? {
            if (keyValues != null && keyValues!!.contains(number)) {
                if (children != null && children!!.size > keyValues!!.indexOf(number)) {
                    return children!![keyValues!!.indexOf(number)]
                }
            }
            return null
        }
    }
}