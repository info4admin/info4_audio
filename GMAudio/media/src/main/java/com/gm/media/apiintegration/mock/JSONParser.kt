package com.gm.media.apiintegration.mock

import com.gm.media.R
import com.gm.media.apiintegration.AudioService
import com.gm.media.apiintegration.SystemListener
import com.gm.media.models.AMFMStationInfo_t
import com.gm.media.models.Builder
import com.gm.media.models.eRdsStatus
import com.gm.media.models.eTPStationStatus
import org.json.JSONArray
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader

/**
 * This class provides implementation for IDataSourceProvider.
 * It fetches AM/FM data from R.raw.simulationdata
 *
 * @author Aswin on 3/21/2018.
 */

class JSONParser : IDataSourceProvider {

    override fun nowPlayingDataList(): ArrayList<AMFMStationInfo_t> {
        val stationList = ArrayList<AMFMStationInfo_t>()
        val stream = AudioService.instance!!.resources.openRawResource(R.raw.simulationdata)
        val jsonArray = JSONArray(getJsonString(stream))

        for (i in 0 until jsonArray.length()) {
            if (getAMFMStationInfo_t(jsonArray.getJSONObject(i)) != null)
                stationList.add(getAMFMStationInfo_t(jsonArray.getJSONObject(i))!!)
        }

        return stationList
    }

    /**
     * Parses JSONObject and creates AMFMStationInfo_t object
     *
     * @param jsonObject eg. {"sourceType": "FM","metaData1": "93.5","metaData2": "FM1","metaData3": "Red FM","ptyCategory": "1","objectId":1,"TPsupported" :1}
     * @return AMFMStationInfo_t object
     */
    private fun getAMFMStationInfo_t(jsonObject: JSONObject): AMFMStationInfo_t? {

        if (jsonObject.getString("sourceType").equals("AM") || jsonObject.getString("sourceType").equals("FM")) {
            val amFMStationInfo_t = Builder()
                    .frequency(jsonObject.getString("metaData1").toFloat())
                    .stationName(jsonObject.getString("metaData3"))
                    .rdsStatus(eRdsStatus.AMFM_RDS_AVAILABLE)
                    .objectId(jsonObject.getInt("objectId"))
                    .rdsStationInfo(jsonObject.getString("sourceType"))
                    .ptyCategory(jsonObject.getString("ptyCategory").toInt())
                    .tpStationStatus(eTPStationStatus.AMFM_STATION_SUPPORTS_TP)
                    .build()
            when (jsonObject.getInt("TPsupported")) {
                1 -> amFMStationInfo_t.tpStationStatus = eTPStationStatus.AMFM_STATION_SUPPORTS_TP
                else ->
                    amFMStationInfo_t.tpStationStatus = eTPStationStatus.AMFM_STATION_DOES_NOT_SUPPORT_TP
            }
            return amFMStationInfo_t
        } else {
            return null
        }

    }

    private fun getJsonString(stream: InputStream): String {
        val strBuilder = StringBuilder()
        var jsonString: String? = null
        val bfReader = BufferedReader(InputStreamReader(stream, "UTF-8"))
        while ({ jsonString = bfReader.readLine(); jsonString }() != null) {
            strBuilder.append(jsonString)
        }
        return strBuilder.toString()
    }
}