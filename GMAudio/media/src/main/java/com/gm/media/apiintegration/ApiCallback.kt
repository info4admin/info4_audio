package com.gm.media.apiintegration

interface ApiCallback {

    fun addCategoryStationListUpdate()

    fun updateCarouselView()

    fun updateRadioPresetsList()

}