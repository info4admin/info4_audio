package com.gm.media.models

import android.databinding.ObservableArrayList
import android.databinding.ObservableField
import gm.calibrations.LANGUAGEANDREGIONALIZATIONGLOBALA_ENUM

/**
 * Container of observable fields used for UI update. Technically called each variable as Data pool ID's.
 * @author GM on 3/7/2018.
 */
object DataPoolDataHandler {

    var previousSourceType: NOWPLAYING_SOURCE_TYPE = NOWPLAYING_SOURCE_TYPE.AM

    /** To set the list of the AMFMStationInfo */
    var browseList = ObservableArrayList<AMFMStationInfo_t>()

    /** To set the list of the AMStationInfo */
    var browseListAM = ObservableArrayList<AMFMStationInfo_t>()

    /** To set the list of the FMStationInfo */
    var browseListFM = ObservableArrayList<AMFMStationInfo_t>()

    /** Current tune station info is set in this variable */
    var aMFMStationInfo_t = ObservableField<AMFMStationInfo_t>()

    /** To set the list of the favoriteList */
    var favoriteList = ObservableArrayList<String>()

    /** To set the themeType */
    var themeType = ObservableField<Boolean>()

    var LHD_RHD_RTL = ObservableField<eLHD_RHD_RTL>()

    /** To set the deviceData */
    var deviceData = ObservableField<DeviceData>()

    var EUOrNA = ObservableField<LANGUAGEANDREGIONALIZATIONGLOBALA_ENUM.REGIONS>(LANGUAGEANDREGIONALIZATIONGLOBALA_ENUM.REGIONS.GMNA)



    /** To set the list of the amfmCategoryList */
    var amfmCategoryList = ObservableArrayList<AMFMStationInfo_t>()

    /** Used to update Progress bar */
    var updateProgress = ObservableField(0)

    /** Used to show Progress bar */
    var showUpdateProgress = ObservableField<Boolean>(false)

    /** Used to Disabled audio Control */
    var audioControlDisabled = ObservableField<Boolean>(false)

    /** Used to Disabled audio Control FM */
    var audioControlDisabledforFM = ObservableField<Boolean>(false)

     /** To set the manualTuneData */
    var manualTuneData = ObservableField<ManualTuneData>()

    //DataPoolDataHandler from Low Radio
    /**
     * To set the AUDIOMANAGER_CHANGE_AUDIOSOURCE
     * It can have any constant from NOWPLAYING_SOURCE_TYPE
     * It represents current selected source for audio
     */
    var AUDIOMANAGER_CHANGE_AUDIOSOURCE = ObservableField<NOWPLAYING_SOURCE_TYPE>(NOWPLAYING_SOURCE_TYPE.AM)
    /**
     * To set the AMTUNER_CURRENTSTATIONINFO_OBJECTID
     * It represents current selected channel id
     */
    var AMTUNER_CURRENTSTATIONINFO_OBJECTID = ObservableField<Int>()
    /**
     * To set the AMTUNER_CURRENTSTATIONINFO_FREQUENCY
     * It represents current selected channel frequency
     */
    var AMTUNER_CURRENTSTATIONINFO_FREQUENCY = ObservableField<String>()
    /**
     * To set the AMTUNER_CURRENTSTATIONINFO_STATIONNAME
     * It represents current selected channel name
     */
    var AMTUNER_CURRENTSTATIONINFO_STATIONNAME = ObservableField<String>("")
    /**
     * To set the AMTUNER_CURRENTSTATIONINFO_RDSSTATUS
     * It represents current selected channel rds status
     */
    var AMTUNER_CURRENTSTATIONINFO_RDSSTATUS = ObservableField<Float>()
    /**
     * To set the AMTUNER_CURRENTSTATIONINFO_RDSSTATIONINFO
     * It represents current selected channel rds info
     */
    var AMTUNER_CURRENTSTATIONINFO_RDSSTATIONINFO = ObservableField<String>()
    /**
     * To set the AMTUNER_CURRENTSTATIONINFO_PTYCATEGORY
     * It represents current selected channel type
     */
    var AMTUNER_CURRENTSTATIONINFO_PTYCATEGORY = ObservableField<Int>()
    /**
     * To set the AMTUNER_CURRENTSTATIONINFO_TPSTATIONSTATUS
     * It represents current selected channel tp station status
     */
    var AMTUNER_CURRENTSTATIONINFO_TPSTATIONSTATUS = ObservableField<Float>()
    /**
     * To set the FMTUNER_CURRENTSTATIONINFO_OBJECTID
     * It represents current selected channel id
     */
    var FMTUNER_CURRENTSTATIONINFO_OBJECTID = ObservableField<Int>()
    /**
     * To set the FMTUNER_CURRENTSTATIONINFO_PTYCATEGORY
     * It represents current selected channel type
     */
    var FMTUNER_CURRENTSTATIONINFO_PTYCATEGORY = ObservableField<Int>()
    /**
     * To set the FMTUNER_CURRENTSTATIONINFO_TPSTATIONSTATUS
     * It represents current selected channel tp station status
     */
    var FMTUNER_CURRENTSTATIONINFO_TPSTATIONSTATUS = ObservableField<Float>()
    /**
     * To set the FMTUNER_CURRENTSTATIONINFO_RDSSTATUS
     * It represents current selected channel rds status
     */
    var FMTUNER_CURRENTSTATIONINFO_RDSSTATUS = ObservableField<Float>()
    /**
     * To set the FMTUNER_CURRENTSTATIONINFO_RDSSTATIONINFO
     * It represents current selected channel rds info
     */
    var FMTUNER_CURRENTSTATIONINFO_RDSSTATIONINFO = ObservableField<String>()
    /**
     * To set the FMTUNER_CURRENTSTATIONINFO_FREQUENCY
     * It represents current selected channel frequency
     */
    var FMTUNER_CURRENTSTATIONINFO_FREQUENCY = ObservableField<String>()
    /**
     * To set the FMTUNER_CURRENTSTATIONINFO_STATIONNAME
     * It represents current selected channel name
     */
    var FMTUNER_CURRENTSTATIONINFO_STATIONNAME = ObservableField<String>("")
    /**
     * To set the FMTUNER_TPSTATUS_NOWPLAYING
     * It represents current selected channel playing status
     */
    var FMTUNER_TPSTATUS_NOWPLAYING = ObservableField<Float>()
    /**
     * To set the FMTUNER_TPSTATUS_LABELINDICATOR_NOWPLAYING
     */
    var FMTUNER_TPSTATUS_LABELINDICATOR_NOWPLAYING = ObservableField<Float>()
    /**
     * To set the FMTUNER_CATEGORYSTATIONLIST_CATEGORYTYPE
     */
    var FMTUNER_CATEGORYSTATIONLIST_CATEGORYTYPE = ObservableField<String>()
    /**
     * To set the FMTUNER_CATEGORYSTATIONLIST_STATIONLISTCOUNT
     */
    var FMTUNER_CATEGORYSTATIONLIST_STATIONLISTCOUNT = ObservableField<Int>()
    /**
     * To set the AMTUNER_STRONGSTATIONS_LIST_LISTID
     */
    var AMTUNER_STRONGSTATIONS_LIST_LISTID = ObservableField<Int>()
    /**
     * To set the AMTUNER_STRONGSTATIONS_LIST_UPDATE_STATUS
     */
    var AMTUNER_STRONGSTATIONS_LIST_UPDATE_STATUS = ObservableField<Float>()
    /**
     * To set the FMTUNER_STRONGSTATIONS_LIST_LISTID
     */
    var FMTUNER_STRONGSTATIONS_LIST_LISTID = ObservableField<Int>()
    /**
     * To set the FMTUNER_STRONGSTATIONS_LIST_UPDATE_STATUS
     */
    var FMTUNER_STRONGSTATIONS_LIST_UPDATE_STATUS = ObservableField<Float>()
    /**
     * To set the AMTUNER_STRONGSTATIONS_LIST_UPDATE_PROGRESS
     */
    var AMTUNER_STRONGSTATIONS_LIST_UPDATE_PROGRESS = ObservableField<Int>()
    /**
     * To set the FMTUNER_STRONGSTATIONS_LIST_UPDATE_PROGRESS
     */
    var FMTUNER_STRONGSTATIONS_LIST_UPDATE_PROGRESS = ObservableField<Int>()
    /**
     * To set the AMTUNER_DIRECTTUNE_PARTIAL_FREQUENCY
     */
    var AMTUNER_DIRECTTUNE_PARTIAL_FREQUENCY = ObservableField<String>("")
    /**
     * To set the AMTUNER_DIRECTTUNE_AVAILABLE_KEYS
     */
    var AMTUNER_DIRECTTUNE_AVAILABLE_KEYS = ObservableField<String>()
    /**
     * To set the FMTUNER_DIRECTTUNE_PARTIAL_FREQUENCY
     */
    var FMTUNER_DIRECTTUNE_PARTIAL_FREQUENCY = ObservableField<String>("")
    /**
     * To set the FMTUNER_DIRECTTUNE_AVAILABLE_KEYS
     */
    var FMTUNER_DIRECTTUNE_AVAILABLE_KEYS = ObservableField<String>()
    /**
     * To set the FMTUNER_RDSSWITCH
     */
    var FMTUNER_RDSSWITCH = ObservableField<Int>()
    /**
     * To set the FMTUNER_REGIONSETTING
     */
    var FMTUNER_REGIONSETTING = ObservableField<Int>()
    /**
     * To set the FMTUNER_TPSTATUS
     */
    var FMTUNER_TPSTATUS = ObservableField(0)
    /**
     * To set the FMTUNER_TPSTATUS_SEARCHSTATE
     */
    var FMTUNER_TPSTATUS_SEARCHSTATE = ObservableField<Float>()
    /**
     * To set the FMTUNER_TRAFFICALERT_NAME
     */
    var FMTUNER_TRAFFICALERT_NAME = ObservableField<String>()
    /**
     * To set the FMTUNER_TRAFFICALERTACTIVECALL_NAME
     */
    var FMTUNER_TRAFFICALERTACTIVECALL_NAME = ObservableField<String>()
    /**
     * To set the FMTUNER_TRAFFICALERTACTIVECALL_PROGRAMIDENTIFIER
     */
    var FMTUNER_TRAFFICALERTACTIVECALL_PROGRAMIDENTIFIER = ObservableField<Int>()
    /**
     * To set the FMTUNER_PROGRAMTYPERDSNOTIFICATION_NAME
     */
    var FMTUNER_PROGRAMTYPERDSNOTIFICATION_NAME = ObservableField<String>()
    /**
     * To set the FMTUNER_PROGRAMTYPERDSNOTIFICATION_CONTENT
     */
    var FMTUNER_PROGRAMTYPERDSNOTIFICATION_CONTENT = ObservableField<String>()
    /**
     * To set the FMTUNER_STATIONAVAILABILITY_METADATA
     */
    var FMTUNER_STATIONAVAILABILITY_METADATA = ObservableField<String>()
    /**
     * To set the FMTUNER_STATIONAVAILABILITY_STATUS
     */
    var FMTUNER_STATIONAVAILABILITY_STATUS = ObservableField<Float>()
    /**
     * To set the AMFMTUNER_CATEGORYSTATIONLIST_OBJECTLIST
     */
    var AMFMTUNER_CATEGORYSTATIONLIST_OBJECTLIST = ObservableArrayList<AMFMStationInfo_t>()
    /**
     * To set the FMTUNER_TPSEARCH_UPDATE
     */
    var FMTUNER_TPSEARCH_UPDATE = ObservableField<String>()
    /**
     * To set the AMTUNER_CATEGORYSTATIONLIST_CATEGORYTYPE
     */
    var AMTUNER_CATEGORYSTATIONLIST_CATEGORYTYPE = ObservableField<String>()
    /**
     * To set the AMTUNER_CATEGORYSTATIONLIST_STATIONLISTCOUNT
     */
    var AMTUNER_CATEGORYSTATIONLIST_STATIONLISTCOUNT = ObservableField<Int>()
    /*
    * To set the screen layout size is Large or not*/
    var isLargeScreenLayout = ObservableField<Boolean>(false)

    /* To set the screen layout mode enabled or disabled*/
    var isLargeScreenLayoutEnabled = ObservableField<Boolean>(true)

    /** To Enable and Disable Skew */
    var isSkewEnabled = ObservableField<Boolean>(false)

    /** To set Skew slope angle to 8*/
    var SKEW_ANGLE_8 = ObservableField<Float>()

    /** To set Skew slope angle to 9*/
    var SKEW_ANGLE_9 = ObservableField<Float>()

    /** To set Skew slope angle to */
    var SKEW_ANGLE_15 = ObservableField<Float>()

    /** To set the PopupMenu Visibility */
    var isPopupMenuVisibile = ObservableField<Boolean>(false)

    /** To set the BROWSE_SOURCE_TYPE */
    var BROWSE_SOURCE_TYPE = ObservableField<String>()

    /** To set the Curved View */
    var isInfoCurvedView = ObservableField<Boolean>(false)

    /** To set and update the select item in browse preset list */
    var nowPlayingBrowseListPosition  = ObservableField<Int>(0)

}