package com.gm.media.models

/* Custom Data Models */

/**
 * This class has information about DeviceData.
 * @property type the type of DeviceData class.
 * @property name the name of DeviceData class.
 * @property id the id of DeviceData class.
 * @property isReadable the isReadable of DeviceData class.
 * @property isBrowsable the isBrowsable of DeviceData class.
 */
data class DeviceData(var type: String = "", var name: String = "", var id: String = "", var isReadable: String = "", var isBrowsable: String = "")

/**
 * This class has information about ManualTuneData.
 * @property frequency the frequency of ManualTuneData class.
 * @property isValidChannel the isValidChannel of ManualTuneData class.
 * @property buttonValues True/False for enabling/disabling the button respectively.
 */
data class ManualTuneData(var frequency: String = "", var isValidChannel: Boolean = false, var buttonValues: Array<Boolean> = arrayOf(true, true, true, true, true, true, true, true, true, true))

//this builder class name should be changed based on features
data class Builder
(
        var frequency: Float = 0f,//TODO need to convert to int
        var stationName: String? = "",
        var rdsStatus: eRdsStatus = eRdsStatus.AMFM_RDS_NOT_AVAILABLE,
        var objectId: Int = 0,
        var rdsStationInfo: String? = "",
        var ptyCategory: Int = 0,
        var tpStationStatus: eTPStationStatus = eTPStationStatus.AMFM_STATION_DOES_NOT_SUPPORT_TP,
        var isFavorite: Boolean = false,
        var categoryData: MutableMap<String, ArrayList<AMFMStationInfo_t>>? = null
) {
    fun frequency(frequency: Float) = apply { this.frequency = frequency }
    fun stationName(stationName: String?) = apply {
        if (stationName == null)
            this.stationName = ""
        else
            this.stationName = stationName
    }

    fun rdsStatus(rdsStatus: eRdsStatus) = apply {
        this.rdsStatus = rdsStatus
    }
    fun objectId(objectId: Int) = apply { this.objectId = objectId }

    fun rdsStationInfo(rdsStationInfo: String?) = apply {
        if (rdsStationInfo == null)
            this.rdsStationInfo = ""
        else
            this.rdsStationInfo = rdsStationInfo
    }

    fun ptyCategory(ptyCategory: Int) = apply { this.ptyCategory = ptyCategory }
    fun tpStationStatus(tpStationStatus: eTPStationStatus) = apply { this.tpStationStatus = tpStationStatus }
    fun isFavorite(isFavorite: Boolean) = apply { this.isFavorite = isFavorite }
    fun categoryData(categoryData: MutableMap<String, ArrayList<AMFMStationInfo_t>>) = apply { this.categoryData = categoryData }


    fun build() = AMFMStationInfo_t(frequency, stationName, rdsStatus, objectId, rdsStationInfo, ptyCategory,
            tpStationStatus, isFavorite, categoryData)
}